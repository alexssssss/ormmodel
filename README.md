OrmModel
====
| Branch  | GitLab CI Status                                                                                                                                                                                                                                                                          |
|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Master  | [![build status](https://gitlab.com/alexssssss/ormmodel/badges/master/build.svg)](https://gitlab.com/alexssssss/ormmodel/commits/master) [![coverage report](https://gitlab.com/alexssssss/ormmodel/badges/master/coverage.svg)](https://gitlab.com/alexssssss/ormmodel/commits/master)   |

#Install

**config.php**
```php
<?php
define('DB_SERVER', '');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');
```

**db.php**
```php
<?php
/* @var $injector \Auryn\Injector */

$injector->alias('Aura\Sql\ExtendedPdoInterface', 'Aura\Sql\ExtendedPdo');
$injector->share('Aura\Sql\ExtendedPdoInterface');
$injector->share('Aura\Sql\ExtendedPdo');

$injector->share('Framework\PdoDatabase');
$injector->alias('Framework\DatabaseInterface', 'Framework\PdoDatabase');
$injector->share('Framework\Database');

if (defined('DB_SERVER') && defined('DB_USERNAME') && defined('DB_PASSWORD') && defined('DB_DATABASE')) {
    $injector->delegate('Aura\Sql\ExtendedPdo', function () {
        $connection = new \Aura\Sql\ExtendedPdo('mysql:host=' . DB_SERVER . ';dbname=' . DB_DATABASE, DB_USERNAME, DB_PASSWORD);
        $connection->exec("SET NAMES 'utf8'");

        return $connection;
    });
}
```

**ormmodel.yml**
```yaml
paths:
    bootstrapFile: %%ORMMODEL_CONFIG_DIR%%/bootstrap.php
    services: %%ORMMODEL_CONFIG_DIR%%/app/models/Service    
    modelNamespace: Model
```
**index.php**
```php
<?php
require_once __DIR__.'/vender/autoload.php';
require_once __DIR__.'/config.php';
require_once __DIR__.'/dv.php';

$carModel = $injector->make('\Model\Service\Car');

$carModel->get();
```
