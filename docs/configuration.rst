.. _config:

################
  Configuration
################
to get started we first need to configure some things.

===============
  Database
===============

We need to set the database correctly in our ``.env``

.. code-block:: sql

    ###> alexssssss/ormmodel-bundle ###
    DB_TYPE=mysql
    DB_SERVER=127.0.0.1
    DB_USERNAME=root
    DB_PASSWORD=root
    DB_NAME=cars

Now that we are connected to our database we can create :ref:`models <models>`


===========
  Model
===========

In order to use the Models you need to manually create the following file structure:

* ``src\Model``
* ``src\Model\Service``
* ``src\Model\Wrapper``

============
  Services
============

In order to use a service we have to set it inside the ``config\services.yaml``

.. code-block:: yaml

    services:
        App\Model\Service\:
            resource: '../src/Model/Service/*'
            public: true

============
  htacces
============
The routes need a model rewrite to work we can make this by creating the ``public/.htaccess`` file. And adding this to it:

.. code-block:: php

    RewriteEngine on

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ index.php [QSA,L]

=============
  Bootstrap
=============
If you make a form or use a ``$object->save();`` you need to define 2 things in your ``config/bootstrap.php``.
This will define your vendor directory the standaard is:

.. code-block:: php

    define('ROOT_DIR', __DIR__);
    define('VENDOR_DIR', ROOT_DIR . '/vendor');