.. OrmModel documentation master file, created by
   sphinx-quickstart on Fri Mar  8 08:48:21 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OrmModel's documentation!
====================================

What is this about
======================
In this documentation  we are going to make a simple website.
On this website you are able to buy and sell cars, and make advertisements for them.
This document should make you more comfortable with the OrmModel.
We want to cover as much as possible in this documentation.
If you have questions please go to Alex Damen, creator of The OrmModel.


.. toctree::
   :maxdepth: 4
   :caption: Contents:

   installation
   configuration
   phinx
   ormmodel/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

