.. _installation:

################
  Installation
################

.. _homestead: https://symfony.com/doc/current/setup/homestead.html
.. _setup: https://symfony.com/doc/current/setup.html
.. _Phinx: https://book.cakephp.org/3.0/en/phinx.html
.. _source code: https://gitlab.com/alexssssss/ormmodel
.. _symfony bundle: https://gitlab.com/alexssssss/ormmodel-bundle

===========
  Symfony
===========
First open a Terminal with ``cmd + space`` that opens Spotlight Search and type Terminal. `
To create your new Symfony application, first make sure you’re using PHP 7.1 ``php --version`` or higher and have Composer installed.
If you want to use a virtual machine (VM), check out `homestead`_.

Create your new project by running:

.. code-block:: html

    compser create-project symfony/website-skeleton my-project

Once installed go into the my-project directory. And install the Symfony server into your project:

.. code-block:: html

    cd my-project
    composer require symfony/web-server-bundle --dev

Now we can start the webserver like so:

.. code-block:: html

    bin/console server:run

Open your browser and navigate to ``http://localhost:8000/``.
If everything is working, you’ll see a welcome page.
Later, when you are finished working, stop the server by pressing ``Ctrl+C`` rom your terminal.

At this point everything we need is installed if, you want to go deeper into the installation visit the symfony `setup`_ page.

============
  OrmModel
============
Once the installation went succesful we need to install the OrmModel,
we can do so by installing the symfony bundle in our project.
You can find the `source code`_ here.

.. code-block:: html

    cd my-project
    composer require alexssssss/ormmodel-bundle

==================
  Phinx *optional*
==================
`Phinx`_ is a standalone command line tool for managing database Migrations.
The official Migrations plugin for CakePHP is based on this tool.

Phinx makes it ridiculously easy to manage the database migrations for your PHP app.
In less than 5 minutes, you can install Phinx using Composer and create your first database migration.
Phinx is just about migrations without all the bloat of a database ORM system or application framework.

To install Phinx, simply require it using Composer:

.. code-block:: html

    cd my-project
    composer require alexssssss/phinx-bundle
