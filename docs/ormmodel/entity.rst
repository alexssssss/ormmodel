.. _entity:

##############
  Entity
##############

.. note::
    You can use entities inside your ``controller`` or twig template.
    The ``AbstractEntity.php`` is located inside the ``vendor/alexssssss/ormmodel/src/Entity``


Working with entities is easy they work the same as Symfony services.
in the entity class you can make custom functions to prevent recursion.

To create a model we firts need the database.
Entities are automaticly generated with the command ``bin/console ormmodel:make:model <NAME> <DATABASE NAME>``
This is the ``Car`` entity:

.. code-block:: php

    <?php

    namespace App\Model\Entity;

    use Alexssssss\OrmModel\Relation\HasManyToManyInterface;
    use App\Model\Service;

    class Car extends \Alexssssss\OrmModel\Entity\AbstractEntity
    {
        public $name;

        public $year;

        public $state;

        public $image;

        public $manufacture;

        public $model;

        /**
         * price in euro
         * @var Integer
         */
        public $price;

        public $colour;

        // use this to connect your many-to-many relationship
        /**
         * @var HasManyToManyInterface
         */
        public $bodywork;

        /**
         * Power of the engine in hp
         * @var Integer
         */
        public $power;

        public function __construct(Service\Car $service)
        {
            parent::__construct($service);
        }
    }

In this example we will create a function ``convertPrice()`` this will
return an integer or float.

.. code-block:: php

    public function convertPrice()
    {
        return $this->price * 1.11;
        // Return the price of the car * 1.11
    }

==============
  Get
==============
You can get multiple things from an entitiy like : ``getId()``, ``getUuid()``

==============
  GetId
==============
You can get the id of an entity like this.

.. code-block:: php

    <?php
    $carEntity->getId();
    // return the id as an integer

==============
  GetUuid
==============
You can get the uuid from an entity.

.. code-block:: php

    <?php
    $carEntity->getUuid();
    // return the uuid as an string

=================
  Has
=================
You can check if an entity has a field.

.. code-block:: php

    <?php
    $carEntity->has('name'); // true
    // return a boolean

===================
  ToFriendlyArray
===================
This will cast an entity to an key, value array without relationships.

.. code-block:: php

    <?php
    $carEntity->toFriendlyArray();
    // return the car as an array

===================
  Save
===================
After editing an entity you want to save it.

.. code-block:: php

    <?php
    $carEntity->save();
    // return a boolean or exception

===================
  Saveall
===================
If you edit an entity that has an edited relationship you can save them both with saveall.

.. code-block:: php

    <?php
    $carEntity->saveAll();
    // return a boolean or exception

===================
  Isset
===================
to check if an entity isset you use this function.

.. code-block:: php

    <?php
    $carEntity->isset();
    // return a boolean

===================
  Hidden
===================
If you have hidden field this function will retrieve them.

.. code-block:: php

    <?php
    $carEntity->getHidden();
    // return an array with the hidden values

===================
  Twig
===================
You can call an entity function directly in a Twig template.

.. code-block:: html

    {{ car.convertPrice() }}
    <!-- This will convert euro to dollars -->