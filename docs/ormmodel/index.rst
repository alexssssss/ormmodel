.. _symfony: https://symfony.com/doc/current/index.html#gsc.tab=0
.. _ormmodel:

#################
  The OrmModel
#################

..  toctree::
    :maxdepth: 3

    relationship
    service
    entity

After the :ref:`installation` we need to create a few simple pages.

.. note::

    this is not a symfony tutorial.
    if you do not understand how to create a page or controller.
    please read the `symfony`_ documents.


.. _cli:

================
  CLI
================

The CLI of the OrmModel can be activated trough ``bin/console``
There you should see the following:

.. code-block:: html

    ormmodel
        ormmodel:make:model                     Create new models
        ormmodel:status                         Compaire model with the database

.. _models:

================
  Models
================



We need to have a relationship between a car and the car manufacture.
we can create a that model as follow:

.. code-block:: html

    bin/console ormmodel:make:model Manufacture car_manufactures

Now we just need some models to bind a car to a car manufacture.
Maybe we even want to bind cars to a specific type ex: cabrio, coupe, suv, etc..
For all that we need relationships.


.. note::

    if you need a namespace you can do so:

    .. code-block:: html

        bin/console ormmodel:make:model Engine\Crank car_engine_cranks

    this will create the following structure:
    ``App\Model\(Entity, Wrapper, Service)\Engine\Crank``

