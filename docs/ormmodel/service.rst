.. _services: https://symfony.com/doc/current/service_container.html

.. _service:

==========
  Service
==========

.. note::
    You can use services inside your ``controller`` .
    The ``AbstractService.php`` is located inside the ``vendor/alexssssss/ormmodel/src/Service``


For more information about how Symfony this does see the `services`_ docs
To use a service model we have to include it in our controller like this ``use App\Model\Service\Car;``
Now we can use it in our function as a parameter. The code below will return all the cars we have.

.. code-block:: php

    <?php
    use App\Model\Service\Car;

    class CarController extends AbstractController
    {
        public function index(Car $carModel)
        {
            /**
             * This will return all the cars as an object with arrays
             */
            $cars = $carModel->get();
        }
    }

--------
  Get
--------
There a multiple ways of getting something out of your model with the ``Object Storage`` below i list a couple of examples.
but first we need to get all of our cars:

.. code-block:: php

    <?php
    $cars = $carModel->get();
    // this will return all the cars in the database

The first one is getting the first database entry:

.. code-block:: php

    <?php
    $cars->getFirst();
    // returns the first car object in the database

And there is also one to get the last one:

.. code-block:: php

    <?php
    $cars->getLast();
    // returns the last car object in the database

-----------
  Get one
-----------
If we want to see the details of a car or manufacture we can select one by their id.
This will return an object where the id is 1. There are multiple ways of doing this:

.. code-block:: php

    <?php
    $carWithId = $carModel->getOne(1);
    $carWithId = $carModel->get(['id' => 1]);
    // This will return the car with the id 1

----------
  Get all
----------
Let's say that we want to show all of the cars we have, we can do that with the following code:

.. code-block:: php

    <?php
    $carAll = $carModel->get();
    // this will return all the cars in the database

------------------
  Get with qeury
------------------
Alex have made something really special in his code, because you can use the function ``get`` like a SQL qeury.
Here is an example of what you can do with it:

.. code-block:: php

    <?php
    $carQeury = $cars->get([ ['price', '>', 80000]], 'OR', ['power', '>', 200]);
    // This will return all the cars where the price is above 80.000 or the power is more then 200

Another example is an and statement:

.. code-block:: php

    <?php
    $cars->get([['year', '>', 2016], 'state' => 'New']);
    // year = 2018, state = New
    // year = 2016, state = New

This will get all the cars newer then 2016 with the state 'New'.

---------
  Sort
---------
We can for example sort the cars by price:

.. code-block:: php

    <?php
    $cars->sort(['price' => 'DESC']);
    // 150.000
    // 120.000
    // 90.000

-----------
  Limit
-----------
With the limit function you can limit the results you want. It can take a start and end parameter.
So if we want to show a page of 20 cars we can do so:

.. code-block:: php

    <?php
    $cars->limit(start = 0, max = 20);
    // return a limit between 0 and 20 cars.

---------
  Count
---------
We can count how many cars we have with the count function.

.. code-block:: php

    <?php
    $cars->count(); // 5

----------
  Math
----------
We can also do math with the ``Object storage``.
It supports al the basic equation.
**This will only return the value not the object**

^^^^^^^^^^^
  Minimun
^^^^^^^^^^^
We can get the lowest price of an car by simply calling min on it:


.. code-block:: php

    <?php
    $car->min('price'); // 70.000

^^^^^^^^^^^
  Maximum
^^^^^^^^^^^
With max you will get the highest price of a car.

.. code-block:: php

    <?php
    $car->max('price'); // 1.000.000

^^^^^^^^^^^
  Average
^^^^^^^^^^^
With avg you will get the average price of the cars.

.. code-block:: php

    <?php
    $car->avg('price'); // 67.000 + 40.000 / 2 = 53.500

^^^^^^^^^^^
  Sum
^^^^^^^^^^^
With sum you will get the total price of the cars.

.. code-block:: php

    <?php
    $car->sum('price'); // 80.000 + 50.000 + 45.000 = 175.000



