##############
  Phinx
##############

.. _Phinx: https://book.cakephp.org/3.0/en/phinx/migrations.html

Link to the `Phinx`_ migration docs.

Phinx has a couple of CLI tools build into your symfony project:

.. code-block:: html

    phinx
        phinx:breakpoint                        Manage breakpoints
        phinx:create                            Create a new migration
        phinx:migrate                           Migrate the database
        phinx:rollback                          Rollback the last or to a specific migration
        phinx:seed:create                       Create a new database seeder
        phinx:seed:run                          Run database seeders
        phinx:status                            Show migration status

Were are not going to cover them in this documentation.
But we did use phinx inside the project. It is recommendid that you do soo to.

.. note::

    To use Phinx you need to manually create the directories:
    ``src/Migration`` and for the seed: ``src/Seed``