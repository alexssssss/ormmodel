<?php

namespace Alexssssss\OrmModel;

use Aura\Sql\ConnectionLocator;
use Psr\Container\ContainerInterface;

abstract class AbstractConnectionFactory implements ConnectionFactoryInterface
{
    /**
     * @var ConnectionLocator
     */
    protected $connectionLocator;

    /**
     * @var \Aura\Sql\ExtendedPdoInterface
     */
    protected $adapter;

    /**
     * @var \Aura\Sql\ExtendedPdoInterface
     */
    protected $writeAdapter;

    /**
     * @var \Aura\Sql\ExtendedPdoInterface
     */
    protected $readAdapter;

    /**
     *
     * @var string
     */
    protected $username;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     *
     * @var string
     */
    protected $database;

    /**
     *
     * @var string
     */
    protected $host;

    /**
     *
     * @var string
     */
    protected $charset = 'utf8';

    /**
     *
     * @var string
     */
    protected $readSlave1;

    /**
     *
     * @var string
     */
    protected $readSlave2;

    /**
     *
     * @var string
     */
    protected $readSlave3;

    /**
     *
     * @var string
     */
    protected $readSlave4;

    /**
     *
     * @var string
     */
    protected $readSlave5;

    /**
     * ConnectionFactory constructor.
     * @param string $username
     * @param string $password
     * @param string $database
     * @param string $host
     * @param string|null $readSlave1
     * @param string|null $readSlave2
     * @param string|null $readSlave3
     * @param string|null $readSlave4
     * @param string|null $readSlave5
     * @param string|null $charset
     */
    public function __construct(string $username, string $password, string $database, string $host, string $readSlave1 = null, string $readSlave2 = null, string $readSlave3 = null, string $readSlave4 = null, string $readSlave5 = null, string $charset = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->host = $host;
        $this->readSlave1 = $readSlave1;
        $this->readSlave2 = $readSlave2;
        $this->readSlave3 = $readSlave3;
        $this->readSlave4 = $readSlave4;
        $this->readSlave5 = $readSlave5;
        $this->charset = $charset ?? $this->charset;
    }

    public function getAdapter(): \Aura\Sql\ExtendedPdoInterface
    {
        if ($this->adapter === null) {
            $this->adapter = new \Aura\Sql\ExtendedPdo('mysql:host=' . $this->host . ';dbname=' . $this->database . ';charset=' . $this->charset, $this->username, $this->password, [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $this->charset . ', default_week_format = 3;']);
        }

        return $this->adapter;
    }

    public function getWriteAdapter(string $name = null): \Aura\Sql\ExtendedPdoInterface
    {
        if ($this->writeAdapter === null) {
            $this->writeAdapter = $this->getConnectionLocator()->getWrite($name ?? '');
        }

        return $this->writeAdapter;
    }

    public function getReadAdapter(string $name = null): \Aura\Sql\ExtendedPdoInterface
    {
        if ($this->readAdapter === null) {
            $this->readAdapter = $this->getConnectionLocator()->getRead($name ?? '');
        }

        return $this->readAdapter;
    }

    public function getConnectionLocator(): ConnectionLocator
    {
        if ($this->connectionLocator === null) {
            $this->connectionLocator = new ConnectionLocator([$this, 'getAdapter'], [], ['master' => [$this, 'getAdapter']]);

            // the CLI will always use the default (= master)
            if (php_sapi_name() !== 'cli') {
                /** @var ConnectionLocator $connections */

                for ($i = 1; $i <= 5; $i++) {
                    if (!empty($this->{'readSlave' . $i})) {
                        $host = $this->{'readSlave' . $i};
                        $database = $this->database;
                        $username = $this->username;
                        $password = $this->password;

                        $this->connectionLocator->setRead('slave' . $i, function () use ($host, $database, $username, $password) {
                            $adapter = new \Aura\Sql\ExtendedPdo('mysql:host=' . $host . ';dbname=' . $database . ';charset=' . $this->charset, $username, $password, [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $this->charset . ', default_week_format = 3;']);
                            return $adapter;
                        });
                    }
                }
            }
        }

        return $this->connectionLocator;
    }
}
