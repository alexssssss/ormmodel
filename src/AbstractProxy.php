<?php

namespace Alexssssss\OrmModel;

/**
 * Class AbstractProxy
 * @package Alexssssss\OrmModel
 */
abstract class AbstractProxy implements ProxyInterface
{
    /**
     * @param $fun
     * @param $params
     * @return mixed
     */
    abstract public function __call($fun, $params);

    /**
     * @return mixed
     */
    abstract public function getRealObject();
}
