<?php

namespace Alexssssss\OrmModel;

interface ConnectionFactoryInterface
{
    /**
     * ConnectionFactoryInterface constructor.
     * @param string $username
     * @param string $password
     * @param string $database
     * @param string $host
     * @param string|null $readSlave1
     * @param string|null $readSlave2
     * @param string|null $readSlave3
     * @param string|null $readSlave4
     * @param string|null $readSlave5
     * @param string|null $charset
     */
    public function __construct(string $username, string $password, string $database, string $host, string $readSlave1 = null, string $readSlave2 = null, string $readSlave3 = null, string $readSlave4 = null, string $readSlave5 = null, string $charset = null);

    /**
     * @return \Aura\Sql\ExtendedPdoInterface
     */
    public function getAdapter(): \Aura\Sql\ExtendedPdoInterface;

    /**
     * @param string|null $name
     * @return \Aura\Sql\ExtendedPdo
     */
    public function getReadAdapter(string $name = null): \Aura\Sql\ExtendedPdoInterface;

    /**
     * @param string|null $name
     * @return \Aura\Sql\ExtendedPdo
     */
    public function getWriteAdapter(string $name = null): \Aura\Sql\ExtendedPdoInterface;
}