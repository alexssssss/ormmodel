<?php

namespace Alexssssss\OrmModel\Console\Command;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Alexssssss\OrmModel\Console\Config\Config;
use Alexssssss\OrmModel\Console\Config\ConfigInterface;

abstract class AbstractCommand extends Command
{

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->addOption('--configuration', '-c', InputOption::VALUE_REQUIRED, 'The configuration file to load');
    }

    /**
     * Bootstrap OrmModel.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    public function bootstrap(InputInterface $input, OutputInterface $output)
    {
        if (!$this->getConfig()) {
            $this->loadConfig($input, $output);
        }

        // report the service path
        $output->writeln('<info>using service path</info> ' . $this->getConfig()->getServicePath());

        // report the model path
        $output->writeln('<info>using model path</info> ' . $this->getConfig()->getModelPath());

        // report and load the bootstrap file
        $output->writeln('<info>using bootstrap file</info> ' . $this->getConfig()->getBootstrapFilePath());
        require_once $this->getConfig()->getBootstrapFilePath();

        // report the Model namespace
        $output->writeln('<info>model namespace</info> ' . $this->getConfig()->getModelNamespace());
    }

    /**
     * Sets the config.
     *
     * @param  ConfigInterface $config
     * @return AbstractCommand
     */
    public function setConfig(ConfigInterface $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * Gets the config.
     *
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Returns config file path
     *
     * @param InputInterface $input
     * @return string
     */
    protected function locateConfigFile(InputInterface $input)
    {
        $configFile = $input->getOption('configuration');

        $useDefault = false;

        if (null === $configFile || false === $configFile) {
            $useDefault = true;
        }

        $cwd = getcwd();

        // locate the ormModel config file (default: ormmodel.yml)
        // TODO - In future walk the tree in reverse (max 10 levels)
        $locator = new FileLocator([
            $cwd . DIRECTORY_SEPARATOR
        ]);

        if (!$useDefault) {
            // Locate() throws an exception if the file does not exist
            return $locator->locate($configFile, $cwd, $first = true);
        }

        $possibleConfigFiles = ['ormmodel.yml'];
        foreach ($possibleConfigFiles as $configFile) {
            try {
                return $locator->locate($configFile, $cwd, $first = true);
            } catch (\InvalidArgumentException $exception) {
                $lastException = $exception;
            }
        }
        throw $lastException;
    }

    /**
     * Parse the config file and load it into the config object
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \InvalidArgumentException
     * @return void
     */
    protected function loadConfig(InputInterface $input, OutputInterface $output)
    {
        $configFilePath = $this->locateConfigFile($input);
        $output->writeln('<info>using config file</info> .' . str_replace(getcwd(), '', realpath($configFilePath)));

        $config = Config::fromYaml($configFilePath);

        $this->setConfig($config);
    }
}
