<?php

namespace Alexssssss\OrmModel\Console\Command\Make;

use Alexssssss\OrmModel\ConnectionFactory;
use Alexssssss\OrmModel\Console\Command\AbstractCommand;
use Alexssssss\OrmModel\Console\Command\Make\ModelCommand\Generator\ClassGenerator;
use Alexssssss\OrmModel\Exception\DuplicateModel;
use Alexssssss\OrmModel\Service\ServiceFactory;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Model extends AbstractCommand
{
    /**
     * DB Connection
     *
     * @var \Aura\Sql\ExtendedPdoInterface
     */
    protected $adapter;

    public function __construct(ConnectionFactory $connectionFactory)
    {
        $this->adapter = $connectionFactory->getAdapter();
        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('make:model')
            ->setDescription('Create new models')
            ->setHelp('The models command allows you to dynamically create models.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name for the new models')
            ->addArgument('tableName', InputArgument::REQUIRED, 'The table name for the models');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $this->bootstrap($input, $output);

        /** @var $classGenerator ClassGenerator */
        $classGenerator = new \Alexssssss\OrmModel\Console\Command\Make\ModelCommand\Generator\ClassGenerator($this->adapter, $this->getConfig()->getModelPath());

        $modelName = $input->getArgument('name');
        $tableName = $input->getArgument('tableName');

        if ($this->tableExists($tableName) === false) {
            $output->writeln("Table '" . $tableName . "' does not exist.");
            exit();
        }

        if (strpos($modelName, '/') !== false) {
            $modelNameParts = explode('/', $modelName);
            $modelName = end($modelNameParts);
            array_pop($modelNameParts);
            $modelNamespace = implode("/", $modelNameParts);
        }

        $modelTypes = ['entity', 'service', 'wrapper'];
        $classGenerator->setClassName($modelName);
        $classGenerator->setModelTypes($modelTypes);

        foreach ($modelTypes as $modelType) {
            try {
                $classGenerator->setClassType($modelType)->setTableName($tableName);
                if ($classGenerator->write($modelNamespace ?? null, $this->getConfig()->getModelNamespace()) !== false) {
                    $output->writeln("Successfully created '" . $modelName . "' " . $modelType . " model.");
                } else {
                    $output->writeln("Failed to create '" . $modelName . "' " . $modelType . " model due to an unknown error.");
                }
            } catch (DuplicateModel $exception) {
                $output->writeln("Failed to create '" . $modelName . "' " . $modelType . " model. The model already exists.");
            }
        }

        return 0;
    }

    /**
     * @param string $tableName
     * @return bool
     */
    private function tableExists(string $tableName)
    {
        try {
            return ($tableInfo = $this->adapter->fetchAll("DESCRIBE `" . $tableName . "`;")) ? true : false;
        } catch (\PDOException $exception) {
            return false;
        }
    }

    private function getDatabaseName()
    {
        return $this->adapter->fetchValue('select database()');
    }

    private function getHasManyRelations(string $tableName)
    {
        $dbName = $this->getDatabaseName();
        return ($this->adapter->fetchAll("SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '" . $dbName . "' AND REFERENCED_TABLE_NAME = '" . $tableName . "' AND REFERENCED_TABLE_NAME IS NOT NULL AND REFERENCED_COLUMN_NAME IS NOT NULL;")) ?: false;
    }

    private function getBelongsToRelations(string $tableName)
    {
        $dbName = $this->getDatabaseName();
        return ($this->adapter->fetchAll("SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '" . $dbName . "' AND TABLE_NAME = '" . $tableName . "' AND REFERENCED_TABLE_NAME IS NOT NULL AND REFERENCED_COLUMN_NAME IS NOT NULL;")) ?: false;
    }
}
