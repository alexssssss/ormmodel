<?php

namespace Alexssssss\OrmModel\Console\Command\Make\ModelCommand\Generator;

use Alexssssss\OrmModel\Exception\DuplicateModel;
use Aura\Sql\ExtendedPdoInterface;

class ClassBuilder
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string
     */
    protected $className;

    /**
     * @var string
     */
    protected $classType;

    /**
     * @var ExtendedPdoInterface
     */
    protected $pdo;

    /**
     * @var array
     */
    protected $modelTypes;

    /**
     * @var string
     */
    private $completeClassString;

    /**
     * @var array
     */
    private $entityClassProperties;

    /**
     * @var string
     */
    protected $modelPath;

    /**
     * ClassGenerator constructor.
     * @param $modelPath
     */
    public function __construct($modelPath)
    {
        $this->modelPath = $modelPath;
    }

    /**
     * @param $namespace
     * @param $rootModelNamespace
     * @return bool|string
     */
    protected function buildEntityClass($namespace, $rootModelNamespace)
    {
        $namespace = !empty($namespace) ? '\\' . str_replace('/', '\\', $namespace) : '';
        $modelClassContent = $this->getBlueprintContent();
        if ($this->classType == 'service') {
            $this->completeClassString = sprintf("<?php\n" . $modelClassContent, $rootModelNamespace, $namespace, $this->className, $this->tableName, $namespace . '\\' . $this->className);
        } elseif ($this->classType == 'entity') {
            $this->setEntityClassProperties();
            $propertiesString = 'public $' . implode("; \n\n public $", $this->entityClassProperties) . ';';
            $this->completeClassString = sprintf("<?php\n" . $modelClassContent, $rootModelNamespace, $namespace, $this->className, $propertiesString, $namespace . '\\' . $this->className);
        } else {
            $this->completeClassString = sprintf("<?php\n" . $modelClassContent, $rootModelNamespace, $namespace, $this->className, $namespace . '\\' . $this->className);
        }

        return ($this->completeClassString !== false) ? $this->completeClassString : false;
    }

    /**
     * @return bool|string
     */
    private function getBlueprintContent()
    {
        return file_get_contents(__DIR__ . "/../" . $this->classType . ".txt");
    }

    /**
     * @param string $namespace
     * @param $rootModelNamespace
     * @return bool
     * @throws DuplicateModel
     * @throws \Exception
     */
    protected function writeModel($namespace, $rootModelNamespace): bool
    {
        if (empty($this->classType) || empty($this->className) || !in_array($this->classType, $this->modelTypes)) {
            throw new \Exception("Class name and/or class type cannot be empty, or are not correct.");
        }

        if ($namespace !== null && !file_exists($this->getClassPath($namespace))) {
            mkdir($this->getClassPath($namespace), 0777, true);
        }

        if (!file_exists($this->getClassPath($namespace) . $this->className . ".php")) {
            $modelFile = fopen($this->getClassPath($namespace) . $this->className . ".php", "w") or false;

            if ($modelFile !== false) {
                if (fwrite($modelFile, $this->buildEntityClass($namespace, $rootModelNamespace)) !== false && fclose($modelFile) !== false) {
                    return true;
                }
            }
        } else {
            throw new DuplicateModel();
        }

        return false;
    }

    /**
     * @param string $namespace
     * @return string
     */
    private function getClassPath($namespace)
    {
        if ($this->classType == 'service') {
            $folder = '/Service/';
        } elseif ($this->classType == 'wrapper') {
            $folder = '/Wrapper/';
        } else {
            $folder = '/Entity/';
        }

        $folder = $this->modelPath . $folder . (($namespace !== null) ? $namespace . '/' : '');
        return $folder;
    }

    private function setEntityClassProperties()
    {
        $tableCols = $this->getTableCols();
        if ($tableCols !== false) {
            foreach ($tableCols as $key => $tableCol) {
                if ($tableCol['Field'] !== 'id') {
                    if (substr($tableCol['Field'], -2) === 'Id') {
                        $tableCol['Field'] = substr($tableCol['Field'], 0, -2);
                    }

                    $this->entityClassProperties[] = $tableCol['Field'];
                }
            }
        }
    }

    /**
     * @return array|bool
     */
    private function getTableCols()
    {
        try {
            return ($tableCols = $this->pdo->fetchAll("SHOW COLUMNS FROM " . $this->tableName)) ? $tableCols : false;
        } catch (\PDOException $exception) {
            return $exception->getMessage();
        }
    }
}
