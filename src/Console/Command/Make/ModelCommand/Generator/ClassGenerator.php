<?php

namespace Alexssssss\OrmModel\Console\Command\Make\ModelCommand\Generator;

use Aura\Sql\ExtendedPdoInterface;

class ClassGenerator extends ClassBuilder
{
    /**
     * ClassGenerator constructor.
     * @param ExtendedPdoInterface $pdo
     */
    public function __construct(ExtendedPdoInterface $pdo, $modelPath)
    {
        $this->pdo = $pdo;
        parent::__construct($modelPath);
    }

    /**
     * @param string $classType
     * @return ClassGenerator
     */
    public function setClassType(string $classType): ClassGenerator
    {
        $this->classType = strtolower($classType);
        return $this;
    }

    /**
     * @param string $className
     * @return ClassGenerator
     */
    public function setClassName(string $className): ClassGenerator
    {
        $this->className = ucfirst($className);
        return $this;
    }

    /**
     * @param array $modelTypes
     * @return $this
     */
    public function setModelTypes(array $modelTypes)
    {
        $this->modelTypes = $modelTypes;
        return $this;
    }

    /**
     * @param string $tableName
     * @return ClassGenerator
     */
    public function setTableName(string $tableName): ClassGenerator
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @param string|null $namespace
     * @return bool
     */
    public function write(string $namespace = null, $rootModelNamespace = 'Model'): bool
    {
        return $this->writeModel($namespace, $rootModelNamespace);
    }
}
