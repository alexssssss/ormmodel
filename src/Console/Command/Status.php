<?php

namespace Alexssssss\OrmModel\Console\Command;

use Alexssssss\OrmModel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class Status extends AbstractCommand
{

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('status')
            ->setDescription('Compaire model with the database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->bootstrap($input, $output);

        $table = new Table($output);
        $table->setHeaders(['Entity', 'Table', 'Fields with error(s)']);

        $errorCount = 0;
        foreach ($this->getServices() as $serviceName) {
            list($fullClassName, $entity, $service) = $this->getServiceByFile($serviceName);
            $fieldsWithErrors = @$this->getdFieldsWithErrorsByEntity($entity);
            $errorCount += count($fieldsWithErrors);
            $table->addRow([$fullClassName, $service->getDataTable(), $fieldsWithErrors ? implode("\n", $fieldsWithErrors) : '-']);
        }
        $table->render();

        $output->writeln('<info>error count</info> ' . $errorCount);

        return 0;
    }

    protected function getServiceByFile($file)
    {
        $src = file_get_contents($file);

        $ns = $this->getNsBySrc($src);

        $fullClassName = $ns . '\\' . basename($file, '.php');

        /** @var OrmModel\Service\ServiceInterface $service */
        $service = \Alexssssss\OrmModel\Service\ServiceFactory::create($fullClassName);
        try {
            $entity = $service->create();
        } catch (\Throwable $t) {
            $entity = $service->create([], false);
        }

        return [get_class($entity), $entity, $service];
    }

    protected function getNsBySrc($src)
    {
        $tokens = token_get_all($src);
        $count = count($tokens);
        $i = 0;
        $namespace = '';
        $namespace_ok = false;
        while ($i < $count) {
            $token = $tokens[$i];
            if (is_array($token) && $token[0] === T_NAMESPACE) {
                // Found namespace declaration
                while (++$i < $count) {
                    if ($tokens[$i] === ';') {
                        $namespace_ok = true;
                        $namespace = trim($namespace);
                        break;
                    }
                    $namespace .= is_array($tokens[$i]) ? $tokens[$i][1] : $tokens[$i];
                }
                break;
            }
            $i++;
        }

        if (!$namespace_ok) {
            return null;
        } else {
            return $namespace;
        }
    }

    protected function getdFieldsWithErrorsByEntity(OrmModel\Entity\EntityInterface $entity)
    {
        $output = [];

        $tableCols = [];
        try {
            foreach ($entity->getService()->getDataAccess()->getTableCols() as $col) {
                $tableCols[] = $col['Field'];
            }
        } catch (\Throwable $th) {
            if (str_contains($th->getMessage(), 'Base table or view not found')) {
                $output[] = 'Table or view not found';
            } else {
                $output[] = $th->getMessage();
            }
            return $output;
        }
        $entityFields = $entity->toArray(true, true, true);

        foreach (array_keys($entityFields) as $field) {
            if (!in_array($field, $tableCols)) {
                $output[] = $field . ' - missing in DB';
            }
        }

        foreach ($tableCols as $col) {
            if (!in_array($col, array_keys($entityFields))) {
                $output[] = $col . ' - missing in Entity';
            }
        }

        return $output;
    }

    protected function getServices($dir = false)
    {
        if ($dir == false) {
            $dir = $this->getConfig()->getServicePath();
        }

        $files = array_diff(scandir($dir), ['..', '.']);

        $output = [];
        foreach ($files as $file) {
            if (is_file($dir . DIRECTORY_SEPARATOR . $file)) {
                // Only *.php files that dont start with 'abstract' and dont end with 'interface'
                if (strtolower(substr($file, -4)) === '.php' && strtolower(substr($file, -13, -4)) !== 'interface' && strtolower(substr($file, 0, 8)) !== 'abstract') {
                    $output[] = $dir . DIRECTORY_SEPARATOR . $file;
                }
            } else {
                $output = array_merge($output, (array)$this->getServices($dir . DIRECTORY_SEPARATOR . $file));
            }
        }

        return $output;
    }
}
