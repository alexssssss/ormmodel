<?php

namespace Alexssssss\OrmModel\Console\Config;

use Symfony\Component\Yaml\Yaml;

class Config implements ConfigInterface
{

    /**
     * @var array
     */
    private $values = [];

    /**
     * @var string
     */
    protected $configFilePath;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configArray, $configFilePath = null)
    {
        $this->configFilePath = $configFilePath;
        $this->values = $this->replaceTokens($configArray);
    }

    /**
     * Create a new instance of the config class using a Yaml file path.
     *
     * @param  string $configFilePath Path to the Yaml File
     * @throws \RuntimeException
     * @return Config
     */
    public static function fromYaml($configFilePath)
    {
        $configArray = Yaml::parse(file_get_contents($configFilePath));
        if (!is_array($configArray)) {
            throw new \RuntimeException(sprintf(
                'File \'%s\' must be valid YAML',
                $configFilePath
            ));
        }
        return new static($configArray, $configFilePath);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigFilePath()
    {
        return $this->configFilePath;
    }

    /**
     * {@inheritdoc}
     */
    public function getBootstrapFilePath()
    {
        if (!isset($this->values['paths']['bootstrapFile'])) {
            throw new \UnexpectedValueException('Bootstrap file path missing from config file');
        }

        return $this->values['paths']['bootstrapFile'];
    }

    /**
     * {@inheritdoc}
     */
    public function getModelNamespace()
    {
        if (!isset($this->values['paths']['modelNamespace'])) {
            throw new \UnexpectedValueException('Model namespace missing from config file');
        }

        return $this->values['paths']['modelNamespace'];
    }

    /**
     * {@inheritdoc}
     */
    public function getServicePath()
    {
        if (!isset($this->values['paths']['services'])) {
            throw new \UnexpectedValueException('Services path missing from config file');
        }

        return $this->values['paths']['services'];
    }

    /**
     * {@inheritdoc}
     */
    public function getModelPath()
    {
        if (!isset($this->values['paths']['model'])) {
            return realpath($this->getServicePath() . '/../');
        }

        return $this->values['paths']['model'];
    }

    /**
     * Replace tokens in the specified array.
     *
     * @param array $arr Array to replace
     * @return array
     */
    protected function replaceTokens($arr)
    {
        // Get environment variables
        // $_ENV is empty because variables_order does not include it normally
        $tokens = [];
        foreach ($_SERVER as $varname => $varvalue) {
            if (0 === strpos($varname, 'ORMMODEL_')) {
                $tokens['%%' . $varname . '%%'] = $varvalue;
            }
        }

        // OrmModel defined tokens (override env tokens)
        $tokens['%%ORMMODEL_CONFIG_PATH%%'] = $this->getConfigFilePath();
        $tokens['%%ORMMODEL_CONFIG_DIR%%'] = dirname($this->getConfigFilePath());

        // Recurse the array and replace tokens
        if (is_array($arr)) {
            return $this->recurseArrayForTokens($arr, $tokens);
        }

        return $arr;
    }

    /**
     * Recurse an array for the specified tokens and replace them.
     *
     * @param array $arr Array to recurse
     * @param array $tokens Array of tokens to search for
     * @return array
     */
    protected function recurseArrayForTokens($arr, $tokens)
    {
        $out = [];
        foreach ($arr as $name => $value) {
            if (is_array($value)) {
                $out[$name] = $this->recurseArrayForTokens($value, $tokens);
                continue;
            }
            if (is_string($value)) {
                foreach ($tokens as $token => $tval) {
                    $value = str_replace($token, $tval, $value);
                }
                $out[$name] = $value;
                continue;
            }
            $out[$name] = $value;
        }
        return $out;
    }

    public function offsetSet($id, $value) : void
    {
        $this->values[$id] = $value;
    }

    #[\ReturnTypeWillChange]
    public function offsetGet($id)
    {
        if (!array_key_exists($id, $this->values)) {
            throw new \InvalidArgumentException(sprintf('Identifier "%s" is not defined.', $id));
        }

        return $this->values[$id] instanceof \Closure ? $this->values[$id]($this) : $this->values[$id];
    }

    public function offsetExists($id) : bool
    {
        return isset($this->values[$id]);
    }

    #[\ReturnTypeWillChange]
    public function offsetUnset($id) : void
    {
        unset($this->values[$id]);
    }
}
