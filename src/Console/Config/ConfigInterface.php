<?php

namespace Alexssssss\OrmModel\Console\Config;

interface ConfigInterface extends \ArrayAccess
{

    /**
     * Class Constructor
     *
     * @param array $configArray Config Array
     * @param string $configFilePath Optional File Path
     */
    public function __construct(array $configArray, $configFilePath = null);

    /**
     * Gets the config file path.
     *
     * @return string
     */
    public function getConfigFilePath();

    /**
     * Gets the path of the entity files.
     *
     * @return string
     */
    public function getServicePath();

    /**
     * Gets the path of the bootstrap file.
     *
     * @return string
     */
    public function getBootstrapFilePath();
}
