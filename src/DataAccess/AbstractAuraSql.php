<?php

namespace Alexssssss\OrmModel\DataAccess;

use Alexssssss\OrmModel\ConnectionFactoryInterface;
use Alexssssss\OrmModel\Helper\Profiler;
use Alexssssss\OrmModel\Helper\Rules;

abstract class AbstractAuraSql implements DataAccessInterface
{
    /**
     * De database tabel waarvan de objecten gemaakt moeten worden
     *
     * @var string
     */
    protected $table;

    /**
     * Prefix voor alle velden
     *
     * @var string
     */
    protected $fieldPrefix;

    /**
     * Array met de kolom definities als self::getTableCols() aangeroepen is geweest
     *
     * @var array
     */
    protected $tableColls;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $dsn;

    /**
     * @var \Aura\Sql\ExtendedPdoInterface
     */
    protected $writeAdapter;

    /**
     * @var \Aura\Sql\ExtendedPdoInterface
     */
    protected $readAdapter;

    /**
     * @var ConnectionFactoryInterface
     */
    protected $connectionFactory;

    protected $whereKeysPattern = '/<|>|=| IS | IN | IN\(|LIKE | EXISTS |EXISTS\(| REGEXP |REGEXP\(/';

    /**
     * AbstractAuraSql constructor.
     *
     * @param ConnectionFactoryInterface $connectionFactory
     */
    public function __construct(ConnectionFactoryInterface $connectionFactory)
    {
        $this->connectionFactory = $connectionFactory;
        $this->name = get_called_class();
    }

    protected function getAdapter(): \Aura\Sql\ExtendedPdoInterface
    {
        return $this->connectionFactory->getAdapter();
    }

    protected function getReadAdapter(string $name = null): \Aura\Sql\ExtendedPdoInterface
    {
        if ($this->writeAdapter !== null) {
            return $this->writeAdapter;
        } elseif ($this->readAdapter === null) {
            $this->readAdapter = $this->connectionFactory->getReadAdapter($name);
        }

        return $this->readAdapter;
    }

    protected function getWriteAdapter(string $name = null): \Aura\Sql\ExtendedPdoInterface
    {
        if ($this->writeAdapter === null) {
            $this->writeAdapter = $this->connectionFactory->getWriteAdapter($name);
        }

        return $this->writeAdapter;
    }

    public function getDsn(): string
    {
        if ($this->dsn === null) {
            $adapter = $this->getAdapter();

            $dbName = $adapter->fetchValue('SELECT database()');
            $host = $adapter->fetchValue('SHOW VARIABLES WHERE Variable_name = \'hostname\';');

            $this->dsn = 'mysql:host=' . $host . ';dbname=' . $dbName;
        }

        return $this->dsn;
    }

    /**
     * Een object ophalen op basis van hun ID
     *
     * @param integer $id
     *
     * @return array
     */
    public function getById(int $id): array
    {
        $query = 'SELECT `' . $this->table . '`.* FROM `' . $this->table . '` WHERE `' . $this->table . '`.`' . $this->fieldPrefix . 'id` = :id';
        $bind = ['id' => $id];

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $line = $adapter->fetchOne($query, $bind);

        if ($e !== null) {
            $e->stop();
        }

        return $line === false ? [] : $this->removePrefixFromFields($line);
    }

    /**
     * @param array $rules
     * @param string|null $where
     * @param array $bind
     *
     * @return int
     */
    public function countBy(array $rules = [], string $where = null, array $bind = []): int
    {
        if (count($rules) > 0) {
            if (isset($rules['limit']) && is_int($rules['limit'])) {
                $limit = $rules['limit'];
                unset($rules['limit']);

                if (isset($rules['page']) && is_int($rules['page'])) {
                    $page = $rules['page'];
                    unset($rules['page']);
                }
            }

            if (isset($rules['sort']) && is_array($rules['sort'])) {
                $sort = $rules['sort'];
                unset($rules['sort']);
            }

            $allRules = Rules::process($rules);

            list($query, $extraBind) = Rules::processSimpleToWhereAndBind($allRules, false, $this->fieldPrefix);

            if (empty($query)) {
                $query = $where;
            } else {
                $query .= (!empty($where) ? ' AND ' . $where : null);
                $bind = array_merge($bind, $extraBind);
            }

            if (empty($query) && (isset($sort) || isset($limit))) {
                $query = 'true = true';
            }

            if (isset($sort)) {
                $sorts = [];
                foreach ($sort as $field => $order) {
                    $sorts[] = '`' . $this->table . '`.`' . $this->fieldPrefix . $field . '` ' . strtoupper($order);
                }

                $query .= ' ORDER BY ' . implode(', ', $sorts);
            }

            if (isset($limit)) {
                $query .= ' LIMIT ' . ((($page ?? 1) - 1) * $limit) . ', ' . $limit;
            }
        } else {
            $query = $where;
        }

        return $this->count($query, $bind);
    }

    /**
     * @param array $rules
     * @param string|null $where
     * @param array $bind
     * @param bool $onlyFirstRow
     *
     * @return array
     */
    public function getBy(array $rules = [], string $where = null, array $bind = [], $onlyFirstRow = false): iterable
    {
        if (count($rules) > 0) {
            if (isset($rules['limit']) && is_int($rules['limit'])) {
                $limit = $rules['limit'];
                unset($rules['limit']);

                if (isset($rules['page']) && is_int($rules['page'])) {
                    $page = $rules['page'];
                    unset($rules['page']);
                }
            }

            if (isset($rules['sort']) && is_array($rules['sort'])) {
                $sort = $rules['sort'];
                unset($rules['sort']);
            }

            $allRules = Rules::process($rules);

            list($query, $extraBind) = Rules::processSimpleToWhereAndBind($allRules, false, $this->fieldPrefix);

            if (empty($query)) {
                $query = $where;
            } else {
                $query .= (!empty($where) ? ' AND ' . $where : null);
                $bind = array_merge($bind, $extraBind);
            }

            if (empty($query) && (isset($sort) || isset($limit))) {
                $query = 'true = true';
            }

            if (isset($sort)) {
                $sorts = [];
                foreach ($sort as $field => $order) {
                    $sorts[] = '`' . $this->table . '`.`' . $this->fieldPrefix . $field . '` ' . strtoupper($order);
                }

                $query .= ' ORDER BY ' . implode(', ', $sorts);
            }

            if (isset($limit)) {
                $query .= ' LIMIT ' . ((($page ?? 1) - 1) * $limit) . ', ' . $limit;
            }
        } else {
            $query = $where;
        }

        return $this->get($query, $bind, $onlyFirstRow);
    }

    /**
     * Meerdere objecten ophalen
     *
     * @param string $where
     * @param array $bind
     * @param bool $onlyFirstRow
     *
     * @return iterable
     */
    public function get(string $where = null, array $bind = [], $onlyFirstRow = false): iterable
    {
        $query = 'SELECT `' . $this->table . '`.* FROM `' . $this->table . '` ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        if ($onlyFirstRow && ($line = $adapter->fetchOne($query, $bind))) {
            $data = $this->removePrefixFromFields($line);
        } elseif (!$onlyFirstRow && ($data = $adapter->yieldAll($query, $bind))) {
            if ($this->fieldPrefix) {
                $data = iterator_to_array($data);
                foreach ($data as $key => $line) {
                    $data[$key] = $this->removePrefixFromFields($line);
                }
            }
        } else {
            $data = [];
        }

        if ($e !== null) {
            $e->stop();
        }

        return $data;
    }

    /**
     *
     * @param string $where
     * @param array $bind
     *
     * @return integer
     */
    public function count(string $where = null, array $bind = []): int
    {
        $query = 'SELECT COUNT(`' . $this->table . '`.`' . $this->fieldPrefix . 'id`) FROM `' . $this->table . '` ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = (int)$adapter->fetchValue($query, $bind);

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    /**
     * Meerdere ID's ophalen op basis van $where en $bind
     *
     * @param string $where
     * @param array $bind
     *
     * @return array
     */
    public function getIds(string $where = null, array $bind = []): array
    {
        $query = 'SELECT `' . $this->table . '`.`' . $this->fieldPrefix . 'id` FROM `' . $this->table . '` ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        if ($data = $adapter->fetchAll($query, $bind)) {
            if (is_array($data)) {
                foreach ($data as $key => $line) {
                    $data[$key] = $this->removePrefixFromFields($line);
                }
            }
        } else {
            $data = [];
        }

        if ($e !== null) {
            $e->stop();
        }

        return $data;
    }

    /**
     *
     * @param array $fields
     * @param bool $withIgnore
     *
     * @return integer|bool
     * @throws \Exception
     */
    public function insert(array $fields = [], $withIgnore = false): ?int
    {
        $this->checkSetup();

        if (empty($fields)) {
            $fields = ['id' => null];
        }

        $fieldsWithPrefix = $this->addPrefixToFields($fields);

        $fieldNames = [];
        $bind = [];
        foreach ($fieldsWithPrefix as $fieldName => $value) {
            $fieldNames[] = $fieldName;
            $bind[str_replace('-', '', $fieldName)] = $value;
        }

        $query = 'INSERT ' . ($withIgnore ? 'IGNORE ' : null) . 'INTO `' . $this->table . '` (`' . implode('`, `', $fieldNames) . '`) VALUES (:' . implode(', :', array_keys($bind)) . ')';

        $adapter = $this->getWriteAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = $adapter->perform($query, $bind) === false ? null : ($fields['id'] ?? $adapter->lastInsertId());

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    /**
     *
     * @param integer|integer[] $ids
     * @param array $fields
     *
     * @return bool
     * @throws \Exception
     */
    public function update($ids, array $fields = []): bool
    {
        if (!is_int($ids) && !(is_array($ids) && ctype_digit((string)implode('', $ids)))) {
            throw new \Exception('The $ids param of ' . get_class($this) . '::update() only supports an integer as ID or an array of integers.');
        } elseif (is_array($ids) && count($ids) > 1 && isset($fields['id'])) {
            throw new \Exception('Cant set multple ID\'s to the same value with  ' . get_class($this) . '::update()');
        }

        $this->checkSetup();

        if (empty($fields)) {
            return true;
        } else {
            $fieldsWithPrefix = $this->addPrefixToFields($fields);

            $query = 'UPDATE `' . $this->table . '` SET ';

            $fieldsQuery = [];
            $bind = [];

            foreach ($fieldsWithPrefix as $fieldName => $value) {
                $fieldsQuery[] = '`' . $fieldName . '` = :' . str_replace('-', '', $fieldName);
                $bind[str_replace('-', '', $fieldName)] = $value;
            }

            $query .= implode(', ', $fieldsQuery) . ' WHERE `' . $this->fieldPrefix . 'id` ' . (is_array($ids) ? 'IN (:id)' : '= :id');

            $bind['id'] = $ids;

            $adapter = $this->getWriteAdapter();

            $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

            $return = $adapter->perform($query, $bind) === false ? false : true;

            if ($e !== null) {
                $e->stop();
            }

            return $return;
        }
    }

    /**
     *
     * @param integer|integer[] $ids
     *
     * @return bool
     * @throws \Exception
     */
    public function delete($ids): bool
    {
        if (!is_int($ids) && !(is_array($ids) && ctype_digit((string)implode('', $ids)))) {
            throw new \Exception('The $ids param of ' . get_class($this) . '::delete() only supports an integer as ID or an array of integers.');
        }

        $this->checkSetup();

        $query = 'DELETE FROM `' . $this->table . '` WHERE `' . $this->table . '`.`' . $this->fieldPrefix . 'id` ' . (is_array($ids) ? 'IN (:id)' : '= :id');
        $bind = ['id' => $ids];

        $adapter = $this->getWriteAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = $adapter->perform($query, $bind) === false ? false : true;

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    /**
     *
     * @throws \Exception
     */
    protected function checkSetup()
    {
        if (empty($this->table)) {
            throw new \Exception('Class ' . get_class($this) . ' needs to have the property \'table\' set correctly.');
        }
    }

    /**
     *
     * @return array
     */
    public function getTableCols(): array
    {
        // store table colls in the class for small performance boost
        if ($this->tableColls === null) {
            $adapter = $this->getReadAdapter();

            $query = 'SHOW COLUMNS FROM `' . $this->table . '`';

            $e = Profiler::addCall($this->name, __FUNCTION__, compact('query'));

            $tableColls = $adapter->fetchAll($query);

            if ($e !== null) {
                $e->stop();
            }

            foreach ($tableColls as $key => $row) {
                $tableColls[$key]['Field'] = ($this->fieldPrefix && strpos($row['Field'], $this->fieldPrefix) === 0 ? substr($row['Field'], strlen($this->fieldPrefix)) : $row['Field']);
            }

            $this->tableColls = $this->removePrefixFromFields($tableColls);

            Profiler::log('Called ' . $this->name . '::getTableCols and queried the DB and stored values in cache.', ['tableColls' => $this->tableColls]);
        } else {
            Profiler::log('Called ' . $this->name . '::getTableCols and used the cached value.', ['tableColls' => $this->tableColls]);
        }

        return $this->tableColls;
    }

    /**
     *
     * @param array $fields
     *
     * @return array
     */
    protected function addPrefixToFields(array $fields = [])
    {
        if ($this->fieldPrefix && !empty($fields)) {
            return array_combine(array_map(function ($key) {
                return $this->fieldPrefix . $key;
            }, array_keys($fields)), $fields);
        } else {
            return $fields;
        }
    }

    /**
     *
     * @param array $fields
     *
     * @return array
     */
    protected function removePrefixFromFields($fields = [])
    {
        if ($this->fieldPrefix && !empty($fields)) {
            return array_combine(
                array_map(function ($key) {
                    return strpos($key, $this->fieldPrefix) === 0 ? substr($key, strlen($this->fieldPrefix)) : $key;
                }, array_keys($fields)),
                $fields
            );
        } else {
            return $fields;
        }
    }

    /**
     *
     * @return string
     */
    public function getFieldPrefix(): string
    {
        return (string)$this->fieldPrefix;
    }

    /**
     *
     * @return string
     */
    public function getTable(): string
    {
        return (string)$this->table;
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function min($field, string $where = null, array $bind = [])
    {
        $query = 'SELECT MIN(`' . $this->table . '`.`' . $this->fieldPrefix . $field . '`) FROM `' . $this->table . '`  ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = $adapter->fetchValue($query, $bind);

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function max($field, string $where = null, array $bind = [])
    {
        $query = 'SELECT MAX(`' . $this->table . '`.`' . $this->fieldPrefix . $field . '`) FROM `' . $this->table . '`  ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = $adapter->fetchValue($query, $bind);

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function avg($field, string $where = null, array $bind = [])
    {
        $query = 'SELECT AVG(`' . $this->table . '`.`' . $this->fieldPrefix . $field . '`) FROM `' . $this->table . '`  ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = $adapter->fetchValue($query, $bind);

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function sum($field, string $where = null, array $bind = [])
    {
        $query = 'SELECT SUM(`' . $this->table . '`.`' . $this->fieldPrefix . $field . '`) FROM `' . $this->table . '`  ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = $adapter->fetchValue($query, $bind);

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function distinct($field, string $where = null, array $bind = []): array
    {
        $query = 'SELECT DISTINCT `' . $this->table . '`.`' . $this->fieldPrefix . $field . '` FROM `' . $this->table . '`  ' . (preg_match($this->whereKeysPattern, $where) ? 'WHERE ' . $where : $where);

        $adapter = $this->getReadAdapter();

        $e = Profiler::addCall($this->name, __FUNCTION__, compact('query', 'bind'));

        $return = $adapter->fetchCol($query, $bind);

        if ($e !== null) {
            $e->stop();
        }

        return $return;
    }

    public function __get($name)
    {
        if ($name == 'adapter') {
            trigger_error('The property "' . $name . '" is deprecated, use connectionLocator', E_USER_DEPRECATED);

            return $this->getAdapter();
        }

        throw new \Exception('Property "' . $name . '" is not defined');
    }
}
