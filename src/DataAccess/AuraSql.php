<?php

namespace Alexssssss\OrmModel\DataAccess;

use Alexssssss\OrmModel\Helper\Profiler;

class AuraSql extends AbstractAuraSql
{

    /**
     *
     * @param string $table
     *
     * @return AuraSql
     */
    public function setTable($table)
    {
        Profiler::log('Alexssssss\OrmModel\DataAccess\AuraSql set table to "{table}".', compact('table'));

        $this->table = $table;
        $this->name .= '(' . $table . ')';
        return $this;
    }

    /**
     *
     * @param string $fieldPrefix
     *
     * @return AuraSql
     */
    public function setFieldPrefix($fieldPrefix)
    {
        Profiler::log('Alexssssss\OrmModel\DataAccess\AuraSql set field prefix to to "{fieldPrefix}".', compact('fieldPrefix'));

        $this->fieldPrefix = $fieldPrefix;
        return $this;
    }
}
