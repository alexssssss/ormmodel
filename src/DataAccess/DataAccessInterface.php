<?php

namespace Alexssssss\OrmModel\DataAccess;

interface DataAccessInterface
{

    /**
     * @return string
     */
    public function getDsn(): string;

    /**
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array;

    /**
     * @param string|null $where
     * @param array $bind
     *
     * @param bool $onlyFirstRow
     * @return \iterable
     */
    public function get(string $where = null, array $bind = [], $onlyFirstRow = false): iterable;

    /**
     * @param array $rules
     * @param string|null $where
     * @param array $bind
     *
     * @return int
     */
    public function countBy(array $rules = [], string $where = null, array $bind = []): int;

    /**
     * @param string|null $where
     * @param array $bind
     *
     * @return int
     */
    public function count(string $where = null, array $bind = []): int;

    /**
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function getIds(string $where = null, array $bind = []): array;

    /**
     * @param array $fields
     * @param bool $withIgnore
     * @return int
     */
    public function insert(array $fields = [], $withIgnore = false): ?int;

    /**
     * @param integer|integer[] $ids
     * @param array $fields
     * @return bool
     */
    public function update($ids, array $fields = []): bool;

    /**
     * @param integer|integer[] $ids
     * @return bool
     */
    public function delete($ids): bool;

    /**
     * @return array
     */
    public function getTableCols(): array;

    /**
     * @param array $rules
     * @param string|null $where
     * @param array $bind
     * @param bool $onlyFirstRow
     * @return \iterable
     */
    public function getBy(array $rules = [], string $where = null, array $bind = [], $onlyFirstRow = false): iterable;

    /**
     * @return string
     */
    public function getFieldPrefix(): string;

    /**
     * @return string
     */
    public function getTable(): string;

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     * @return mixed
     */
    public function min($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     * @return mixed
     */
    public function max($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     * @return mixed
     */
    public function avg($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     * @return mixed
     */
    public function sum($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     * @return array
     */
    public function distinct($field, string $where = null, array $bind = []): array;
}
