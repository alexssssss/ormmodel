<?php

namespace Alexssssss\OrmModel\DataMapper;

use Alexssssss\OrmModel;
use Alexssssss\OrmModel\DataAccess\AbstractDataAccess;
use Alexssssss\OrmModel\DataAccess\DataAccessInterface;

/**
 * AbstractDataMapper voor het koppelen van de database aan de data objecten
 */
abstract class AbstractDataMapper implements DataMapperInterface
{

    /**
     *
     * @var array
     */
    protected static $cache = [];
    /**
     *
     * @var DataAccessInterface
     */
    protected $dataAccess;
    /**
     *
     * @var OrmModel\Entity\EntityFactory
     */
    protected $entityFactory;
    /**
     * The name of the entity class for the entity factory without namespaces.
     *
     * @var string
     */
    protected $entityClassName;
    /**
     * The name of the entity class for the entity factory without namespaces.
     *
     * @var string
     */
    protected $wrapperClassName;
    /**
     *
     * @var OrmModel\Service\ServiceInterface
     */
    protected $service = null;

    /**
     *
     * @param OrmModel\Entity\EntityFactory $entityFactory
     */
    public function __construct(OrmModel\Entity\EntityFactory $entityFactory)
    {
        $this->entityFactory = $entityFactory;
    }

    /**
     *
     * @param DataAccessInterface $dataAccess
     * @return AbstractDataMapper
     */
    public function setDataAccess(OrmModel\DataAccess\DataAccessInterface $dataAccess): DataMapperInterface
    {
        $this->dataAccess = $dataAccess;
        return $this;
    }

    /**
     *
     * @param OrmModel\Service\ServiceInterface $service
     * @return AbstractDataMapper
     */
    public function setService(OrmModel\Service\ServiceInterface $service): DataMapperInterface
    {
        $this->service = $service;
        return $this;
    }

    /**
     *
     * @param \iterable $rows
     * @param bool $raw
     * @return OrmModel\ObjectStorageInterface
     * @throws OrmModel\Exception\Limit
     * @throws \Exception
     */
    public function rowsToEntities(iterable &$rows = [], bool $raw = true): OrmModel\ObjectStorageInterface
    {
        $this->checkSetup();
        $objectStorage = $this->createObjectStorageObject();

        $limit = 5000;
        if (!defined('ALEXSSSSSS_ORM_MODEL_IGNORE_LIMIT') && is_array($rows) && count($rows) > $limit) {
            throw new OrmModel\Exception\Limit(get_class($this) . ' is trying to parse ' . count($rows) . ' rows to entities, thats more then the limit of ' . $limit);
        }

        foreach ($rows as $row) {
            $entity = $this->rowToEntity($row, $raw, false);
            $objectStorage->attach($entity);
        }

        return $objectStorage;
    }

    /**
     *
     * @throws \Exception
     */
    protected function checkSetup()
    {
        if ($this->dataAccess === null) {
            throw new \Exception('Class \'' . get_class($this) . '\' needs to have the property \'dataAccess\' set correctly.');
        }

        if ($this->service === null) {
            throw new \Exception('Class \'' . get_class($this) . '\' needs to have the property \'service\' set correctly.');
        }

        if (empty($this->entityClassName)) {
            throw new \Exception('Class \'' . get_class($this) . '\' needs to have the property \'entityClassName\' set correctly.');
        }

        if (empty($this->wrapperClassName)) {
            throw new \Exception('Class \'' . get_class($this) . '\' needs to have the property \'wrapperClassName\' set correctly.');
        }
    }

    /**
     * @codeCoverageIgnore
     * @return OrmModel\ObjectStorage|OrmModel\Entity\EntityInterface[]
     * @throws \Exception
     */
    protected function createObjectStorageObject(): OrmModel\ObjectStorage
    {
        return new OrmModel\ObjectStorage([], $this->service);
    }

    /**
     *
     * @param array $row
     * @param bool $raw
     * @param bool $checkSetup
     * @return OrmModel\Entity\EntityInterface
     * @throws \Exception
     */
    public function rowToEntity(array $row, bool $raw = true, bool $checkSetup = true): OrmModel\Entity\EntityInterface
    {
        if ($checkSetup === true) {
            $this->checkSetup();
        }
        
        $entityFields = [];
        $additionalFields = [];
        foreach ($row as $key => $value) {
            if ($key[0] === '_') {
                $additionalFields[substr($key, 1)] = $value;
            } else {
                $entityFields[$key] = $value;
            }
        }

        if (isset($row['id']) && $row['id'] !== null && $this->isCached((int)$row['id'])) {
            $entity = $this->getFromCache((int)$row['id'], false);
        } else {
            $entity = $this->entityFactory->create($this->service, $this->entityClassName, $entityFields, $raw);

            if (isset($row['id']) && $row['id'] !== null) {
                self::$cache[$this->entityClassName][(int)$row['id']] = $entity;
            }
        }
        
        $entity->setAdditonalFields($additionalFields);
        return $entity;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function isCached(int $id): bool
    {
        if (!isset(self::$cache[$this->entityClassName])) {
            self::$cache[$this->entityClassName] = [];
        } elseif (isset(self::$cache[$this->entityClassName][$id])) {
            if (self::$cache[$this->entityClassName][$id]->id === $id) {
                return true;
            } else {
                $cachedEntity = self::$cache[$this->entityClassName][$id];
                unset(self::$cache[$this->entityClassName][$id]);

                if ($cachedEntity->id !== null) {
                    if (isset(self::$cache[$this->entityClassName][(int)$cachedEntity->id]) &&
                        $cachedEntity->getUuid() !== self::$cache[$this->entityClassName][(int)$cachedEntity->id]->getUuid()
                    ) {
                        throw new \Exception('Class \'' . get_class($this) . '\' entity with ID #' . $id . ' already in cache.');
                    }

                    self::$cache[$this->entityClassName][(int)$cachedEntity->id] = $cachedEntity;
                }
            }
        }

        return false;
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function deleteFromCache(int $id)
    {
        if ($this->isCached($id) == true) {
            unset(self::$cache[$this->entityClassName][$id]);
        }
    }

    /**
     * @param int $id
     * @param bool $checkFirst
     * @return OrmModel\Entity\EntityInterface
     * @throws \Exception
     */
    public function getFromCache(int $id, bool $checkFirst = true): OrmModel\Entity\EntityInterface
    {
        if ($checkFirst === true && $this->isCached($id) === false) {
            throw new \Exception('Class \'' . get_class($this) . '\' no entity with ID #' . $id . ' in cache.');
        }

        return self::$cache[$this->entityClassName][$id];
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @return bool
     * @throws \Exception
     */
    public function delete(OrmModel\Entity\EntityInterface &$entity): bool
    {
        $this->checkSetup();

        if ($entity->id === null) {
            return true;
        } else {
            $this->deleteFromCache($entity->id);

            return $this->dataAccess->delete($entity->id);
        }
    }

    /**
     * @param integer|integer[] $ids
     * @return bool
     * @throws \Exception
     */
    public function deleteById($ids): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        if (!ctype_digit((string)implode('', $ids))) {
            throw new \Exception('The $ids param of \'' . get_class($this) . '\'::deleteById() only supports an integer as ID or an array of integers.');
        }

        foreach ($ids as $id) {
            if ($this->isCached($id)) {
                $entity = $this->getFromCache($id);

                $entity->clearId(true);
                $entity->setPrevious();
                $this->deleteFromCache($id);
            }
        }

        return $this->dataAccess->delete($ids);
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @return bool
     */
    public function insert(OrmModel\Entity\EntityInterface &$entity): bool
    {
        $this->checkSetup();

        $fields = $entity->toArrayForSave();

        foreach ($fields as $field => $value) {
            if ($value === null) {
                unset($fields[$field]);
            }
        }
        
        if ($id = $this->dataAccess->insert($fields)) {
            $entity->id = $id;

            if ($this->isCached($id) == false) {
                self::$cache[$this->entityClassName][$id] = $entity;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @return bool
     * @throws \Exception
     */
    public function update(OrmModel\Entity\EntityInterface &$entity): bool
    {
        $this->checkSetup();

        $fields = $entity->toArrayForSave();

        return $this->dataAccess->update($entity->id, $fields);
    }

    /**
     * @param integer|integer[] $ids
     * @param $values
     * @return bool
     * @throws \Exception
     */
    public function updateById($ids, $values): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        if (!ctype_digit((string)implode('', $ids))) {
            throw new \Exception('The $ids param of \'' . get_class($this) . '\'::updateById() only supports an integer as ID or an array of integers.');
        }

        foreach ($ids as $id) {
            if ($this->isCached($id)) {
                $entity = $this->getFromCache($id);

                foreach ($values as $key => $value) {
                    $entity->{$key} = $value;
                }

                $entity->validate();

                $entity->setPrevious();
            }
        }

        foreach ($values as $key => &$value) {
            if (is_object($value) && $value instanceof \DateTime) {
                $value = (!empty($value->format('u')) ? $value->format('Y-m-d H:i:s.u') : $value->format('Y-m-d H:i:s'));
            }
        }

        return $this->dataAccess->update($ids, $values);
    }

    /**
     * check to see if a entity is a belongs to this service
     */
    public function checkEntityType($entity): bool
    {
        return $entity instanceof $this->entityClassName;
    }

    /**
     * @return string
     */
    public function getEntityClassName(): string
    {
        return $this->entityClassName;
    }

    /**
     * @return string
     */
    public function getWrapperClassName(): string
    {
        return $this->wrapperClassName;
    }

    public function min($field, string $where = null, array $bind = [])
    {
        $value = $this->dataAccess->min($field, $where, $bind);
        return $this->convertData($field, $value);
    }

    public function max($field, string $where = null, array $bind = [])
    {
        $value = $this->dataAccess->max($field, $where, $bind);
        return $this->convertData($field, $value);
    }

    public function avg($field, string $where = null, array $bind = [])
    {
        $value = $this->dataAccess->avg($field, $where, $bind);
        return $this->convertData($field, $value);
    }

    public function sum($field, string $where = null, array $bind = [])
    {
        $value = $this->dataAccess->sum($field, $where, $bind);
        return $this->convertData($field, $value);
    }

    public function convertData($field, $value)
    {
        $field = str_replace('-', '', $field);

        $data = [$field => $value];

        OrmModel\Helper\Data::changeType($this->entityClassName, $data);

        return $data[$field];
    }
}
