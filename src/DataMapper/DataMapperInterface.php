<?php

namespace Alexssssss\OrmModel\DataMapper;

use Alexssssss\OrmModel;

/**
 * Interface DataMapperInterface
 * @package Alexssssss\OrmModel\DataMapper
 */
interface DataMapperInterface
{

    /**
     *
     * @param OrmModel\Entity\EntityFactory $entityFactory
     */
    public function __construct(OrmModel\Entity\EntityFactory $entityFactory);

    /**
     *
     * @param OrmModel\DataAccess\DataAccessInterface $dataAccess
     */
    public function setDataAccess(OrmModel\DataAccess\DataAccessInterface $dataAccess): DataMapperInterface;

    /**
     *
     * @param OrmModel\Service\ServiceInterface $service
     */
    public function setService(OrmModel\Service\ServiceInterface $service): DataMapperInterface;

    /**
     *
     * @param array $rows
     * @param bool $raw
     * @return OrmModel\ObjectStorageInterface
     */
    public function rowsToEntities(array &$rows = [], bool $raw = true): OrmModel\ObjectStorageInterface;

    /**
     *
     * @param array $row
     * @param bool $raw
     * @return OrmModel\Entity\EntityInterface
     */
    public function rowToEntity(array $row, bool $raw = true): OrmModel\Entity\EntityInterface;

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @return bool
     */
    public function insert(OrmModel\Entity\EntityInterface &$entity): bool;

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @return bool
     */
    public function update(OrmModel\Entity\EntityInterface &$entity): bool;

    /**
     * @param integer|integer[] $ids
     * @param $values
     * @return bool
     */
    public function updateById($ids, $values): bool;

    /**
     * check to see if a entity is a belongs to this service
     */
    public function checkEntityType($entity): bool;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @return bool
     */
    public function delete(OrmModel\Entity\EntityInterface &$entity): bool;

    /**
     * @param integer|integer[] $ids
     * @return bool
     */
    public function deleteById($ids): bool;

    /**
     *
     * @param int $id
     * @return bool
     */
    public function isCached(int $id): bool;

    /**
     *
     * @param int $id
     * @return OrmModel\Entity\EntityInterface
     */
    public function getFromCache(int $id): OrmModel\Entity\EntityInterface;

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteFromCache(int $id);

    /**
     * @return string
     */
    public function getEntityClassName(): string;

    /**
     * @return string
     */
    public function getWrapperClassName(): string;

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function min($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function max($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function avg($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function sum($field, string $where = null, array $bind = []);
}
