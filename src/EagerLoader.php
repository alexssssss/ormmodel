<?php

namespace Alexssssss\OrmModel;

use Alexssssss\OrmModel\Entity\EntityInterface;
use Alexssssss\OrmModel\Entity\Proxy\AbstractEntity;
use Alexssssss\OrmModel\Helper\Relation;

/**
 * Class EagerLoader
 * @package Alexssssss\OrmModel
 */
class EagerLoader
{
    protected static $cacheHasMany = [];
    protected static $cacheHasOne = [];
    protected static $cacheBelongsTo = [];
    protected $ownerService;
    protected $relations = [];

    /**
     *
     * @param Service\ServiceInterface $ownerService
     * @param array $relations
     * @throws \Exception
     */
    public function __construct(Service\ServiceInterface $ownerService, array $relations = [])
    {
        $this->ownerService = $ownerService;

        $this->relations = Helper\DotNotation::toArray($relations);
    }

    /**
     *
     * @param string $fun
     * @param array $params
     * @return mixed
     * @throws Exception\UndefinedMethod
     * @throws \Exception
     */
    public function __call($fun, $params)
    {

        if (is_callable([$this->ownerService, $fun])) {
            $returnObject = call_user_func_array([$this->ownerService, $fun], $params);
        } else {
            throw new Exception\UndefinedMethod('Call to undefined method ' . get_class($this) . '::' . $fun . '()');
        }

        if ($fun === 'create') {
            return $returnObject;
        } elseif ($returnObject instanceof EntityInterface) {
            if ($returnObject instanceof FakeObjectInterface) {
                return $this->processOne($returnObject->getRealObject());
            } else {
                return $this->processOne($returnObject);
            }
        } elseif ($returnObject instanceof ObjectStorageInterface) {
            if ($returnObject instanceof FakeObjectInterface) {
                return $this->processMany($returnObject->getRealObject());
            } else {
                return $this->processMany($returnObject);
            }
        } else {
            return $returnObject;
        }
    }

    /**
     *
     * @param ObjectStorage|Entity\EntityInterface[] $objects
     * @return array|false
     */
    protected function getOfObjectsIds(ObjectStorage $objects)
    {
        $ids = [];
        foreach ($objects as $object) {
            if ($object->id != null) {
                $ids[] = $object->id;
            }
        }

        return !empty($ids) ? $ids : false;
    }

    /**
     *
     * @param Service\ServiceInterface $service
     * @param string $where
     * @param array $bind
     * @param array $subRelations
     * @return ObjectStorage|Entity\EntityInterface[]|false
     */
    protected function getChildObjects(Service\ServiceInterface $service, $where, array $bind, array $subRelations)
    {
        if (!empty($subRelations)) {
            $eagerLoadedObjects = $service->with(Helper\DotNotation::fromArray($subRelations))->get($where, $bind);
        } else {
            $eagerLoadedObjects = $service->get($where, $bind);
        }

        return $eagerLoadedObjects->count() !== 0 ? $eagerLoadedObjects : false;
    }

    /**
     *
     * @param ObjectStorage $objects
     * @return ObjectStorage|Entity\EntityInterface[]
     * @throws \Exception
     */
    protected function processMany(ObjectStorage &$objects)
    {
        if ($objects->count() > 0 && ($ids = $this->getOfObjectsIds($objects))) {
            $service = $objects->getFirst()->getService();

            foreach ($this->relations as $relation => $subRelations) {
                foreach ($service->getHasMany() as $rel) {
                    if ($rel['storeInField'] === $relation) {
                        $serviceModel = $rel['service']::getInstance();

                        if (!isset(self::$cacheHasMany[$rel['service']])) {
                            self::$cacheHasMany[$rel['service']] = [];
                        }

                        $bind = ['ids' => $ids];
                        $where = '`' . $serviceModel->getDataTable() . '`.`' . $serviceModel->getDataFieldPrefix() . $rel['foreignKeyField'] . '` IN (:ids)' . (isset($rel['conditions']) ? ' AND ' . $rel['conditions'] : '');

                        if ($eagerLoadedObjects = $this->getChildObjects($serviceModel, $where, $bind, $subRelations)) {
                            $indexOfEagerLoadedObjects = $eagerLoadedObjects->buildIndex($rel['foreignKeyField']);
                        }

                        foreach ($objects as &$object) {
                            $key = $object->id . '-' . $rel['storeInField'];
                            if (!in_array($key, self::$cacheHasMany[$rel['service']])) {
                                self::$cacheHasMany[$rel['service']][] = $key;

                                if ($eagerLoadedObjects === false || !isset($indexOfEagerLoadedObjects[$object->id])) {
                                    $object->{$rel['storeInField']} = Relation::createHasMany($object, $rel['foreignKeyObjectField'], $rel['foreignKeyField'], [], $serviceModel);
                                } else {
                                    $subObjects = $eagerLoadedObjects->getByUuids($indexOfEagerLoadedObjects[$object->id]);

                                    if ($object->{$rel['storeInField']} instanceof ObjectStorageProxy) {
                                        $object->{$rel['storeInField']}->setRealObject($subObjects);
                                    } else {
                                        $object->{$rel['storeInField']} = $subObjects;
                                    }
                                }
                            }
                        }

                        continue 2;
                    }
                }

                foreach ($service->getHasOne() as $rel) {
                    if ($rel['storeInField'] === $relation) {
                        $serviceModel = $rel['service']::getInstance();

                        if (!isset(self::$cacheHasOne[$rel['service']])) {
                            self::$cacheHasOne[$rel['service']] = [];
                        }

                        $bind = ['ids' => $ids];
                        $where = '`' . $serviceModel->getDataTable() . '`.`' . $serviceModel->getDataFieldPrefix() . $rel['foreignKeyField'] . '` IN (:ids)' . (isset($rel['conditions']) ? ' AND ' . $rel['conditions'] : '');

                        if ($eagerLoadedObjects = $this->getChildObjects($serviceModel, $where, $bind, $subRelations)) {
                            $indexOfEagerLoadedObjects = $eagerLoadedObjects->buildIndex($rel['foreignKeyField']);
                        }

                        foreach ($objects as &$object) {
                            $key = $object->id . '-' . $rel['storeInField'];
                            if (!in_array($key, self::$cacheHasOne[$rel['service']])) {
                                self::$cacheHasOne[$rel['service']][] = $key;

                                if ($eagerLoadedObjects === false || !isset($indexOfEagerLoadedObjects[$object->id])) {
                                    $object->{$rel['storeInField']} = Relation::createHasOne($object, $rel['foreignKeyObjectField'], $serviceModel);
                                } else {
                                    $subObject = $eagerLoadedObjects->getByUuid($indexOfEagerLoadedObjects[$object->id][0]);

                                    if ($object->{$rel['storeInField']} instanceof WrapperInterface
                                        && $object->{$rel['storeInField']}->getRealObject(false) instanceof AbstractEntity
                                    ) {
                                        $object->{$rel['storeInField']}->getRealObject(false)->setRealObject($subObject);
                                    } else {
                                        $object->{$rel['storeInField']} = $subObject;
                                    }
                                }
                            }
                        }

                        continue 2;
                    }
                }

                foreach ($service->getBelongsTo() as $rel) {
                    if ($rel['storeInField'] === $relation) {
                        $serviceModel = $rel['service']::getInstance();

                        if (!isset(self::$cacheBelongsTo[$rel['service']])) {
                            self::$cacheBelongsTo[$rel['service']] = [];
                        }

                        if ($serviceModel->getDataAccess()->getDsn() === $this->ownerService->getDataAccess()->getDsn()) {
                            $bind = ['ids' => $ids];
                            $where = '`' . $serviceModel->getDataTable() . '`.`' . $serviceModel->getDataFieldPrefix() . 'id` IN (SELECT `' . $this->ownerService->getDataTable() . '`.`' . $this->ownerService->getDataFieldPrefix() . $rel['foreignKeyField'] . '` FROM `' . $this->ownerService->getDataTable() . '` WHERE `' . $this->ownerService->getDataTable() . '`.id IN (:ids))' . (isset($rel['conditions']) ? ' AND ' . $rel['conditions'] : '');

                            if ($eagerLoadedObjects = $this->getChildObjects($serviceModel, $where, $bind, $subRelations)) {
                                $indexOfEagerLoadedObjects = $eagerLoadedObjects->buildIndex('id');

                                foreach ($objects as &$object) {
                                    $key = $object->id . '-' . $rel['storeInField'];
                                    if (!in_array($key, self::$cacheBelongsTo[$rel['service']])) {
                                        self::$cacheBelongsTo[$rel['service']][] = $key;

                                        if ($object->{$rel['foreignKeyField']} === null || !isset($indexOfEagerLoadedObjects[$object->{$rel['foreignKeyField']}])) {
                                            // done
                                        } else {
                                            $subObject = $eagerLoadedObjects->getByUuid($indexOfEagerLoadedObjects[$object->{$rel['foreignKeyField']}][0]);

                                            if ($object->{$rel['storeInField']} instanceof WrapperInterface
                                                && $object->{$rel['storeInField']}->getRealObject(false) instanceof AbstractEntity
                                            ) {
                                                $object->{$rel['storeInField']}->setRealObject($subObject);
                                            } else {
                                                $object->{$rel['storeInField']} = $subObject;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        continue 2;
                    }
                }

                foreach ($service->getHasManyToMany() as $rel) {
                    if ($rel['primStoreInField'] === $relation) {
                        $secServiceModel = $rel['secService']::getInstance();
                        $linkRelatedService = $rel['linkRelatedService']::getInstance();

                        $bind = ['primForeignKeyField' => $ids];
                        $where = '`' . $secServiceModel->getDataTable() . '`.`' . $secServiceModel->getDataFieldPrefix() . 'id` IN
                            (
                                SELECT `' . $linkRelatedService->getDataTable() . '`.`' . $linkRelatedService->getDataFieldPrefix() . $rel['secForeignKeyField'] . '`
                                FROM `' . $linkRelatedService->getDataTable() . '`
                                WHERE `' . $linkRelatedService->getDataTable() . '`.`' . $this->ownerService->getDataFieldPrefix() . $rel['primForeignKeyField'] . '` IN (:primForeignKeyField)
                            )' . (isset($rel['secConditions']) ? ' AND ' . $rel['secConditions'] : '');

                        if ($eagerLoadedObjects = $this->getChildObjects($secServiceModel, $where, $bind, $subRelations)) {
                            // Alle childObjects staan nu in de cache van de betreffende service en worden daaruit geladen.
                            // de relatie gaat alleen via een link tabel en om per regel die nu uit te voeren kost evenveel als de proxy
                            // zijn werk te laten doen. Tot dat er een betere oplossing is laten we het dus maar zo.
                            //
                            // todo: onderstaande foreach opbouwen efficienter dan de proxy
                            //
                            //foreach ($objects as &$object) {
                            //    $object->{$rel['primStoreInField']} = $eagerLoadedObjects->get([$rel['secForeignKeyField'] => $object->id]) ?: null;
                            //}
                        }

                        continue 2;
                    }
                }
            }
        }

        return $objects;
    }

    /**
     *
     * @param Entity\EntityInterface $object
     * @return Entity\EntityInterface
     * @throws \Exception
     */
    protected function processOne(Entity\EntityInterface &$object)
    {
        $objectStorage = new ObjectStorage([], $this->ownerService);
        $objectStorage->attach($object);

        $this->processMany($objectStorage);

        return $object;
    }
}
