<?php

namespace Alexssssss\OrmModel\Entity;

use Alexssssss\OrmModel;
use Alexssssss\OrmModel\Helper;

/**
 * Een abstracte class welke als basis voor een entity dient
 *
 * @property mixed $id De ID van de entity
 */
abstract class AbstractEntity implements EntityInterface
{
    /**
     * Clone van het object gemaakt bij aanmaken of na dat het laast save() aangroepen is
     *
     * @var EntityInterface
     */
    protected $previous = null;

    /**
     * Array met veld namen welke niet gemapped staan aan/met een DB veld en uitgefilters
     * worden voor het opslaan en inladen
     *
     * @var array
     */
    protected static $notMapped = ['uuid'];

    /**
     * Array met velden welke niet mee genomen worden als er een array van het object
     * gemaakt word.
     *
     * @var array
     */
    protected static $hidden = [];

    /**
     * De ID van de entity
     *
     * @var int
     */
    protected $id;

    /**
     * @var OrmModel\Service\ServiceInterface
     */
    protected $service;

    /**
     * @var string
     */
    protected $serviceName;

    /**
     * @var array
     */
    protected $hasManyOrOneRel = [];

    /**
     * Universally unique identifier
     *
     * @var mixed
     */
    protected $uuid;

    /**
     *
     * @var EntityInterface
     */
    protected $wrapped;

    /**
     * @var array|EntityInterface[]
     */
    protected $entitiesToDeleteBeforeSave = [];

    /**
     * @var array|EntityInterface[]
     */
    protected $entitiesToSaveAfterSave = [];

    protected $lastStoredVersion;

    protected $additionalFields = [];

    /**
     *
     * @param OrmModel\Service\ServiceInterface $service
     */
    public function __construct(OrmModel\Service\ServiceInterface $service)
    {
        $this->uuid = uniqid(str_replace('\\', '-', get_class($this)) . '-', true);
        $this->service = $service;

        foreach (array_merge($this->service->getHasMany(), $this->service->getHasOne()) as $rel) {
            if (property_exists($this, $rel['storeInField'])) {
                unset($this->{$rel['storeInField']});
            }
        }

        foreach ($this->service->getHasManyToMany() as $rel) {
            if (property_exists($this, $rel['primStoreInField'])) {
                unset($this->{$rel['primStoreInField']});
            }
        }
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }


    /**
     * @param $id
     *
     * @return $this
     * @throws \Exception
     */
    protected function setUuid($id)
    {
        $this->setOrExceptionOnChange('uuid', $id);

        return $this;
    }

    /**
     * @return OrmModel\Service\ServiceInterface
     */
    public function getService(): OrmModel\Service\ServiceInterface
    {
        if ($this->wrapped === null) {
            return $this->service;
        } else {
            return $this->wrapped->getService();
        }
    }

    /**
     * Functie om de ID in te stellen en zorgt er ook voor dat de ID niet
     * overschreven kan worden.
     *
     * @param mixed $id
     *
     * @return EntityInterface
     * @throws \Exception                 Als ID al ingesteld is en gewijzigd zou worden
     */
    protected function setId(int $id = null): EntityInterface
    {
        $this->setOrExceptionOnChange('id', $id);
        return $this;
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return bool                    True if the field was changed, false is the values are the same
     * @throws \Exception
     */
    protected function setOrExceptionOnChange($field, $value)
    {
        if (!isset($this->{$field})) {
            $this->{$field} = $value;
            return true;
        } elseif ($this->{$field} !== $value) {
            throw new \Exception("Field '" . $field . "' of entity '" . get_class($this) . "' is already set, can't change it (" . $this->{$field} . " !== " . $value . ")");
        }

        return false;
    }

    /**
     * Magische functie voor het instellen van niet publieke eigenschappen.
     *
     * @param string $field
     * @param mixed $value
     *
     * @return EntityInterface
     * @throws \Exception                 Als het veld niet ingesteld kan worden komt er een exception
     */
    public function __set(string $field, $value)
    {
        if ($this->wrapped === null) {
            $field = str_replace('-', '', $field);
            $mutator = "set" . ucfirst($field);


            if (!isset($this->hasManyOrOneRel[$field])) {
                if ($rel = $this->service->getHasOneRelByField($field)) {
                    if ($this->id == null) {
                        $this->hasManyOrOneRel[$field] = $this->prepEntity($rel, 'hasOne');
                    } else {
                        $this->hasManyOrOneRel[$field] = $this->processHasOne($rel);
                    }
                } elseif ($rel = $this->service->getHasManyRelByField($field)) {
                    if ($this->id == null) {
                        $this->hasManyOrOneRel[$field] = $this->prepEntity($rel, 'hasMany');
                    } else {
                        $this->hasManyOrOneRel[$field] = $this->processHasMany($rel);
                    }
                } elseif ($rel = $this->service->getHasManyToManyRelByField($field)) {
                    if ($this->id == null) {
                        $this->hasManyOrOneRel[$field] = $this->prepEntity($rel, 'hasManyToMany');
                    } else {
                        $this->hasManyOrOneRel[$field] = $this->processHasManyToMany($rel);
                    }
                }
            }

            if (($rel = $this->service->getBelongsToRelByField($field)) && $this->processBelongsTo($value, $rel) === true) {
                // its a FK ;)
            } elseif (method_exists($this, $mutator) && is_callable([$this, $mutator])) {
                return $this->{$mutator}($value);
            } elseif (isset($this->hasManyOrOneRel[$field])) {
                if ($value === null) {
                    throw new OrmModel\Exception\UndefinedField("Can't overwrite value for the field '" . $field . "' with NULL in '" . get_class($this) . "'.");
                } elseif ($value instanceof OrmModel\Relation\HasManyInterface ||
                    $value instanceof OrmModel\Relation\HasManyToManyInterface ||
                    $value instanceof OrmModel\Relation\HasOneInterface ||
                    ($value instanceof OrmModel\WrapperInterface && $value->getRealObject(false) instanceof OrmModel\Relation\HasOneInterface)
                ) {
                    $this->hasManyOrOneRel[$field] = $value;
                } elseif ($value instanceof OrmModel\ObjectStorageInterface || $value instanceof EntityInterface) {
                    $this->hasManyOrOneRel[$field]->replace($value instanceof OrmModel\FakeObjectInterface ? $value->getRealObject() : $value);
                } else {
                    throw new OrmModel\Exception\UndefinedField("Can't understand overwrite value for the field '" . $field . "' in '" . get_class($this) . "'.");
                }
            } elseif (property_exists($this, $field)) {
                return $this->{$field} = $value;
            } elseif (!in_array($field, static::$notMapped)) {
                throw new OrmModel\Exception\UndefinedField("Setting the field '" . $field . "' is not valid for this entity '" . get_class($this) . "'.");
            }
        } else {
            return $this->wrapped->{$field} = $value;
        }
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return EntityInterface
     * @throws \Exception
     */
    public function setRaw(string $field, $value): EntityInterface
    {
        if ($this->wrapped === null) {
            $field = str_replace('-', '', $field);

            if (($rel = $this->service->getBelongsToRelByField($field)) && $this->processBelongsTo($value, $rel) === true) {
                // its a FK ;)

                if ($field == 'id') {
                    // FK and PK in one :S
                    $this->setId($value === null ? null : (int)$value);
                }
            } elseif (!property_exists($this, $field) && !in_array($field, static::$notMapped)) {
                throw new OrmModel\Exception\UndefinedField("Setting the raw field '" . $field . "' is not valid for this entity '" . get_class($this) . "'.");
            } elseif ($field == 'id') {
                $this->setId($value === null ? null : (int)$value);
            } else {
                $this->{$field} = $value;
            }
        } else {
            $this->wrapped->setRaw($field, $value);
        }

        return $this;
    }

    /**
     * Een magische functie die aangeroepen word als er een niet publieke
     * eigenschap van een entity opgevraagd word
     *
     * @param string $field
     *
     * @return mixed
     * @throws \Exception Als het veld niet bestaand word er een exception gemaakt
     */
    public function &__get(string $field)
    {
        if ($this->wrapped === null) {
            $field = str_replace('-', '', $field);

            $accessor = "get" . ucfirst($field);

            if (method_exists($this, $accessor) && is_callable([$this, $accessor])) {
                $value = $this->{$accessor}();
                return $value;
            } elseif (isset($this->hasManyOrOneRel[$field])) {
                return $this->hasManyOrOneRel[$field];
            } elseif ($rel = $this->service->getHasOneRelByField($field)) {
                if ($this->id == null) {
                    $this->hasManyOrOneRel[$field] = $this->prepEntity($rel, 'hasOne');
                } else {
                    $this->hasManyOrOneRel[$field] = $this->processHasOne($rel);
                }

                return $this->hasManyOrOneRel[$field];
            } elseif ($rel = $this->service->getHasManyRelByField($field)) {
                if ($this->id == null) {
                    $this->hasManyOrOneRel[$field] = $this->prepEntity($rel, 'hasMany');
                } else {
                    $this->hasManyOrOneRel[$field] = $this->processHasMany($rel);
                }

                return $this->hasManyOrOneRel[$field];
            } elseif ($rel = $this->service->getHasManyToManyRelByField($field)) {
                if ($this->id == null) {
                    $this->hasManyOrOneRel[$field] = $this->prepEntity($rel, 'hasManyToMany');
                } else {
                    $this->hasManyOrOneRel[$field] = $this->processHasManyToMany($rel);
                }

                return $this->hasManyOrOneRel[$field];
            } elseif (property_exists($this, $field)) {
                return $this->{$field};
            } elseif ($rel = $this->service->getBelongsToRelByField($field)) {
                if (!isset($this->{$rel['storeInField']})) {
                    // TODO - is dit slim??
                    $null = null;
                    return $null;
                } else {
                    return $this->{$rel['storeInField']}->id;
                }
            } elseif (in_array($field, array_keys($this->additionalFields))) {
                return $this->additionalFields[$field];
            } else {
                throw new OrmModel\Exception\UndefinedField("Getting the field '" . $field . "' is not valid for this entity '" . get_class($this) . "'.");
            }
        } else {
            return $this->wrapped->{$field};
        }
    }

    /**
     *
     * @param string|null $field
     *
     * @return bool
     */
    public function has(string $field): bool
    {
        if ($this->wrapped === null) {
            return $this->__isset($field);
        } else {
            return $this->wrapped->has($field);
        }
    }

    /**
     * @return array
     */
    public function toFriendlyArray(): array
    {
        if ($this->wrapped === null) {
            return $this->toArray(true, false, false, false);
        } else {
            return $this->wrapped->toFriendlyArray();
        }
    }

    /**
     * Alle eigenschappen van de entity teruggeven als array
     *
     * @param bool $hideObjects
     * @param bool $showHidden
     * @param bool $hideNotMapped
     * @param bool $rawOutput
     * @param bool $stringOutput
     *
     * @return array
     * @throws \Exception
     */
    public function toArray(bool $hideObjects = false, bool $showHidden = false, bool $hideNotMapped = false, bool $rawOutput = true, $stringOutput = false): array
    {
        if ($this->wrapped === null) {
            $realObject = $this;

            if ($this instanceof OrmModel\FakeObjectInterface) {
                $realObject = $this->getRealObject();
            }

            $data = array_merge(get_class_vars(get_class($realObject)), get_object_vars($realObject));

            // remove entity config fields from output
            foreach (['service', 'serviceName', 'previous', 'uuid', 'wrapped', 'entitiesToDeleteBeforeSave', 'entitiesToSaveAfterSave', 'hasManyOrOneRel', 'lastStoredVersion', 'additionalFields', 'hidden', 'notMapped'] as $field) {
                unset($data[$field]);
            }

            // set foreignKeyField of getBelongsTo objects
            foreach ((array)$this->service->getBelongsTo() as $relation) {
                if (isset($data[$relation['storeInField']]) && is_object($data[$relation['storeInField']])) {
                    $data[$relation['foreignKeyField']] = $data[$relation['storeInField']]->id;
                } else {
                    $data[$relation['foreignKeyField']] = $data[$relation['storeInField']] ?? null;
                }
            }

            // remove not mapped fields from output if $hideNotMapped === true
            if ($hideNotMapped === true) {
                foreach ((array)static::$notMapped as $field) {
                    unset($data[$field]);
                }
            } else {
                $data = array_merge($data, $this->hasManyOrOneRel, $this->additionalFields);
            }

            // remove hidden fields from output if $showHidden === false
            if ($showHidden === false) {
                foreach ((array)static::$hidden as $field) {
                    unset($data[$field]);
                }
            }

            if ($hideNotMapped === true || $hideObjects === true) {
                // remove relations 1/2
                foreach (array_merge($this->service->getHasOne(), $this->service->getHasMany(), $this->service->getBelongsTo()) as $relation) {
                    unset($data[$relation['storeInField']]);
                }

                // remove relations 2/2
                foreach ($this->service->getHasManyToMany() as $relation) {
                    unset($data[$relation['primStoreInField']]);
                }
            }

            // remove objects from output if $hideObjects === true
            if ($hideObjects === true) {
                foreach ($data as $field => &$value) {
                    if (is_object($value) && $value instanceof \DateTime) {
                        $value = (!empty($value->format('u')) ? $value->format('Y-m-d H:i:s.u') : $value->format('Y-m-d H:i:s'));
                    } elseif (is_object($value) && function_exists('enum_exists') && enum_exists(get_class($value))) {
                        $value = $value->name;
                    } elseif (is_array($value)) {
                        $value = json_encode($value);
                    } elseif (is_object($value)) {
                        unset($data[$field]);
                    }
                }
            }

            if ($rawOutput === false) {
                foreach ($data as $field => &$value) {
                    $value = $this->__get($field);
                }
            }

            if ($stringOutput === true) {
                foreach ($data as &$value) {
                    if ($value !== null && is_bool($value) === false && is_int($value) === false) {
                        $value = (string)$value;
                    }
                }
            }

            return $data;
        } else {
            return $this->wrapped->toArray($hideObjects, $showHidden, $hideNotMapped, $rawOutput, $stringOutput);
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function toArrayForSave(): array
    {
        if ($this->wrapped === null) {
            $fields = $this->toArray(true, true, true);

            return $fields;
        } else {
            return $this->wrapped->toArrayForSave();
        }
    }

    /**
     * Functie om te controleren of de entity een bepaalde variabele heeft.
     * Let op dat ook private en protected hierdoor van buiten de class herkent
     * kunnen worden als bestaand.
     *
     * @param string $field
     *
     * @return bool
     */
    public function __isset(string $field): bool
    {
        if ($this->wrapped === null) {
            $field = str_replace('-', '', $field);

            if (isset($this->hasManyOrOneRel[$field])) {
                return (
                    $this->hasManyOrOneRel[$field] !== null &&
                    ($object = ($this->hasManyOrOneRel[$field] instanceof OrmModel\WrapperInterface ? $this->hasManyOrOneRel[$field]->getRealObject(false) : $this->hasManyOrOneRel[$field])) &&
                    (
                        $object instanceof AbstractEntity
                        ||
                        ($object instanceof OrmModel\Relation\HasOneInterface && $object->isset())
                        ||
                        (($object instanceof OrmModel\Relation\HasManyInterface || $object instanceof OrmModel\Relation\HasManyToManyInterface) && $object->count() > 0)
                    )
                ) ? true : false;
            } elseif ($rel = $this->service->getHasOneRelByField($field)) {
                if ($this->id == null) {
                    return false;
                } else {
                    $this->hasManyOrOneRel[$field] = $this->processHasOne($rel);
                    return $this->__isset($field);
                }
            } elseif ($rel = $this->service->getHasManyRelByField($field)) {
                if ($this->id == null) {
                    return false;
                } else {
                    $this->hasManyOrOneRel[$field] = $this->processHasMany($rel);
                    return $this->__isset($field);
                }
            } elseif ($rel = $this->service->getHasManyToManyRelByField($field)) {
                if ($this->id == null) {
                    return false;
                } else {
                    $this->hasManyOrOneRel[$field] = $this->processHasManyToMany($rel);
                    return $this->__isset($field);
                }
            } elseif ($rel = $this->service->getBelongsToRelByField($field)) {
                $field = $rel['storeInField'];
            } elseif (in_array($field, array_keys($this->additionalFields))) {
                return true;
            }

            return (
                property_exists($this, $field) &&
                isset($this->{$field})
            ) ? true : false;
        } else {
            return $this->wrapped->__isset($field);
        }
    }

    /**
     *
     * @param array $args
     *
     * @return bool
     */
    public function delete(array &$args = []): bool
    {
        if ($this->wrapped === null) {
            return $this->service->delete($this, $args);
        } else {
            return $this->wrapped->delete($args);
        }
    }

    /**
     *
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     */
    public function save(bool $validate = true, array &$args = []): bool
    {
        if ($this->wrapped === null) {
            return $this->service->save($this, $validate, $args);
        } else {
            return $this->wrapped->save($validate, $args);
        }
    }

    /**
     *
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     */
    public function saveAll(bool $validate = true, array &$args = []): bool
    {
        if ($this->wrapped === null) {
            return $this->service->saveAll($this, $validate, $args);
        } else {
            return $this->wrapped->saveAll($validate, $args);
        }
    }

    /**
     *
     * @return bool
     */
    public function validate(): bool
    {
        if ($this->wrapped === null) {
            $this->service->validate($this);
            return true; // no exception means ok
        } else {
            return $this->wrapped->validate();
        }
    }

    /**
     *
     * @return bool
     */
    public function validateAll(): bool
    {
        if ($this->wrapped === null) {
            $this->service->validateAll($this);
            return true; // no exception means ok
        } else {
            return $this->wrapped->validateAll();
        }
    }

    /**
     * @return EntityInterface
     */
    public function setPrevious(): EntityInterface
    {
        if ($this->wrapped === null) {
            $this->previous = clone $this;
            $this->setLastStoredVersion();
        } else {
            $this->wrapped->setPrevious();
        }

        return $this;
    }

    /**
     *
     * @return EntityInterface
     */
    public function getPrevious(): EntityInterface
    {
        if ($this->wrapped === null) {
            return $this->previous ?? $this;
        } else {
            return $this->wrapped->getPrevious();
        }
    }

    /**
     * Methode om ID terug op NULL te zetten, dit is alleen wenselijk na een delete. Geeft anders veel vreemde effecten!
     *
     * @param bool $confirm Instellen op true op daadwerkelijk uit te voeren!
     *
     * @return EntityInterface
     */
    public function clearId(bool $confirm = false): EntityInterface
    {
        if ($this->wrapped === null) {
            if ($confirm === true) {
                $this->id = null;
            }
        } else {
            $this->wrapped->clearId($confirm);
        }

        return $this;
    }

    /**
     * @return array
     */
    public static function getNotMapped()
    {
        return static::$notMapped;
    }

    /**
     * @return array
     */
    public static function getHidden()
    {
        return static::$hidden;
    }

    /**
     * @return bool
     */
    public function isset(): bool
    {
        if ($this->wrapped === null) {
            return true;
        } else {
            return $this->wrapped->isset();
        }
    }

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return mixed|void
     */
    public function addEntityToDeleteBeforeSave(OrmModel\Entity\EntityInterface &$entity)
    {
        if ($this->wrapped === null) {
            if (isset($this->entitiesToSaveAfterSave[$entity->getUuid()])) {
                unset($this->entitiesToSaveAfterSave[$entity->getUuid()]);
            }

            if ($entity->getId() !== null) {
                $this->entitiesToDeleteBeforeSave[$entity->getUuid()] = $entity;
            }
        } else {
            return $this->wrapped->addEntityToDeleteBeforeSave($entity);
        }
    }

    /**
     * @return array|EntityInterface[]
     */
    public function getEntitiesToDeleteBeforeSave(): array
    {
        if ($this->wrapped === null) {
            return $this->entitiesToDeleteBeforeSave;
        } else {
            return $this->wrapped->getEntitiesToDeleteBeforeSave();
        }
    }

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return mixed
     */
    public function addEntityToSaveAfterSave(OrmModel\Entity\EntityInterface &$entity)
    {
        if ($this->wrapped === null) {
            if (isset($this->entitiesToDeleteBeforeSave[$entity->getUuid()])) {
                unset($this->entitiesToDeleteBeforeSave[$entity->getUuid()]);
            }

            if ($entity->getId() !== null) {
                $this->entitiesToSaveAfterSave[$entity->getUuid()] = $entity;
            }
        } else {
            return $this->wrapped->addEntityToSaveAfterSave($entity);
        }
    }

    /**
     * @return array|EntityInterface[]
     */
    public function getEntitiesToSaveAfterSave(): array
    {
        if ($this->wrapped === null) {
            return $this->entitiesToSaveAfterSave;
        } else {
            return $this->wrapped->getEntitiesToSaveAfterSave();
        }
    }

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     * @param array $rel
     * @param $type
     *
     * @return OrmModel\Relation\HasMany|OrmModel\Relation\HasManyToMany|OrmModel\WrapperInterface
     */
    protected function prepEntity(array $rel, $type)
    {
        if ($type === 'hasMany') {
            $serviceModel = $rel['service']::getInstance();
            return Helper\Relation::createHasMany($this, $rel['foreignKeyObjectField'], $rel['foreignKeyField'], [], $serviceModel);
        } elseif ($type === 'hasManyToMany') {
            $secService = $rel['secService']::getInstance();
            return Helper\Relation::createHasManyToMany($this, $rel['linkStoreInField'], $rel['linkSecForeignKeyObjectField'], $rel['linkSecForeignKeyField'], [], $secService);
        } elseif ($type === 'hasOne') {
            $serviceModel = $rel['service']::getInstance();
            return Helper\Relation::createHasOne($this, $rel['foreignKeyObjectField'], $serviceModel);
        }
    }

    /**
     * Functie om de "belongsTo" relatie in te stellen van een entity
     *
     * @param mixed $value
     * @param array $rel
     *
     * @return bool
     */
    protected function processBelongsTo($value, array $rel): bool
    {
        if (empty($value)) {
            if (isset($this->{$rel['storeInField']})) {
                $this->{$rel['storeInField']} = null;
            }
        } elseif (!isset($this->{$rel['storeInField']}) ||
            $this->{$rel['storeInField']}->id !== $value
        ) {
            if ($value instanceof OrmModel\WrapperInterface && $value->getRealObject() instanceof OrmModel\Entity\EntityInterface) {
                $this->{$rel['storeInField']} = $value;
            } else {
                $serviceModel = $rel['service']::getInstance();
                $this->{$rel['storeInField']} = Helper\Wrapper::createWrappedProxy($serviceModel, $value);
            }
        }
        return true;
    }

    /**
     * @param array $rel
     *
     * @return OrmModel\WrapperInterface
     */
    protected function processHasOne(array $rel): OrmModel\WrapperInterface
    {
        $serviceModel = $rel['service']::getInstance();

        $where = '`' . $serviceModel->getDataTable() . '`.`' . $serviceModel->getDataFieldPrefix() . $rel['foreignKeyField'] . '` = :' . $rel['foreignKeyField'] . (isset($rel['conditions']) ? ' AND ' . $rel['conditions'] : '');
        $bind = [$rel['foreignKeyField'] => $this->id];
        return Helper\Relation::createHasOneProxy($this, $rel['foreignKeyObjectField'], $where, $bind, $serviceModel);
    }

    /**
     * @param array $rel
     *
     * @return OrmModel\Relation\Proxy\HasManyToMany
     */
    protected function processHasManyToMany(array $rel): ?OrmModel\Relation\Proxy\HasManyToMany
    {
        if ($this->id !== null) {
            $secServiceModel = $rel['secService']::getInstance();
            $linkRelatedServiceModel = $rel['linkRelatedService']::getInstance();

            $bind = ['primForeignKeyField' => $this->id];

            $where = '`' . $secServiceModel->getDataTable() . '`.`' . $secServiceModel->getDataFieldPrefix() . 'id` IN 
                    (
                        SELECT `' . $linkRelatedServiceModel->getDataTable() . '`.`' . $linkRelatedServiceModel->getDataFieldPrefix() . $rel['secForeignKeyField'] . '` 
                        FROM `' . $linkRelatedServiceModel->getDataTable() . '` 
                        WHERE `' . $linkRelatedServiceModel->getDataTable() . '`.`' . $this->service->getDataFieldPrefix() . $rel['primForeignKeyField'] . '` = :primForeignKeyField
                    )' . (isset($rel['secConditions']) ? ' AND ' . $rel['secConditions'] : '');

            return Helper\Relation::createHasManyToManyProxy($this, $rel['linkStoreInField'], $rel['linkSecForeignKeyObjectField'], $rel['linkSecForeignKeyField'], $where, $bind, $secServiceModel);
        }
    }

    /**
     * @param array $rel
     *
     * @return OrmModel\Relation\Proxy\HasMany
     */
    protected function processHasMany(array $rel): OrmModel\Relation\Proxy\HasMany
    {
        $serviceModel = $rel['service']::getInstance();

        $where = '`' . $serviceModel->getDataTable() . '`.`' . $serviceModel->getDataFieldPrefix() . $rel['foreignKeyField'] . '` = :' . $rel['foreignKeyField'] . (isset($rel['conditions']) ? ' AND ' . $rel['conditions'] : '');
        $bind = [$rel['foreignKeyField'] => $this->id];

        return Helper\Relation::createHasManyProxy($this, $rel['foreignKeyObjectField'], $rel['foreignKeyField'], $where, $bind, $serviceModel);
    }

    public function __call($name, $arguments)
    {
        if (empty($arguments) && ($this->__isset($name) || stripos($name, 'get') !== 0)) {
            return $this->__get($name);
        }

        throw new OrmModel\Exception\NotSupported('Magic __call (' . $name . ' with args: ' . print_r($arguments, true) . ') is not supported, only hear to make Twig work with relation');
    }

    public function __sleep()
    {
        if ($this->wrapped === null) {
            if ($this instanceof OrmModel\FakeObjectInterface) {
                $data = get_object_vars($this->getRealObject());
            } else {
                $data = get_object_vars($this);
            }

            $this->serviceName = get_class($this->service);

            if ($data['id'] !== null) {
                return ['id', 'serviceName'];
            } else {
                // remove entity config fields from output
                foreach (['service', 'previous', 'entitiesToDeleteBeforeSave', 'entitiesToSaveAfterSave'] as $field) {
                    unset($data[$field]);
                }

                return array_keys($data);
            }
        } else {
            return ['wrapped'];
        }
    }

    public function __wakeup()
    {
        if ($this->wrapped === null) {
            $service = ($this->serviceName)::getInstance();
            $this->__construct($service);

            if ($this->id !== null) {
                $data = $service->getArrayById($this->id);

                foreach ($data as $field => $value) {
                    $this->setRaw($field, $value);
                }
            }
        } else {
            $this->__construct($this->wrapped);
        }
    }

    public function hasUnsavedChanges(): bool
    {
        if ($this->wrapped === null) {
            return $this->id === null || $this->lastStoredVersion != $this->toMd5();
        } else {
            return $this->wrapped->hasUnsavedChanges();
        }
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function setLastStoredVersion()
    {
        if ($this->wrapped === null) {
            $this->lastStoredVersion = $this->id !== null ? $this->toMd5() : null;
            return $this;
        } else {
            $this->wrapped->setLastStoredVersion();
        }
    }

    protected function toMd5()
    {
        $array = $this->toArray(true, true, true, true, true);

        foreach ($array as &$value) {
            if (is_string($value) && !mb_detect_encoding($value, 'UTF-8', true)) {
                $value = mb_convert_encoding($value, 'UTF-8', 'ISO-8859-1');
            }
        }

        return md5(json_encode($array));
    }

    public function toJson()
    {
        return json_encode($this);
    }

    public function __toString()
    {
        return $this->toJson();
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        if ($this->wrapped === null) {
            return $this->toFriendlyArray();
        } else {
            return $this->wrapped->toFriendlyArray();
        }
    }

    public function offsetExists($offset): bool
    {
        return $this->__isset($offset);
    }

    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->__get($offset);
    }

    public function offsetSet($offset, $value): void
    {
        $this->__set($offset, $value);
    }

    #[\ReturnTypeWillChange]
    public function offsetUnset($offset): void
    {
        throw new OrmModel\Exception\NotSupported('ArrayAccess offsetUnset (args: ' . $offset . ') is not supported, only hear to make Twig easier with objects');
    }

    public function setAdditonalFields(array $fields): void
    {
        foreach ($fields as $key => $value) {
            if (isset($this->additionalFields[$key]) && $this->additionalFields[$key] !== (string)$value) {
                throw new OrmModel\Exception\NotSupported('Changing an already set addional field (key: ' . $key . ') is not supported');
            }
            $this->additionalFields[$key] = (string)$value;
        }
    }
}
