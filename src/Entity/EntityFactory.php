<?php

namespace Alexssssss\OrmModel\Entity;

use Alexssssss\OrmModel\Exception\ProtectedFieldName;
use Alexssssss\OrmModel\Helper\Data;
use Alexssssss\OrmModel\Service\ServiceInterface;

class EntityFactory
{

    /**
     *
     * @var array
     */
    protected $entityFieldTypesCache = [];

    protected $protectedFieldNames = ['previous', 'notMapped', 'hidden', 'service', 'serviceName', 'hasManyOrOneRel', 'uuid', 'wrapped', 'entitiesToDeleteBeforeSave', 'entitiesToSaveAfterSave', 'lastStoredVersion'];

    /**
     *
     * @param ServiceInterface $service
     * @param array $data
     * @return void
     */
    protected function removeFkFromDataIfEntityExists(ServiceInterface &$service, array &$data)
    {
        $relations = $service->getBelongsTo();

        foreach ((array)$relations as $relation) {
            if (isset($data[$relation['foreignKeyField']]) && isset($data[$relation['storeInField']])) {
                unset($data[$relation['foreignKeyField']]);
            }
        }
    }

    /**
     *
     * @param ServiceInterface $service
     * @param string $entityClassName
     * @param array $data
     * @param bool $raw
     * @return EntityInterface
     * @throws \ReflectionException
     */
    public function create(ServiceInterface $service, $entityClassName, array $data = [], $raw = false): EntityInterface
    {
        $entity = new $entityClassName($service);
        if (!empty($data)) {
            $this->removeFkFromDataIfEntityExists($service, $data);

            Data::changeType($entityClassName, $data);

            if ($raw == true) {
                $this->fillRaw($entity, $data, $raw);
            } else {
                $this->fillNormal($entity, $data, $raw);
            }
            $entity->setPrevious();
        }

        return $entity;
    }

    /**
     *
     * @param EntityInterface $entity
     * @param array $data
     * @return EntityInterface
     */
    protected function fillNormal(EntityInterface &$entity, array &$data)
    {
        foreach ($data as $field => $value) {
            if (in_array($field, $this->protectedFieldNames)) {
                throw new ProtectedFieldName('The field '. $field. ' is protected and can not be set');
            }

            $entity->{$field} = $value;
        }

        return $entity;
    }

    /**
     *
     * @param EntityInterface $entity
     * @param array $data
     * @return EntityInterface
     */
    protected function fillRaw(EntityInterface &$entity, array &$data)
    {
        foreach ($data as $field => $value) {
            if (in_array($field, $this->protectedFieldNames)) {
                throw new ProtectedFieldName('The field '. $field. ' is protected and can not be set');
            }

            $entity->setRaw($field, $value);
        }

        return $entity;
    }
}
