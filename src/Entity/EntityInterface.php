<?php

namespace Alexssssss\OrmModel\Entity;

use Alexssssss\OrmModel;

/**
 * Interface EntityInterface
 * @package Alexssssss\OrmModel\Entity
 */
interface EntityInterface extends \JsonSerializable, \ArrayAccess
{

    /**
     * @return OrmModel\Service\ServiceInterface
     */
    public function getService();

    /**
     *
     * @param string $field
     * @param mixed $value
     */
    public function __set(string $field, $value);

    /**
     *
     * @param string $field
     * @param mixed $value
     * @return EntityInterface
     */
    public function setRaw(string $field, $value): EntityInterface;

    /**
     *
     * @param string $field
     */
    public function &__get(string $field);

    /**
     * @return array
     */
    public function toFriendlyArray(): array;

    /**
     *
     * @param bool $hideObjects
     * @param bool $showHidden
     * @param bool $hideNotMapped
     * @param bool $rawOutput
     * @param bool $stringOutput
     * @return array
     */
    public function toArray(bool $hideObjects = false, bool $showHidden = false, bool $hideNotMapped = false, bool $rawOutput = true, $stringOutput = false): array;

    /**
     * @return array
     */
    public function toArrayForSave(): array;

    /**
     *
     * @param string $field
     * @return bool
     */
    public function __isset(string $field): bool;

    /**
     *
     * @param array $args
     * @return bool
     */
    public function delete(array &$args = []): bool;

    /**
     *
     * @param bool $validate
     * @param array $args
     * @return bool
     */
    public function save(bool $validate = true, array &$args = []): bool;

    /**
     *
     * @param bool $validate
     * @param array $args
     * @return bool
     */
    public function saveAll(bool $validate = true, array &$args = []): bool;

    /**
     * @return bool
     */
    public function validate(): bool;

    /**
     * @return bool
     */
    public function validateAll(): bool;

    /**
     * @return EntityInterface
     */
    public function setPrevious(): EntityInterface;

    /**
     * @return EntityInterface
     */
    public function getPrevious(): EntityInterface;

    /**
     * @param bool $confirm Instellen op true op daadwerkelijk uit te voeren!
     * @return EntityInterface
     */
    public function clearId(bool $confirm = false): EntityInterface;

    /**
     * @param string $field
     * @return bool
     */
    public function has(string $field): bool;

    /**
     * @return bool
     */
    public function isset(): bool;

    /**
     * @return int|null
     */
    public function getId(): ? int;

    /**
     * @return string
     */
    public function getUuid(): string;

    /**
     * @param EntityInterface $entity
     * @return mixed
     */
    public function addEntityToDeleteBeforeSave(EntityInterface &$entity);

    /**
     * @return array|EntityInterface[]
     */
    public function getEntitiesToDeleteBeforeSave(): array;

    /**
     * @param EntityInterface $entity
     * @return mixed
     */
    public function addEntityToSaveAfterSave(EntityInterface &$entity);


    /**
     * @return array|EntityInterface[]
     */
    public function getEntitiesToSaveAfterSave(): array;

    public function hasUnsavedChanges(): bool;

    public function setLastStoredVersion();

    public function toJson();

    public function __toString();
}
