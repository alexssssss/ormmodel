<?php

namespace Alexssssss\OrmModel\Entity\Proxy;

use Alexssssss\OrmModel;

/**
 * Class AbstractEntity
 *
 * @package Alexssssss\OrmModel\Entity\Proxy
 * @property EntityInterface $previous Clone van het object gemaakt bij aanmaken of na dat het laast save() aangroepen is
 */
abstract class AbstractEntity extends OrmModel\AbstractProxy implements OrmModel\Entity\EntityInterface, OrmModel\Entity\Proxy\EntityInterface
{
    /**
     *
     * @var OrmModel\Service\ServiceInterface
     */
    protected $service;

    /**
     * @var string
     */
    protected $serviceName;

    /**
     *
     * @var OrmModel\Entity\EntityInterface
     */
    protected $entity;

    /**
     * @var bool
     */
    protected $loadCalled = false;

    /**
     * @param $fun
     * @param $params
     *
     * @return mixed
     * @throws OrmModel\Exception\UndefinedMethod
     */
    public function __call($fun, $params)
    {
        $this->load();

        if ($this->entity !== null) {
            if (is_callable([$this->entity, $fun])) {
                return call_user_func_array([$this->entity, $fun], $params);
            } else {
                throw new OrmModel\Exception\UndefinedMethod('Call to undefined method ' . get_class($this) . '::' . $fun . '()');
            }
        } else {
            throw new OrmModel\Exception\UndefinedMethod('Call to method ' . get_class($this) . '::' . $fun . '() on null proxy');
        }
    }

    /**
     * @return string
     */
    public function getEntityType()
    {
        return get_class($this->entity !== null ? $this->entity : $this->service->create());
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return mixed
     * @throws OrmModel\Exception\UndefinedField
     */
    public function __set(string $field, $value)
    {
        $this->load();

        if ($this->entity === null) {
            throw new OrmModel\Exception\UndefinedField('Can\'t set \'' . $field . '\' on null proxy entity of type ' . get_class($this));
        }

        return $this->entity->{$field} = $value;
    }

    /**
     * @param string $field
     *
     * @return null
     */
    public function &__get(string $field)
    {
        $this->load();

        if ($this->entity !== null) {
            $value = &$this->entity->{$field};
        } else {
            $value = null;
        }
        return $value;
    }

    /**
     * @return bool
     */
    public function isLoaded(): bool
    {
        return $this->entity === null ? false : true;
    }

    /**
     * @return $this
     */
    public function load()
    {
        if ($this->entity === null && $this->loadCalled == false) {
            $this->setEntity();
            $this->loadCalled = true;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function unload()
    {
        $this->entity = null;
        $this->loadCalled = false;

        return $this;
    }

    /**
     * @return $this
     */
    public function reload()
    {
        $this->unload();
        $this->load();

        return $this;
    }

    /**
     * @param OrmModel\Entity\EntityInterface|null $entity
     *
     * @return mixed
     */
    abstract protected function setEntity(OrmModel\Entity\EntityInterface $entity = null);

    /**
     * @param string $field
     *
     * @return bool
     */
    public function __isset(string $field): bool
    {
        $this->load();

        return $this->entity !== null && isset($this->entity->{$field});
    }

    /**
     * @return OrmModel\Entity\EntityInterface
     */
    public function getRealObject()
    {
        $this->load();

        return $this->entity;
    }

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return $this
     * @throws \Exception
     */
    public function setRealObject(OrmModel\Entity\EntityInterface $entity)
    {
        if ($this->entity !== null && $entity->id !== $this->entity->id) {
            throw new \Exception("Can't override entity with setRealObject");
        } else {
            $this->setEntity(($entity instanceof OrmModel\FakeObjectInterface ? $entity->getRealObject() : $entity));
        }

        return $this;
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return OrmModel\Entity\EntityInterface
     */
    public function setRaw(string $field, $value): OrmModel\Entity\EntityInterface
    {
        $this->load();
        return $this->entity->setRaw($field, $value);
    }

    /**
     * @param bool $hideObjects
     * @param bool $showHidden
     * @param bool $hideNotMapped
     * @param bool $rawOutput
     * @param bool $stringOutput
     *
     * @return array
     */
    public function toArray(bool $hideObjects = false, bool $showHidden = false, bool $hideNotMapped = false, bool $rawOutput = true, $stringOutput = false): array
    {
        $this->load();
        return $this->entity->toArray($hideObjects, $showHidden, $hideNotMapped, $rawOutput, $stringOutput);
    }

    /**
     * @return array
     */
    public function toArrayForSave(): array
    {
        $this->load();
        return $this->entity->toArrayForSave();
    }

    /**
     * @param array $args
     *
     * @return bool
     */
    public function delete(array &$args = []): bool
    {
        $this->load();
        return $this->entity === null || $this->entity->delete($args);
    }

    /**
     * @param array $data
     * @param bool $setDefaults
     *
     * @return OrmModel\Entity\EntityInterface
     * @throws \Exception
     */
    public function create(array $data = [], bool $setDefaults = true): OrmModel\Entity\EntityInterface
    {
        $this->load();

        if ($this->entity === null) {
            $this->entity = $this->service->create($data, $setDefaults);
            return $this->entity;
        } else {
            throw new \Exception('Can\'t create a new object, there already exists one');
        }
    }

    /**
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     */
    public function save(bool $validate = true, array &$args = []): bool
    {
        if ($this->isLoaded() == true) {
            return $this->entity->save($validate, $args);
        }

        return true;
    }

    /**
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     */
    public function saveAll(bool $validate = true, array &$args = []): bool
    {
        if ($this->isLoaded()) {
            return $this->entity->saveAll($validate, $args);
        }

        return true;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        if ($this->isLoaded()) {
            return $this->entity->validate();
        }

        return true; // no exception means ok
    }

    /**
     * @return bool
     */
    public function validateAll(): bool
    {
        if ($this->isLoaded()) {
            return $this->entity->validate();
        }

        return true; // no exception means ok
    }

    /**
     * @return OrmModel\Service\ServiceInterface
     */
    public function getService(): OrmModel\Service\ServiceInterface
    {
        $this->load();
        return $this->entity->getService();
    }

    /**
     * @return OrmModel\Entity\EntityInterface
     */
    public function setPrevious(): OrmModel\Entity\EntityInterface
    {
        $this->load();
        return $this->entity->setPrevious();
    }

    /**
     * @return OrmModel\Entity\EntityInterface
     */
    public function getPrevious(): OrmModel\Entity\EntityInterface
    {
        $this->load();
        return $this->entity->getPrevious();
    }

    /**
     * @param bool $confirm
     *
     * @return OrmModel\Entity\EntityInterface
     */
    public function clearId(bool $confirm = false): OrmModel\Entity\EntityInterface
    {
        $this->load();
        return $this->entity->clearId($confirm);
    }

    /**
     * @param string $field
     *
     * @return bool
     */
    public function has(string $field): bool
    {
        $this->load();
        return $this->entity !== null && $this->entity->has($field);
    }

    /**
     * @return array
     */
    public function toFriendlyArray(): array
    {
        $this->load();
        return $this->entity->toFriendlyArray();
    }

    /**
     * @return bool
     */
    public function isset(): bool
    {
        $this->load();
        return $this->entity !== null;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->__get('id');
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->__get('uuid');
    }

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return mixed|void
     */
    public function addEntityToDeleteBeforeSave(OrmModel\Entity\EntityInterface &$entity)
    {
        $this->load();
        $this->entity->addEntityToDeleteBeforeSave($entity);
    }

    /**
     * @return array|EntityInterface[]
     */
    public function getEntitiesToDeleteBeforeSave(): array
    {
        if ($this->isLoaded()) {
            return $this->entity->getEntitiesToSaveAfterSave();
        } else {
            return [];
        }
    }


    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return mixed
     */
    public function addEntityToSaveAfterSave(OrmModel\Entity\EntityInterface &$entity)
    {
        $this->load();
        $this->entity->addEntityToSaveAfterSave($entity);
    }

    /**
     * @return array|EntityInterface[]
     */
    public function getEntitiesToSaveAfterSave(): array
    {
        if ($this->isLoaded()) {
            return $this->entity->getEntitiesToSaveAfterSave();
        } else {
            return [];
        }
    }

    public function __sleep()
    {
        $this->serviceName = get_class($this->service);
        return ['id', 'serviceName'];
    }

    public function __wakeup()
    {
        $this->service = ($this->serviceName)::getInstance();
    }

    public function hasUnsavedChanges(): bool
    {
        if ($this->isLoaded()) {
            return $this->entity->hasUnsavedChanges();
        }

        return false;
    }

    public function setLastStoredVersion()
    {
        if ($this->isLoaded()) {
            return $this->entity->setLastStoredVersion();
        }
    }

    public function toJson()
    {
        $this->load();
        return $this->entity->toJson();
    }

    public function __toString()
    {
        $this->load();
        return $this->entity->__toString();
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        $this->load();
        return $this->entity->jsonSerialize();
    }

    public function offsetExists($offset): bool
    {
        return $this->__isset($offset);
    }

    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->{$offset};
    }

    public function offsetSet($offset, $value): void
    {
        $this->{$offset} = $value;
    }

    #[\ReturnTypeWillChange]
    public function offsetUnset($offset) : void
    {
        throw new OrmModel\Exception\NotSupported('ArrayAccess offsetUnset (args: ' . $offset . ') is not supported, only hear to make Twig easier with objects');
    }
}
