<?php

namespace Alexssssss\OrmModel\Entity\Proxy;

use Alexssssss\OrmModel;

/**
 * Class Entity
 * @package Alexssssss\OrmModel\Entity\Proxy
 */
class Entity extends AbstractEntity
{

    /**
     * @var int
     */
    protected $id;

    /**
     * Entity constructor.
     * @param OrmModel\Service\ServiceInterface $service
     * @param int|null $id
     * @param OrmModel\Entity\EntityInterface|null $entity
     */
    public function __construct(OrmModel\Service\ServiceInterface &$service, int $id = null, OrmModel\Entity\EntityInterface &$entity = null)
    {
        $this->id = $id;
        $this->service = $service;

        if ($entity !== null) {
            $this->entity = $entity;
        }
    }

    /**
     * @param string $name
     * @return int|null
     */
    public function &__get(string $name)
    {
        if ($name == 'id') {
            if ($this->entity !== null) {
                return $this->entity->id;
            } elseif ($this->id === null) {
                $this->id = parent::__get($name);
            }

            return $this->id;
        }

        return parent::__get($name);
    }

    /**
     * @param OrmModel\Entity\EntityInterface|null $entity
     * @return mixed|void
     * @throws \Exception
     */
    protected function setEntity(OrmModel\Entity\EntityInterface $entity = null)
    {
        if ($entity !== null) {
            $this->entity = $entity;
        } elseif (!($this->entity = $this->service->getOne($this->id))) {
            throw new \Exception("Proxy of service '" . get_class($this->service) . "' unable to load the entity with ID '" . $this->id . "'");
        }
    }
}
