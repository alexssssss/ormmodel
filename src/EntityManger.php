<?php

namespace Alexssssss\OrmModel;

use Alexssssss\OrmModel\Entity\EntityInterface;

class EntityManger
{
    protected $uuids = [];
    protected $objects = [];

    /**
     * Attach a object to the entity manager
     *
     * @param EntityInterface $object
     * @return bool
     */
    public function attach(EntityInterface &$object): bool
    {
        if (null !== $key = array_key_last($this->uuids)) {
            $key++;
        } else {
            $key = 0;
        }

        $this->uuids[$key] = $object->getUuid();
        $this->objects[$key] = $object;

        return true;
    }

    /**
     * Remove object from entity manger
     * @param EntityInterface $object
     * @return bool
     */
    public function detach(EntityInterface &$object): bool
    {
        $uuid = $object->getUuid();

        if (false !== $key = array_search($uuid, $this->uuids)) {
            unset($this->uuids[$key]);
            unset($this->objects[$key]);
            return true;
        }

        return false;
    }

    /**
     * Clear all objects
     * @return bool
     */
    public function clear(): bool
    {
        $this->objects = [];
        return true;
    }

    /**
     * Validate all objects
     *
     * @param array $args
     *
     * @return bool
     */
    public function validate(array &$args = []): bool
    {
        $allValid = true;
        foreach ($this->objects as $entity) {
            /** @var $entity EntityInterface */
            if ($entity->validate($args) === false) {
                $allValid = false;
            }
        }

        return $allValid;
    }

    /**
     * Save all objects
     *
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     */
    public function save(bool $validate = true, array &$args = []): bool
    {
        if ($validate === true) {
            $this->validate($args);
        }

        $allSaved = true;
        foreach ($this->objects as $entity) /** @var $entity EntityInterface */ {
            if ($entity->save(false, $args) === false) {
                $allSaved = false;
            }
        }

        return $allSaved;
    }
}