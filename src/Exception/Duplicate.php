<?php

namespace Alexssssss\OrmModel\Exception;

/**
 * Exception speciaal voor validatie fouten
 */
class Duplicate extends General
{

    /**
     *
     * @var \Symfony\Contracts\Translation\TranslatorInterface
     */
    protected $translator;

    /**
     *
     * @var string
     */
    protected $key;

    public function __construct($message, \Symfony\Contracts\Translation\TranslatorInterface $translator = null)
    {
        $this->translator = $translator;

        $matches = [];
        preg_match('/Duplicate entry \'.+\' for key \'(.+)\'/', $message, $matches);
        $this->key = ucfirst($matches[1]? : 'unknown');
        parent::__construct($message);
    }

    public function __toString()
    {
        if ($this->translator !== null) {
            return $this->translator->trans('Validation.Duplicate.' . $this->key, [], 'general');
        } else {
            return parent::getMessage();
        }
    }
}
