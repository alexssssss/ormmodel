<?php

namespace Alexssssss\OrmModel\Exception;

/**
 * Exception speciaal voor validatie fouten
 */
class InvalidFields extends Validation
{

    /**
     * @var \Symfony\Component\Validator\ConstraintViolationList
     */
    protected $violations;
    protected $invalidFields = [];

    /**
     * Maakt van de array met velden een echte exception string
     *
     * @param \Symfony\Component\Validator\ConstraintViolationList $violations
     * @param \Symfony\Contracts\Translation\TranslatorInterface $translator
     */
    public function __construct(\Symfony\Component\Validator\ConstraintViolationList $violations, \Symfony\Contracts\Translation\TranslatorInterface $translator = null)
    {
        $this->violations = $violations;

        foreach ($this->violations as $violation) {
            /* @var $violation \Symfony\Component\Validator\ConstraintViolation */
            $field = str_replace('\\', '.', get_class($violation->getRoot()) . "." . ucfirst($violation->getPropertyPath()));

            $this->invalidFields[$field] = $violation->getMessage();
        }

        $message = "Validation error, invalid fields: " . implode(", ", array_keys($this->invalidFields));
        parent::__construct($message, $translator);
    }

    /**
     *
     * @return \Symfony\Component\Validator\ConstraintViolationList
     */
    public function getViolations()
    {
        return $this->violations;
    }

    public function __toString()
    {
        if ($this->translator !== null) {
            $outputArrayOfStrings = [];
            $outputArrayOfseparateStrings = [];
            foreach ($this->violations as $violation) {
                /* @var $violation \Symfony\Component\Validator\ConstraintViolation */
                $class = explode('\\', get_class($violation->getRoot()));
                $field = str_replace('\\', '.', end($class) . "." . ucfirst($violation->getPropertyPath()));

                $constraint = $violation->getConstraint();
                $separate = isset($constraint->payload['separateMsg']) ? $constraint->payload['separateMsg'] : false;

                if ($separate) {
                    $outputArrayOfseparateStrings[$field] = ucfirst($this->translator->trans($field, [], 'entities'));
                } else {
                    $outputArrayOfStrings[$field] = $this->translator->trans($field, [], 'entities');
                }
            }

            $fields = implode(', ', $outputArrayOfStrings);

            $return = '';
            if (count($outputArrayOfStrings) > 1) {
                $return = ucfirst($this->translator->trans('Validation.InvalidFields', ['%fields%' => $fields], 'general'));
            } elseif (count($outputArrayOfStrings) == 1) {
                $return = ucfirst($this->translator->trans('Validation.InvalidField', ['%field%' => $fields], 'general'));
            }

            $return .= (count($outputArrayOfStrings) >= 1 && count($outputArrayOfseparateStrings) >= 1) ? "\n" : null;

            if (count($outputArrayOfseparateStrings) >= 1) {
                $return .= implode("\n", $outputArrayOfseparateStrings);
            }

            return $return;
        } else {
            return parent::getMessage();
        }
    }
}
