<?php

namespace Alexssssss\OrmModel\Exception;

/**
 * Exception speciaal voor validatie fouten
 */
class Validation extends General
{

    /**
     *
     * @var \Symfony\Contracts\Translation\TranslatorInterface
     */
    protected $translator;

    public function __construct($message, \Symfony\Contracts\Translation\TranslatorInterface $translator = null)
    {
        $this->translator = $translator;

        parent::__construct($message);
    }

    public function __toString()
    {
        if ($this->translator !== null) {
            return $this->translator->trans(parent::getMessage(), [], 'general');
        } else {
            return parent::getMessage();
        }
    }
}
