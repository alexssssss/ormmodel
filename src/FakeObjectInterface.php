<?php

namespace Alexssssss\OrmModel;

/**
 * Interface FakeObjectInterface
 * @package Alexssssss\OrmModel
 */
interface FakeObjectInterface
{

    /**
     * @return mixed
     */
    public function getRealObject();
}
