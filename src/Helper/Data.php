<?php

namespace Alexssssss\OrmModel\Helper;

use App\Model\Entity\App;

class Data
{

    protected static $entityFieldTypesCache = [];

    /**
     * @param $entityClassName
     * @param array $data
     *
     * @throws \ReflectionException
     */
    public static function changeType($entityClassName, array &$data)
    {
        $changeableTypes = ['float', 'decimal', 'double', 'integer', 'int', 'boolean', 'bool', 'datetime', 'array', 'string'];

        if (!isset(self::$entityFieldTypesCache[$entityClassName])) {
            $refClass = new \ReflectionClass($entityClassName);

            foreach ($refClass->getProperties() as $refProperty) {
                $type = null;
                $nullable = null;
                if (method_exists($refProperty, 'getType') && (null !== $propertyType = $refProperty->getType())) {
                    $type = $propertyType->getName();

                    if (!function_exists('enum_exists') || enum_exists($type) === false) {
                        $type = strtolower($type);
                    }

                    $nullable = $propertyType->allowsNull();
                } elseif (preg_match('/@var\s+([^\s]+)\s([^\s]+)/', $refProperty->getDocComment(), $matches) || preg_match('/@var\s+([^\s]+)/', $refProperty->getDocComment(), $matches)) {
                    $type = ltrim(strtolower($matches[1]), '\\');
                }

                if ($type !== null && (in_array($type, $changeableTypes) || (function_exists('enum_exists') && enum_exists($type)))) {
                    if (isset($matches[2])) {
                        self::$entityFieldTypesCache[$entityClassName][ltrim($matches[2], '$')] = [$type, $nullable];
                    }
                    self::$entityFieldTypesCache[$entityClassName][$refProperty->name] = [$type, $nullable];
                }
            }
        }

        if (isset(self::$entityFieldTypesCache[$entityClassName])) {
            foreach (self::$entityFieldTypesCache[$entityClassName] as $field => [$type, $nullable]) {
                if (!isset($data[$field]) && $nullable !== false) {
                    if ($nullable === null) {
                        continue;
                    } elseif ($nullable === true) {
                        $data[$field] = null;
                    }
                } elseif (in_array($type, ['string'])) {
                    $data[$field] = (string)($data[$field] ?? null);
                } elseif (in_array($type, ['integer', 'int'])) {
                    $data[$field] = (int)($data[$field] ?? null);
                } elseif (in_array($type, ['float', 'decimal', 'double'])) {
                    $data[$field] = (float)($data[$field] ?? null);
                } elseif (in_array($type, ['boolean', 'bool'])) {
                    $data[$field] = (bool)($data[$field] ?? null);
                } elseif (in_array($type, ['datetime'])) {
                    if (!key_exists($field, $data)) {
                        $data[$field] = new \DateTime('1970-01-01 UTC');
                    } elseif ($data[$field] === '0000-00-00 00:00:00') {
                        $data[$field] = $nullable ? null : new \DateTime('1970-01-01 UTC');
                    } else {
                        $data[$field] = (!$data[$field] instanceof \DateTime) ? new \DateTime($data[$field]) : $data[$field];
                    }
                } elseif (in_array($type, ['array'])) {
                    if (!key_exists($field, $data)) {
                        $data[$field] = [];
                    } else {
                        $data[$field] = (array)json_decode($data[$field], true);
                    }
                } elseif (function_exists('enum_exists') && enum_exists($type)) {
                    $data[$field] = constant($type . '::' . $data[$field]);
                }
            }
        }
    }
}
