<?php

namespace Alexssssss\OrmModel\Helper;

class DotNotation
{

    /**
     *
     * @param string|array $stringOrArray
     * @return array
     * @throws \Exception
     */
    public static function toArray($stringOrArray)
    {
        if (is_string($stringOrArray)) {
            $result = [];
            $output = &$result;
            $keys = explode('.', $stringOrArray);

            while ($key = array_shift($keys)) {
                $result = &$result[$key];
            }

            $result = [];

            return $output;
        } elseif (is_array($stringOrArray)) {
            $output = [];

            foreach ($stringOrArray as $string) {
                $output = array_merge_recursive($output, self::toArray($string));
            }

            return $output;
        } else {
            throw new \Exception('DotNotation::toArray only supports strings and arrays as input, given: ' . gettype($stringOrArray));
        }
    }

    /**
     *
     * @param array $array
     * @return array
     */
    public static function fromArray(array $array)
    {
        $array = self::emptyArrayToNull($array);

        $ritit = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array));
        $result = [];

        foreach ($ritit as $leafValue) {
            $keys = [];
            foreach (range(0, $ritit->getDepth()) as $depth) {
                $keys[] = $ritit->getSubIterator($depth)->key();
            }
            $result[] = join('.', $keys);
        }

        return $result;
    }

    /**
     *
     * @param array $array
     * @return array
     */
    protected static function emptyArrayToNull(array $array)
    {
        foreach ($array as $key => $value) {
            if (is_array($array[$key]) && count($array[$key]) > 0) {
                $array[$key] = self::emptyArrayToNull($array[$key]);
            } else {
                $array[$key] = null;
            }
        }
        return $array;
    }
}
