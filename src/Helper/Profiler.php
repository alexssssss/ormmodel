<?php

namespace Alexssssss\OrmModel\Helper;

use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Stopwatch\StopwatchEvent;
use Psr\Log\LoggerInterface;

class Profiler
{
    protected static $instance = null;

    public static $debug = false;

    protected static $callStats = ['total' => 0];

    /**
     * @var LoggerInterface
     */
    public $logger = null;

    /**
     * @var Stopwatch
     */
    public $stopwatch = null;

    public function __construct(LoggerInterface $logger = null, Stopwatch $stopwatch = null, bool $debug = null)
    {
        if (self::$instance === null) {
            $this->logger = $logger;
            $this->stopwatch = $stopwatch;

            if ($debug !== null) {
                self::$debug = $debug;
            }

            self::$instance = $this;
        }
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Profiler();
        }

        return self::$instance;
    }

    public static function setLogger(LoggerInterface $logger): Profiler
    {
        $profiler = self::getInstance();
        $profiler->logger = $logger;

        return $profiler;
    }

    public static function setDebug(bool $debug)
    {
        self::$debug = $debug;
    }

    public static function setStopwatch(Stopwatch $stopwatch): Profiler
    {
        $profiler = self::getInstance();
        $profiler->stopwatch = $stopwatch;

        return $profiler;
    }

    public static function addCall(string $className, string $type, array $context = []): ?StopwatchEvent
    {
        if (!isset(self::$callStats[$type])) {
            self::$callStats[$type] = 0;
        }
        self::$callStats[$type]++;
        self::$callStats['total']++;

        if (self::$debug) {
            $profiler = self::getInstance();
            $requestId = uniqid();

            if ($profiler->logger !== null) {
                $profiler->logger->debug('Called ' . $className . '::' . $type . ' ' . $requestId, $context);
            }

            if ($profiler->stopwatch !== null) {
                $e = $profiler->stopwatch->start($className . '::' . $type . '#' . $requestId, 'ormmodel');
            }
        }

        return $e ?? null;
    }

    public static function getCallStats()
    {
        return self::$callStats;
    }

    public static function log(string $msg, array $context = [])
    {
        if (self::$debug) {
            $profiler = self::getInstance();

            if ($profiler->logger !== null) {
                $profiler->logger->debug($msg, $context);
            }
        }
    }
}