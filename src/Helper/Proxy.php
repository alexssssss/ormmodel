<?php

namespace Alexssssss\OrmModel\Helper;

use Alexssssss\OrmModel;

class Proxy
{

    protected static $cache = [];

    /**
     * @codeCoverageIgnore
     */
    public static function createEntity(OrmModel\Service\ServiceInterface &$service, int $id = null, OrmModel\Entity\EntityInterface &$entity = null): OrmModel\Entity\Proxy\Entity
    {
        if ($id === null && $entity === null) {
            throw new  OrmModel\Exception\General('Can\t make proxy without atleast one of the following args: ID or entity');
        }

        $key = get_class($service) . '#' . ($entity !== null ? $entity->getUuid() : $id);

        if (!isset(self::$cache[$key])) {
            $proxy = new OrmModel\Entity\Proxy\Entity($service, $id, $entity);
            self::$cache[$key] = $proxy;
        }

        return self::$cache[$key];
    }

    public static function clearCache()
    {
        self::$cache = [];
    }
}
