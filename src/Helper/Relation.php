<?php

namespace Alexssssss\OrmModel\Helper;

use Alexssssss\OrmModel;

class Relation
{
    /**
     * @codeCoverageIgnore
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param string $where
     * @param array $bind
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     *
     * @return \Alexssssss\OrmModel\WrapperInterface
     */
    public static function createHasOneProxy(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, string $where, array $bind, OrmModel\Service\ServiceInterface &$service): OrmModel\WrapperInterface
    {
        $hasOneProxy = new OrmModel\Relation\Proxy\HasOne($parent, $foreignKeyObjectField, $where, $bind, $service);
        $wrapperClassName = $service->getWrapperClassName();
        return new $wrapperClassName($hasOneProxy);
    }

    /**
     * @codeCoverageIgnore
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     * @return \Alexssssss\OrmModel\WrapperInterface
     */
    public static function createHasOne(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, OrmModel\Service\ServiceInterface &$service): OrmModel\WrapperInterface
    {
        $hasOne = new OrmModel\Relation\HasOne($parent, $foreignKeyObjectField, $service);
        $wrapperClassName = $service->getWrapperClassName();
        return new $wrapperClassName($hasOne);
    }


    /**
     * @codeCoverageIgnore
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param string $foreignKeyField
     * @param string $where
     * @param array $bind
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     *
     * @return \Alexssssss\OrmModel\Relation\Proxy\HasMany
     */
    public static function createHasManyProxy(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, string $foreignKeyField, string $where, array $bind, OrmModel\Service\ServiceInterface &$service): OrmModel\Relation\Proxy\HasMany
    {
        return new OrmModel\Relation\Proxy\HasMany($parent, $foreignKeyObjectField, $foreignKeyField, $where, $bind, $service);
    }

    /**
     * @codeCoverageIgnore
     * @param OrmModel\Entity\EntityInterface $parent
     * @param string $linkStoreInField
     * @param string $linkSecForeignKeyObjectField
     * @param string $linkSecForeignKeyField
     * @param string $where
     * @param array $bind
     * @param OrmModel\Service\ServiceInterface $service
     * @return OrmModel\Relation\Proxy\HasManyToMany
     */
    public static function createHasManyToManyProxy(OrmModel\Entity\EntityInterface &$parent, string $linkStoreInField, string $linkSecForeignKeyObjectField, string $linkSecForeignKeyField, string $where, array $bind, OrmModel\Service\ServiceInterface &$service): OrmModel\Relation\Proxy\HasManyToMany
    {
        return new OrmModel\Relation\Proxy\HasManyToMany($parent, $linkStoreInField, $linkSecForeignKeyObjectField, $linkSecForeignKeyField, $where, $bind, $service);
    }

    /**
     * @codeCoverageIgnore
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param string $foreignKeyField
     * @param array $arrayOfObjects
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     * @return \Alexssssss\OrmModel\Relation\HasMany
     * @throws \Exception
     */
    public static function createHasMany(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, string $foreignKeyField, array $arrayOfObjects, OrmModel\Service\ServiceInterface &$service): OrmModel\Relation\HasMany
    {
        return new OrmModel\Relation\HasMany($parent, $foreignKeyObjectField, $foreignKeyField, $arrayOfObjects, $service);
    }

    /**
     * @codeCoverageIgnore
     * @param OrmModel\Entity\EntityInterface $parent
     * @param string $linkStoreInField
     * @param string $linkSecForeignKeyObjectField
     * @param string $linkSecForeignKeyField
     * @param array $arrayOfObjects
     * @param OrmModel\Service\ServiceInterface $service
     * @return OrmModel\Relation\HasManyToMany
     * @throws \Exception
     */
    public static function createHasManyToMany(OrmModel\Entity\EntityInterface &$parent, string $linkStoreInField, string $linkSecForeignKeyObjectField, string $linkSecForeignKeyField, array $arrayOfObjects, OrmModel\Service\ServiceInterface &$service): OrmModel\Relation\HasManyToMany
    {
        return new OrmModel\Relation\HasManyToMany($parent, $linkStoreInField, $linkSecForeignKeyObjectField, $linkSecForeignKeyField, $arrayOfObjects, $service);
    }
}
