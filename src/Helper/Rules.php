<?php

namespace Alexssssss\OrmModel\Helper;

use Alexssssss\OrmModel\Service\ServiceInterface;

/**
 * Class Rules
 * @package Alexssssss\OrmModel\Helper
 */
class Rules
{
    /**
     * @param $rules
     * @return array
     */
    public static function process($rules)
    {
        if (isset($rules[0]) && (is_string($rules[0]) || (count($rules) == 1 && !ctype_digit((string)implode('', array_keys($rules)))))) {
            $rules = [$rules];
        }

        $allRules = [];

        foreach ($rules as $key => $rule) {
            if (is_string($rule) && in_array($rule, ['OR', 'AND'])) {
                $allRules[] = $rule;
            } elseif (isset($rule['field']) && isset($rule['operator']) && (isset($rule['value']) || $rule['value'] === null)) {
                $allRules[] = $rule;
            } elseif (!is_array($rule) || (!ctype_digit((string)$key) && is_array($rule))) {
                $allRules[] = ['field' => $key, 'operator' => '=', 'value' => $rule];
            } elseif (count($rule) == 1 && !ctype_digit((string)implode('', array_keys($rule)))) {
                $field = array_keys($rule)[0];
                $value = $rule[$field];

                $allRules[] = ['field' => $field, 'operator' => '=', 'value' => $value];
            } elseif (!is_int($key) && is_array($rule) && ctype_digit((string)implode('', array_keys($rule)))) {
                $allRules[] = ['field' => $key, 'operator' => '=', 'value' => $rule];
            } elseif (is_int($key) && is_array($rule) && ctype_digit((string)implode('', array_keys($rule))) && is_array($rule[0])) {
                $allRules[] = self::process($rule);
            } elseif (count($rule) == 1 && isset($rule[0]) && is_string($rule[0])) {
                $allRules[] = $rule[0];
            } else {
                $allRules[] = ['field' => $rule[0], 'operator' => $rule[1], 'value' => $rule[2], 'regex' => $rule[3] ?? false];
            }
        }

        return $allRules;
    }

    /**
     * @param array $simpleRules
     * @param bool $regex
     * @return array
     */
    public static function processSimpleToWhereAndBind(array $simpleRules = [], bool $regex = false, $dbPrefix = null, $type = 'AND', $subBinds = 0)
    {
        if (in_array('OR', $simpleRules)) {
            $type = 'OR';
        } elseif (in_array('AND', $simpleRules)) {
            $type = 'AND';
        }

        $queryParts = [];
        $bind = [];
        foreach ($simpleRules as $rule) {
            if (isset($rule['value']) && is_object($rule['value']) && $rule['value'] instanceof \DateTime) {
                $rule['value'] = (!empty($rule['value']->format('u')) ? $rule['value']->format('Y-m-d H:i:s.u') : $rule['value']->format('Y-m-d H:i:s'));
            }

            if (is_string($rule) && in_array($rule, ['OR', 'AND'])) {
                continue;
            }

            if ($regex || (isset($rule['regex']) && $rule['regex'] == true)) {
                preg_match_all('/\/(.+)\/[a-z]*/', $rule['value'], $matches);

                if (isset($matches[1][0])) {
                    $rule['value'] = $matches[1][0];
                }
            }

            if (is_string($rule)) {
                $queryParts[] = $rule;
            } elseif (is_array($rule) && ctype_digit((string)implode('', array_keys($rule)))) {
                list($subWhere, $subBind) = self::processSimpleToWhereAndBind($rule, $regex, $dbPrefix, ($type == 'AND' ? 'OR' : 'AND'), $subBinds++);
                $queryParts[] = '(' . $subWhere . ')';
                $bind = array_merge($bind, $subBind);
            } elseif (in_array($rule['operator'], ['!=', '<>'])) {
                if (is_array($rule['value']) && count($rule['value']) === 0) {
                    continue;
                } elseif (is_array($rule['value'])) {
                    $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` NOT IN (:" . $bindField . ')';
                } elseif ($rule['value'] === null) {
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` IS NOT null";
                } elseif ($rule['value'] === true) {
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` <> 1";
                } elseif ($rule['value'] === false) {
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` <> 0";
                } else {
                    $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` " . ($regex || (isset($rule['regex']) && $rule['regex'] == true) ? "NOT REGEXP" : "<>") . " :" . $bindField;
                }
            } elseif (in_array($rule['operator'], ['=', '=='])) {
                if (is_array($rule['value']) && count($rule['value']) === 0) {
                    $queryParts[] = 'true = false';
                } elseif (is_array($rule['value'])) {
                    $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` IN (:" . $bindField . ')';
                } elseif ($rule['value'] === null) {
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` IS null";
                } elseif ($rule['value'] === true) {
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` = 1";
                } elseif ($rule['value'] === false) {
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` = 0";
                } else {
                    $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                    $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` " . ($regex || (isset($rule['regex']) && $rule['regex'] == true) ? "REGEXP" : "=") . " :" . $bindField;
                }
            } elseif ($rule['operator'] == '<') {
                $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` < :" . $bindField;
            } elseif ($rule['operator'] == '<=') {
                $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` <= :" . $bindField;
            } elseif ($rule['operator'] == '>') {
                $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` > :" . $bindField;
            } elseif ($rule['operator'] == '>=') {
                $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` >= :" . $bindField;
            } elseif (in_array(strtolower($rule['operator']), ['regex', 'regexp'])) {
                $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` REGEXP :" . $bindField;
            } elseif (in_array(strtolower($rule['operator']), ['!regex', 'not regex', 'not_regex', 'notregex', '!regexp', 'not regexp', 'not_regexp', 'notregexp'])) {
                $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` NOT REGEXP :" . $bindField;
            } elseif ($rule['operator'] == 'LIKE') {
                $bindField = self::addToBind($bind, $subBinds, $rule['field'], $rule['value']);
                $queryParts[] = "`" . $dbPrefix . $rule['field'] . "` LIKE :" . $bindField;
            }
        }

        $where = implode(' ' . $type . ' ', $queryParts);

        return [$where, $bind];
    }

    /**
     * @param array $bind
     * @param int $subBinds
     * @param string $field
     * @param $value
     * @return string
     */
    protected static function addToBind(array &$bind, int $subBinds, string $field, $value): string
    {
        $field = $field . '_' . $subBinds;

        if (!isset($bind[$field])) {
            $oriField = $field;
            $i = 0;

            do {
                $i++;
                $field = $oriField . '_' . $i;
            } while (isset($bind[$field]));
        }

        $bind[$field] = $value;
        return $field;
    }

    public static function splitSimpleAndAdv($allRules, ServiceInterface &$service, $usagesOrStatements = false)
    {
        $advRules = [];
        $simpleRules = [];

        if (in_array('OR', $allRules)) {
            $usagesOrStatements = true;
        }

        foreach ($allRules as $rule) {
            if (is_string($rule) && in_array($rule, ['OR', 'AND'])) {
                continue;
            }

            if (is_array($rule) && ctype_digit((string)implode('', array_keys($rule)))) {
                list($subSimpleRules, $subAdvRules) = self::splitSimpleAndAdv($rule, $service);

                if (count($subAdvRules) > 0) {
                    $advRules[] = $rule;
                } elseif (count($subSimpleRules) > 0) {
                    $simpleRules[] = $rule;
                }
            } elseif (isset($rule['field']) && (strpos($rule['field'], '(') !== false || strpos($rule['field'], '.') !== false || $service->isSimpleDbField($rule['field']) === false)) {
                $advRules[] = $rule;
            } else {
                $simpleRules[] = $rule;
            }
        }

        if (count($advRules) > 0 && $usagesOrStatements) {
            return [[], $allRules];
        }

        return [$simpleRules, $advRules];
    }
}
