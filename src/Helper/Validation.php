<?php

namespace Alexssssss\OrmModel\Helper;

class Validation
{

    /**
     *
     * @param string $str
     * @return bool
     */
    public static function isDate($str)
    {
        if (strlen($str) >= 6) {
            $stamp = strtotime($str);

            if (!is_numeric($stamp)) {
                return false;
            }
            $month = date('m', $stamp);
            $day = date('d', $stamp);
            $year = date('Y', $stamp);

            if (checkdate($month, $day, $year)) {
                return true;
            }
        }

        return false;
    }
}
