<?php

namespace Alexssssss\OrmModel\Helper;

use Alexssssss\OrmModel;

class Wrapper
{

    protected static $cache = [];

    /**
     * @codeCoverageIgnore
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     * @param int $id
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @return \Alexssssss\OrmModel\WrapperInterface
     * @throws OrmModel\Exception\General
     */
    public static function createWrappedProxy(OrmModel\Service\ServiceInterface &$service, int $id = null, OrmModel\Entity\EntityInterface &$entity = null): OrmModel\WrapperInterface
    {
        $wrapperClassName = $service->getWrapperClassName();

        if ($id !== null) {
            $key = $wrapperClassName . '#' . $id;

            if (!isset(self::$cache[$key])) {
                $proxy = Proxy::createEntity($service, $id, $entity);
                $wrapped = new $wrapperClassName($proxy);
                self::$cache[$key] = $wrapped;
            }

            return self::$cache[$key];
        } else {
            $proxy = Proxy::createEntity($service, $id, $entity);
            return new $wrapperClassName($proxy);
        }
    }

    public static function clearCache()
    {
        self::$cache = [];
    }
}
