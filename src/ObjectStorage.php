<?php

namespace Alexssssss\OrmModel;

use Alexssssss\OrmModel\Entity\EntityInterface;

/**
 * ObjectStorage dient gebruikt te worden voor objecten op te slaan. Er kan
 * gebruik gemaakt worden van zoek en sorteermogelijkheden en het object kan ook
 * direct aan bijv. een foreach loop mee gegeven worden om alle objecten één voor
 * één op te halen
 *
 * Class ObjectStorage
 * @package Alexssssss\OrmModel
 */
class ObjectStorage implements ObjectStorageInterface
{

    /**
     *
     * @var Service\ServiceInterface
     */
    protected $service = null;

    /**
     * @var
     */
    protected $serviceName;

    /**
     * Array gevuld met alle objecten
     *
     * @var array
     */
    protected $array = [];

    /**
     * Array met op welke velden er gesorteerd moet worden
     *
     * @var array
     */
    protected $sortFields = [];

    /**
     * Array met hoe er gestorteerd moet worden (op of aflopend)
     *
     * @var array
     */
    protected $sortDirections = [];

    /**
     *
     * @var sting
     */
    protected $entityType;

    /**
     *
     * @param array $arrayOfObjects
     * @param Service\ServiceInterface $service
     * @throws \Exception
     */
    public function __construct(array $arrayOfObjects = [], Service\ServiceInterface $service = null)
    {
        $this->service = $service;

        foreach ($arrayOfObjects as &$object) {
            $this->checkEntityType($object);
            $this->array[$object->getUuid()] = $object;
        }
    }

    /**
     *
     * @param Service\ServiceInterface $service
     */
    public function setService(Service\ServiceInterface $service)
    {
        $this->service = $service;
        $this->serviceName = get_class($service);
    }

    /**
     *
     * @param array $data
     * @param bool $setDefaults
     * @return Entity\EntityInterface
     * @throws \Exception
     */
    public function create(array $data = [], bool $setDefaults = true): Entity\EntityInterface
    {
        $object = $this->service->create($data, $setDefaults);
        $this->attach($object);
        return $object;
    }

    /**
     * Een object toevoegen aan ObjectStorage
     *
     * @param  Entity\EntityInterface $object
     * @return bool
     * @throws \Exception
     */
    public function attach(Entity\EntityInterface &$object): bool
    {
        $this->checkEntityType($object);

        return ($this->array[$object->getUuid()] = $object) ? true : false;
    }

    /**
     * @param int|integer[] $ids
     * @return bool Returns false on error
     */
    public function attachById($ids): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $return = true;
        $objects = $this->service->get($ids);
        foreach ($objects as $object) {
            $return = ($this->array[$object->getUuid()] = $object) ? $return : false;
        }

        return $return;
    }


    /**
     *
     * @param Entity\EntityInterface $object
     * @return bool
     * @throws \Exception
     */
    public function detach(Entity\EntityInterface &$object): bool
    {
        $this->checkEntityType($object);

        return $this->detachByUuid($object->getUuid());
    }

    /**
     * @param $uuid
     * @return bool
     */
    public function detachByUuid($uuid): bool
    {
        if ($object = $this->array[$uuid]) {
            unset($this->array[$uuid]);
            return true;
        }

        return false;
    }

    /**
     *
     * @param array $rules
     * @param bool $regex
     * @return bool ObjectStorage with the detached objects
     */
    public function detachBy(array $rules = [], bool $regex = false): bool
    {
        $objects = $this->get($rules, $regex);

        $return = true;

        foreach ($objects as &$object) {
            $return = $this->detachByUuid($object->getUuid()) ? $return : false;
        }

        return $return;
    }

    /**
     *
     * @param integer|integer[] $ids
     * @return bool Returns false on error
     */
    public function detachById($ids): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $return = true;

        foreach ($ids as $id) {
            foreach ($this->array as &$object) {
                if ($object->id == $id) {
                    $return = $this->detachByUuid($object->getUuid()) ? $return : false;
                }
            }
        }

        return $return;
    }

    /**
     *
     * @return bool
     */
    public function detachAll(): bool
    {
        unset($this->array);
        $this->array = [];
        return true;
    }

    /**
     *
     * @param Entity\EntityInterface $object
     * @param string $field
     * @return mixed
     */
    protected function getFieldOrFuncValue(&$object, $field)
    {

        $parts = explode('.', $field);
        $fieldOrFunc = array_pop($parts);
        $linePath = &$object;

        foreach ($parts as $part) {
            if ($linePath->has($part) === false) {
                return;
            }
            $linePath = &$linePath->{$part};
        }

        if (substr($fieldOrFunc, -2) === '()') {
            $value = $linePath->{substr($fieldOrFunc, 0, -2)}();
        } elseif (($char = strpos($fieldOrFunc, '(')) !== false) {
            $functName = substr($fieldOrFunc, 0, $char);
            $arguments = explode(",", substr($fieldOrFunc, $char + 1, -1));

            foreach ($arguments as &$value) {
                if ($value === 'true') {
                    $value = true;
                } elseif ($value === 'null') {
                    $value = null;
                } elseif ($value === 'false') {
                    $value = false;
                } elseif (is_string($value) && ctype_digit($value)) {
                    $value = (int)$value;
                }
            }

            $value = call_user_func_array([$linePath, $functName], $arguments);
        } else {
            $value = &$linePath->{$fieldOrFunc};
        }

        return $value;
    }

    /**
     * Having functie om te filteren, kan gebruikt worden om alleen objecten
     * terug te geven die voor een bepaalde eigenschap een bepaalde waarden
     * hebben.
     *
     * Voorbeeld: $this->get(['veldA' => '1', 'veldB' => '0']);
     * Voorbeeld: $this->get(['veldA' => '1'])->get(['veldB' => '0']);
     * Voorbeeld: $this->get(['veldA' => ['1', '2']]);
     *
     * Voorbeeld: $this->get(['veldA', '=', '1', 'veldB' => '0']);
     * Voorbeeld: $this->get(['veldA', '=', '1'])->get(['veldB', '=', '0']);
     * Voorbeeld: $this->get(['veldA', '=', ['1', '2']]);
     *
     * Operators: ==, =, <, <=, >=, >, <>, !=
     *
     * @param array $rules
     * @return ObjectStorageInterface
     * @throws \Exception
     */
    public function get(...$rules): ObjectStorageInterface
    {
        if (count($rules) == 1 &&
            is_array($rules[0]) &&
            isset($rules[0][0]) &&
            ctype_digit(implode('', array_keys($rules[0]))) &&
            (is_int($rules[0][0]) || ctype_digit($rules[0][0])) &&
            ctype_digit(implode('', $rules[0]))
        ) {
            $rules = ['id' => $rules[0]];
        } elseif (count($rules) == 2 && isset($rules[0]) && isset($rules[1]) && is_bool($rules[1])) {
            $regex = $rules[1];
            $rules = $rules[0];
        } elseif (count($rules) == 1 && isset($rules[0]) && is_array($rules[0])) {
            $rules = $rules[0];
        }

        if (isset($rules['limit']) && is_int($rules['limit'])) {
            $limit = $rules['limit'];
            unset($rules['limit']);

            if (isset($rules['page']) && is_int($rules['page'])) {
                $page = $rules['page'];
                unset($rules['page']);
            }
        }

        if (isset($rules['sort']) && is_array($rules['sort'])) {
            $sort = $rules['sort'];
            unset($rules['sort']);
        }


        $allRules = Helper\Rules::process($rules);

        $newObjectStorage = $this->createObjectStorageObject();

        foreach ($this->array as &$object) {
            if ($this->processRules($object, $allRules, $regex ?? false)) {
                $newObjectStorage->attach($object);
            }
        }

        if (isset($sort)) {
            $newObjectStorage->sort($sort);
        }

        if (isset($limit)) {
            $this->limit(((($page ?? 1) - 1) * $limit), $limit);
        }

        return $newObjectStorage;
    }

    protected function processRules(EntityInterface &$object, $rules, $regex = false, $type = 'AND')
    {
        if (in_array('OR', $rules)) {
            $type = 'OR';
        } elseif (in_array('AND', $rules)) {
            $type = 'AND';
        }

        $return = ($type == 'AND');
        foreach ($rules as $rule) {
            if (is_string($rule) && in_array($rule, ['OR', 'AND'])) {
                continue;
            }

            if (is_array($rule) && ctype_digit((string)implode('', array_keys($rule)))) {
                if ($this->processRules($object, $rule, $regex, ($type == 'AND' ? 'OR' : 'AND'))) {
                    $return = true;
                } elseif ($type == 'AND') {
                    return false;
                }
            } else {
                $objectFieldValue = $this->getFieldOrFuncValue($object, $rule['field']);

                if ($type == 'AND') {
                    if ((in_array($rule['operator'], ['!=', '<>']) && (($regex == false && ((!is_array($rule['value']) && !($objectFieldValue != $rule['value'])) || (is_array($rule['value']) && !(!in_array($objectFieldValue, $rule['value']))))) || ($regex !== false && !(!preg_match($rule['value'], $objectFieldValue))))) ||
                        (in_array($rule['operator'], ['=', '==']) && (($regex == false && ((!is_array($rule['value']) && !($objectFieldValue == $rule['value'])) || (is_array($rule['value']) && !(in_array($objectFieldValue, $rule['value']))))) || ($regex !== false && !(preg_match($rule['value'], $objectFieldValue))))) ||
                        ($rule['operator'] == '<' && !($objectFieldValue < $rule['value'])) ||
                        ($rule['operator'] == '<=' && !($objectFieldValue <= $rule['value'])) ||
                        ($rule['operator'] == '>' && !($objectFieldValue > $rule['value'])) ||
                        ($rule['operator'] == '>=' && !($objectFieldValue >= $rule['value']))
                    ) {
                        return false;
                    }
                } else {
                    if ((in_array($rule['operator'], ['!=', '<>']) && (($regex == false && ((!is_array($rule['value']) && ($objectFieldValue != $rule['value'])) || (is_array($rule['value']) && (!in_array($objectFieldValue, $rule['value']))))) || ($regex !== false && (!preg_match($rule['value'], $objectFieldValue))))) ||
                        (in_array($rule['operator'], ['=', '==']) && (($regex == false && ((!is_array($rule['value']) && ($objectFieldValue == $rule['value'])) || (is_array($rule['value']) && (in_array($objectFieldValue, $rule['value']))))) || ($regex !== false && (preg_match($rule['value'], $objectFieldValue))))) ||
                        ($rule['operator'] == '<' && ($objectFieldValue < $rule['value'])) ||
                        ($rule['operator'] == '<=' && ($objectFieldValue <= $rule['value'])) ||
                        ($rule['operator'] == '>' && ($objectFieldValue > $rule['value'])) ||
                        ($rule['operator'] == '>=' && ($objectFieldValue >= $rule['value']))
                    ) {
                        return true;
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Functie ter ondersteuning van de sorteer functie
     * @param  mixed $objectA
     * @param  mixed $objectB
     * @param  integer $i
     * @return integer
     */
    protected function objSort($objectA, $objectB, $i = 0)
    {
        $field = $this->sortFields[$i];
        $direction = $this->sortDirections[$i];

        $objectFieldValueA = $this->getFieldOrFuncValue($objectA, $field);
        $objectFieldValueB = $this->getFieldOrFuncValue($objectB, $field);

        if ($objectFieldValueA instanceof \DateTime) {
            $objectFieldValueA = $objectFieldValueA->getTimestamp();
        }

        if ($objectFieldValueB instanceof \DateTime) {
            $objectFieldValueB = $objectFieldValueB->getTimestamp();
        }

        $diff = strnatcmp($objectFieldValueA, $objectFieldValueB) * $direction;

        if ($diff == 0 && isset($this->sortFields[++$i])) {
            $diff = $this->objSort($objectA, $objectB, $i);
        }

        return $diff;
    }

    /**
     * Functie voor het sorteren van de objecten op bepaalde velden
     *
     * Voorbeeld #1: $this->sort(array('veldA' => 'DESC'));
     * Voorbeeld #2: $this->sort(array('veldA' => 'DESC', 'veldB' => 'ASC'));
     *
     * Bij voorbeeld 1 worden de objecten gesorteerd aflopend op basis van de
     * waarde van veldA van de ojecten. Bij voorbeeld twee vind de zelfde
     * sortering plaats maar als objecten de zelfde waarde bij veldA hebben
     * worden ze gesorteerd op veldB oplopen.
     *
     * @param  array $sortFields
     * @return ObjectStorage|Entity\EntityInterface[]
     */
    public function sort(array $sortFields)
    {
        $i = 0;
        foreach ($sortFields as $field => $direction) {
            $this->sortFields[$i] = $field;
            strtolower($direction) == "desc" ? $this->sortDirections[$i] = -1 : $this->sortDirections[$i] = 1;
            $i++;
        }

        // lazy loading can change the array so we need to suppress error's
        @usort($this->array, [$this, "objSort"]);

        $this->sortFields = [];
        $this->sortDirections = [];

        return $this;
    }

    /**
     * Functie voor het distinct sorteren van de objecten aan de hand van een veld en teruggeven als array
     * @param string $field
     * @return array
     */
    public function distinct(string $field): array
    {
        $output = [];

        foreach ($this->array as &$object) {
            $value = $this->getFieldOrFuncValue($object, $field);

            if (!in_array($value, $output)) {
                $output[] = $value;
            }
        }

        return $output;
    }

    /**
     * Count functie mbt Countable, voor het weergeven van het aantal objecten
     * @return integer
     */
    public function count() : int
    {
        return count($this->array);
    }

    /**
     * De waarde van een veld optellen en een totaal terug geven
     * @param  string $field
     * @return mixed
     */
    public function sum(string $field)
    {
        $sum = 0;
        foreach ($this->array as &$object) {
            $sum += (float)$this->getFieldOrFuncValue($object, $field);
        }

        return $sum;
    }

    /**
     * Gemiddelde van een veld berekenen
     * @param  string $field
     * @return mixed
     */
    public function avg(string $field)
    {
        $sum = 0;
        $count = 0;
        foreach ($this->array as &$object) {
            $sum += (float)$this->getFieldOrFuncValue($object, $field);
            $count++;
        }

        return $sum / $count;
    }

    /**
     * De laagste waarde ophalen van een bepaald veld
     * @param  string $field
     * @return mixed
     */
    public function min(string $field)
    {
        $min = false;

        foreach ($this->array as &$object) {
            $objectFieldValue = $this->getFieldOrFuncValue($object, $field);

            if ($min === false ||
                !Helper\Validation::isDate($objectFieldValue) && $min > $objectFieldValue ||
                Helper\Validation::isDate($objectFieldValue) && strtotime($min) > strtotime($objectFieldValue)
            ) {
                $min = $objectFieldValue;
            }
        }

        return $min;
    }

    /**
     * De hoogste waarde van een bepaald veld achterhalen
     * @param  string $field
     * @return mixed
     */
    public function max(string $field)
    {
        $max = false;

        foreach ($this->array as &$object) {
            $objectFieldValue = $this->getFieldOrFuncValue($object, $field);

            if ($max === false ||
                !Helper\Validation::isDate($objectFieldValue) && $max < $objectFieldValue ||
                Helper\Validation::isDate($objectFieldValue) && strtotime($max) < strtotime($objectFieldValue)
            ) {
                $max = $objectFieldValue;
            }
        }

        return $max;
    }

    /**
     *
     * @return Entity\EntityInterface
     */
    public function getFirst(): ?EntityInterface
    {
        return reset($this->array) ?: null;
    }

    /**
     *
     * @return Entity\EntityInterface
     */
    public function getLast(): ?EntityInterface
    {
        return end($this->array) ?: null;
    }

    /**
     *
     * @param integer $startOrMax
     * @param bool|false|int $max
     * @return Entity\EntityInterface[]|ObjectStorage
     */
    public function limit($startOrMax = 0, $max = false)
    {
        if ($max === false) {
            return $this->createObjectStorageObject(array_slice($this->array, 0, $startOrMax));
        } else {
            return $this->createObjectStorageObject(array_slice($this->array, $startOrMax, $max));
        }
    }

    /**
     * Functie om in één keer een veld bij alle objecten in de objectstorage aan te passen
     * @param  array $values
     * @param bool $validate
     * @param array $args
     * @return bool
     */
    public function set(array $values = [], bool $validate = true, array &$args = []): bool
    {
        foreach ($this->array as &$object) {
            foreach ($values as $field => $value) {
                if (strpos($field, '.') !== false) {
                    $parts = explode('.', $field);

                    $lastPart = array_pop($parts);

                    $linePath = &$object;
                    foreach ($parts as $part) {
                        if ($linePath->has($part) === false) {
                            continue 2;
                        }
                        $linePath = &$linePath->{$part};
                    }

                    if ($linePath->{$lastPart} !== $value) {
                        $linePath->{$lastPart} = $value;
                    }
                } elseif ($object->{$field} !== $value) {
                    $object->{$field} = $value;
                }
            }
        }

        return $this->saveAll($validate, false, $args);
    }

    /**
     * @param $function
     * @throws \Exception
     */
    public function exec($function)
    {
        if (strpos($function, '(') === false) {
            throw new \Exception('Only function call\'s are allowed');
        }

        foreach ($this->array as &$object) {
            $this->getFieldOrFuncValue($object, $function);
        }
    }

    /**
     * @codeCoverageIgnore
     * @param array $arrayOfObjects
     * @return ObjectStorage|Entity\EntityInterface[]
     * @throws \Exception
     */
    protected function createObjectStorageObject(array $arrayOfObjects = [])
    {
        return new ObjectStorage($arrayOfObjects, $this->service);
    }

    /**
     *
     * @param Entity\EntityInterface $object
     * @throws \Exception
     */
    protected function checkEntityType(Entity\EntityInterface $object)
    {
        if (is_a(is_a($object, 'Alexssssss\\OrmModel\\WrapperInterface') ? $object->getRealObject(false) : $object, 'Alexssssss\\OrmModel\\Entity\\Proxy\\AbstractEntity')) {
            $objectType = $object->getEntityType();
        } else {
            $objectType = get_class($object);
        }

        if ($this->entityType == null) {
            $this->entityType = $objectType;
        } elseif ($this->entityType != $objectType) {
            throw new \Exception('Can\'t mix entities of multiple types in one object storage');
        }
    }

    public function getOne(...$rules): ?EntityInterface
    {
        if (count($rules) == 1 && isset($rules[0]) && (is_int($rules[0]) || (is_string($rules[0]) && ctype_digit($rules[0])))) {
            $rules = ['id' => $rules[0]];
        } elseif ((count($rules) == 1 && isset($rules[0]) && !is_array($rules[0]) && !(is_int($rules[0]) || (is_string($rules[0]) && ctype_digit($rules[0])))) ||
            (isset($rules[0]['id']) && !(is_int($rules[0]['id']) || (is_string($rules[0]['id']) && ctype_digit($rules[0]['id']))))
        ) {
            return null;
        } elseif (count($rules) == 2 && isset($rules[0]) && isset($rules[1]) && is_bool($rules[1])) {
            $regex = $rules[1];
            $rules = $rules[0];
        } elseif (count($rules) == 1 && isset($rules[0]) && is_array($rules[0])) {
            $rules = $rules[0];
        }

        $allRules = Helper\Rules::process($rules);

        foreach ($this->array as &$object) {
            if ($this->processRules($object, $allRules, $regex ?? false)) {
                return $object;
            }
        }
        return null;
    }

    /**
     * @param bool $detach
     * @return bool
     */
    public function deleteAll(bool $detach = true): bool
    {
        foreach ($this->array as &$object) {
            /* @var $object Entity\EntityInterface */
            $object->delete();
        }

        if ($detach === true) {
            unset($this->array);
            $this->array = [];
        }

        return true;
    }

    /**
     * @param array $rules
     * @param bool $regex
     * @param bool $detach
     * @return bool
     * @throws \Exception
     */
    public function deleteBy(array $rules = [], bool $regex = false, bool $detach = true): bool
    {
        $return = true;
        foreach ($this->get($rules, $regex) as $object) {
            $return = $this->deleteById($object->id, $detach) ? $return : false;
        }

        return $return;
    }

    /**
     * @param int $id
     * @param bool $detach
     * @return bool
     */
    public function deleteById(int $id, bool $detach = true): bool
    {
        $return = false;
        foreach ($this->array as $key => &$object) {
            /* @var $object Entity\EntityInterface */
            if ($object->id == $id) {
                $object->delete();

                if ($detach === true) {
                    unset($this->array[$key]);
                }

                $return = true;
            }
        }
        return $return;
    }

    /**
     * @param EntityInterface $object
     * @param bool $detach
     * @return bool
     */
    public function delete(Entity\EntityInterface &$object, bool $detach = true): bool
    {
        return $object->delete() && ($detach == false || $this->detachByUuid($object->getUuid()));
    }

    /**
     * @param bool $validate
     * @param bool $includeSubObjects
     * @param array $args
     * @return bool
     */
    public function saveAll(bool $validate = true, bool $includeSubObjects = true, array &$args = []): bool
    {
        if ($validate === true) {
            // check if all the objects are valid
            foreach ($this->array as &$object) {
                /* @var $object Entity\EntityInterface */
                if ($includeSubObjects === true) {
                    $object->validateAll();
                } else {
                    $object->validate();
                }
            }
        }

        // save all objects
        foreach ($this->array as &$object) {
            /* @var $object Entity\EntityInterface */
            if ($includeSubObjects === true) {
                $object->saveAll(false, $args);
            } else {
                $object->save(false, $args);
            }
        }

        return true;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator() : \Traversable
    {
        return new \ArrayIterator($this->array);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return count($this->array) == 0 ? true : false;
    }

    public function __sleep()
    {
        $this->serviceName = get_class($this->service);
        return ['serviceName', 'array', 'sortFields', 'sortDirections', 'entityType'];
    }

    public function __wakeup()
    {
        $this->service = ($this->serviceName)::getInstance();
    }

    public function getByUuid($uuid): ?EntityInterface
    {
        return $this->array[$uuid] ?? null;
    }

    public function getByUuids(array $uuids)
    {
        $newObjectStorage = $this->createObjectStorageObject();

        foreach ($uuids as $uuid) {
            if ($object = $this->getByUuid($uuid)) {
                $newObjectStorage->attach($object);
            }
        }

        return $newObjectStorage;
    }

    public function buildIndex($field): array
    {
        $index = [];

        foreach ($this->array as &$object) {
            if ($key = $object->{$field}) {
                if (!isset($index[$key])) {
                    $index[$key] = [];
                }

                $index[$key][] = $object->getUuid();
            }
        }

        return $index;
    }

    public function toJson()
    {
        return json_encode(array_values($this->array));
    }

    public function __toString()
    {
        return $this->toJson();
    }

    public function toArray(): array
    {
        return $this->array;
    }
}
