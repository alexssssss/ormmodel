<?php

namespace Alexssssss\OrmModel;

/**
 * Interface ObjectStorageInterface
 * @package Alexssssss\OrmModel
 */
interface ObjectStorageInterface extends \IteratorAggregate, \Countable
{

    /**
     * @param Entity\EntityInterface $object
     * @return bool
     */
    public function attach(Entity\EntityInterface &$object): bool;

    /**
     * @param integer|integer[] $ids
     * @return bool
     */
    public function attachById($ids): bool;

    /**
     * @param Entity\EntityInterface $object
     * @return bool
     */
    public function detach(Entity\EntityInterface &$object): bool;

    /**
     * @param array $rules
     * @param bool $regex
     * @return bool
     */
    public function detachBy(array $rules = [], bool $regex = false): bool;

    /**
     * @param integer|integer[] $ids
     * @return bool
     */
    public function detachById($ids): bool;

    /**
     * @return bool
     */
    public function detachAll(): bool;

    /**
     * @param array $rules
     * @return ObjectStorageInterface
     */
    public function get(...$rules): ObjectStorageInterface;

    /**
     *
     * @param array $sortFields
     */
    public function sort(array $sortFields);

    /**
     *
     * @param string $field
     */
    public function sum(string $field);

    /**
     *
     * @param string $field
     */
    public function avg(string $field);

    /**
     *
     * @param string $field
     */
    public function min(string $field);

    /**
     *
     * @param string $field
     */
    public function max(string $field);

    /**
     * @return Entity\EntityInterface|null
     */
    public function getFirst(): ? Entity\EntityInterface;

    /**
     * @return Entity\EntityInterface|null
     */
    public function getLast(): ? Entity\EntityInterface;

    /**
     *
     * @param array $rules
     * @return Entity\EntityInterface|null
     */
    public function getOne(...$rules): ? Entity\EntityInterface;

    /**
     *
     * @param integer $startOrMax
     * @param bool|integer $max
     * @return mixed
     */
    public function limit($startOrMax = 0, $max = false);

    /**
     *
     * @param array $values
     * @return bool
     */
    public function set(array $values = [], bool $validate = true, array &$args = []) : bool;

    /**
     * @param array $data
     * @param bool $setDefaults
     * @return Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true): Entity\EntityInterface;

    /**
     * @param bool $detach
     * @return bool
     */
    public function deleteAll(bool $detach = true): bool;

    /**
     * @param array $rules
     * @param bool $regex
     * @param bool $detach
     * @return bool
     */
    public function deleteBy(array $rules = [], bool $regex = false, bool $detach = true): bool;

    /**
     * @param int $id
     * @param bool $detach
     * @return bool
     */
    public function deleteById(int $id, bool $detach = true): bool;

    /**
     * @param Entity\EntityInterface $object
     * @param bool $detach
     * @return bool
     */
    public function delete(Entity\EntityInterface &$object, bool $detach = true): bool;


    /**
     * @param bool $validate
     * @param bool $includeSubObjects
     * @param array $args
     * @return bool
     */
    public function saveAll(bool $validate = true, bool $includeSubObjects = true, array &$args = []): bool;

    /**
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * @param string $field
     * @return array
     */
    public function distinct(string $field): array;

    public function __toString();

    public function toJson();

    public function toArray(): array;
}
