<?php

namespace Alexssssss\OrmModel;

use Alexssssss\OrmModel\Entity\EntityInterface;
use Alexssssss\OrmModel\Helper\Rules;

/**
 * Class ObjectStorageProxy
 * @package Alexssssss\OrmModel
 */
class ObjectStorageProxy extends AbstractProxy implements ObjectStorageInterface
{

    /**
     *
     * @var string
     */
    protected $where;

    /**
     *
     * @var array
     */
    protected $bind = [];

    /**
     *
     * @var Service\ServiceInterface
     */
    protected $service;

    /**
     * @var string
     */
    protected $serviceName;

    /**
     *
     * @var ObjectStorageInterface
     */
    protected $objectStorage;

    /**
     *
     * @var bool
     */
    protected $loaded = false;

    /**
     * If the object hasn't been loaded yet and the count method is triggerd is value is stored here and reused until je object is loaded or destroyed
     *
     * @var int
     */
    protected $cachedPreloadCount;

    /**
     * @var array|ObjectStorageProxy
     */
    protected $objectStorageProxies = [];

    /**
     *
     * @param string $where
     * @param array $bind
     * @param Service\ServiceInterface $service
     *
     */
    public function __construct(string $where, array $bind, Service\ServiceInterface &$service)
    {
        $this->where = $where;
        $this->bind = $bind;
        $this->service = $service;

        $this->objectStorage = new ObjectStorage([], $this->service);
    }

    /**
     *
     * @param array $data
     * @param bool $setDefaults
     * @return Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true): Entity\EntityInterface
    {
        $this->cachedPreloadCount = null;

        $entity = $this->objectStorage->create($data, $setDefaults);

        $this->objectStorage->attach($entity);

        return $entity;
    }

    /**
     * @param $fun
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function __call($fun, $params)
    {
        $this->loadObjectStorage();

        if (is_callable([$this->objectStorage, $fun])) {
            return call_user_func_array([$this->objectStorage, $fun], $params);
        } else {
            throw new Exception\UndefinedMethod('Call to undefined method ' . get_class($this) . '::' . $fun . '()');
        }
    }

    /**
     *
     * @return ObjectStorageInterface|Entity\EntityInterface[]
     * @throws \Exception
     */
    protected function loadObjectStorage(): ObjectStorageInterface
    {
        if ($this->loaded === false) {
            $this->loaded = true;

            $objects = $this->service->get($this->where, $this->bind);

            if ($objects->count() > 0) {
                foreach ($objects as $object) {
                    $entity = Helper\Wrapper::createWrappedProxy($this->service, $object->id, $object);
                    $this->objectStorage->attach($entity);
                }
            }
        }

        return $this->objectStorage;
    }

    /**
     *
     * @param Entity\EntityInterface $object
     * @return bool
     */
    public function attach(Entity\EntityInterface &$object): bool
    {
        $this->cachedPreloadCount = null;

        return $this->objectStorage->attach($object);
    }

    /**
     * @param int|integer[] $ids
     * @return bool
     */
    public function attachById($ids): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $this->cachedPreloadCount = null;

        $return = true;

        $objects = $this->service->get($ids);
        foreach ($objects as $object) {
            $object = Helper\Wrapper::createWrappedProxy($this->service, $object->id, $object);
            $return = $this->objectStorage->attach($object) ? $return : false;
        }

        return $return;
    }


    /**
     *
     * @param integer|integer[] $ids
     * @return bool Returns false on error
     */
    public function detachById($ids): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $return = true;

        foreach ($ids as $id) {
            $this->cachedPreloadCount = null;

            if ($this->objectStorage->count() !== 0) {
                $return = $this->objectStorage->detachById($id) ? $return : false;
            }
        }

        return $return;
    }

    /**
     * @param array $sortFields
     * @return $this
     * @throws \Exception
     */
    public function sort(array $sortFields)
    {
        if ($this->isLoaded() === false) {
            $onlySimpleDbField = true;
            $orderByStringParts = [];

            foreach ($sortFields as $field => $order) {
                if (strpos($field, '(') !== false ||
                    strpos($field, '.') !== false ||
                    $this->service->isSimpleDbField($field) === false
                ) {
                    $onlySimpleDbField = false;
                    break;
                }

                $orderByStringParts[] = "`" . $this->service->getDataTable() . "`.`" . $this->service->getDataFieldPrefix() . $field . "` " . $order;
            }

            if ($onlySimpleDbField == true) {
                if (empty($this->where)) {
                    $where = 'true = true';
                } else {
                    $where = '`' . $this->service->getDataTable() . '`.`' . $this->service->getDataFieldPrefix() . 'id` IN (SELECT `' . $this->service->getDataFieldPrefix() . 'id` FROM `' . $this->service->getDataTable() . '` WHERE ' . $this->where . ')';
                }
                $this->where = $where . ' ORDER BY ' . implode(', ', $orderByStringParts);

                return $this;
            }
        }

        $this->loadObjectStorage();
        $this->objectStorage->sort($sortFields);
        return $this;
    }

    /**
     * @return bool
     */
    public function isLoaded(): bool
    {
        return ($this->loaded === false && $this->objectStorage->count() === 0) ? false : true;
    }

    /**
     *
     * @param string $field
     * @return mixed
     * @throws \Exception
     */
    public function sum(string $field)
    {
        if ($this->isLoaded() === false &&
            strpos($field, '(') === false &&
            strpos($field, '.') === false &&
            $this->service->isSimpleDbField($field)
        ) {
            return $this->service->sum($field, $this->where, $this->bind);
        } else {
            $this->loadObjectStorage();
            return $this->objectStorage->sum($field);
        }
    }

    /**
     *
     * @param string $field
     * @return mixed
     * @throws \Exception
     */
    public function avg(string $field)
    {
        if ($this->isLoaded() === false &&
            strpos($field, '(') === false &&
            strpos($field, '.') === false &&
            $this->service->isSimpleDbField($field)
        ) {
            return $this->service->avg($field, $this->where, $this->bind);
        } else {
            $this->loadObjectStorage();
            return $this->objectStorage->avg($field);
        }
    }

    /**
     *
     * @param string $field
     * @return mixed
     * @throws \Exception
     */
    public function min(string $field)
    {
        if ($this->isLoaded() === false &&
            strpos($field, '(') === false &&
            strpos($field, '.') === false &&
            $this->service->isSimpleDbField($field)
        ) {
            return $this->service->min($field, $this->where, $this->bind);
        } else {
            $this->loadObjectStorage();
            return $this->objectStorage->min($field);
        }
    }

    /**
     *
     * @param string $field
     * @return mixed
     * @throws \Exception
     */
    public function max(string $field)
    {
        if ($this->isLoaded() === false &&
            strpos($field, '(') === false &&
            strpos($field, '.') === false &&
            $this->service->isSimpleDbField($field)
        ) {
            return $this->service->max($field, $this->where, $this->bind);
        } else {
            $this->loadObjectStorage();
            return $this->objectStorage->max($field);
        }
    }

    /**
     *
     * @return Entity\EntityInterface
     * @throws \Exception
     */
    public function getFirst(): ?EntityInterface
    {
        $this->loadObjectStorage();
        return $this->objectStorage->getFirst();
    }

    /**
     *
     * @return Entity\EntityInterface
     * @throws \Exception
     */
    public function getLast(): ?EntityInterface
    {
        $this->loadObjectStorage();
        return $this->objectStorage->getLast();
    }

    /**
     * @param int $startOrMax
     * @param bool $max
     * @return mixed
     * @throws \Exception
     */
    public function limit($startOrMax = 0, $max = false)
    {
        if ($this->loaded === false) {
            if (empty($this->where)) {
                $this->where = 'true = true';
                // Fix: onderstaande is veiliger maar werkt niet in combi met sort
                // } else {
                //    $this->where = '`' . $this->service->getDataTable() . '`.`' . $this->service->getDataFieldPrefix() . 'id` IN (SELECT `' . $this->service->getDataFieldPrefix() . 'id` FROM `' . $this->service->getDataTable() . '` WHERE ' . $this->where . ')';
            }

            if ($max == false) {
                $this->where .= ' LIMIT ' . $startOrMax;
            } else {
                $this->where .= ' LIMIT ' . $startOrMax . ', ' . $max;
            }

            return $this;
        }

        $this->loadObjectStorage();
        return $this->objectStorage->limit($startOrMax, $max);
    }

    /**
     *
     * @param array $values
     * @param bool $validate
     * @param array $args
     * @return bool
     * @throws \Exception
     */
    public function set(array $values = [], bool $validate = true, array &$args = []): bool
    {
        if ($this->isLoaded()) {
            $this->loadObjectStorage();
            return $this->objectStorage->set($values, $validate, $args);
        } else {
            return $this->service->set($this->where, $this->bind, $values, $validate, $args);
        }
    }

    /**
     *
     * @param array $args
     * @return EntityInterface|null
     * @throws Exception\General
     * @throws \Exception
     */
    public function getOne(...$rules): ?EntityInterface
    {
        if (count($rules) == 1 && isset($rules[0]) && (is_int($rules[0]) || (is_string($rules[0]) && ctype_digit($rules[0])))) {
            $rules = ['id' => $rules[0]];
        } elseif ((count($rules) == 1 && isset($rules[0]) && !is_array($rules[0]) && !(is_int($rules[0]) || (is_string($rules[0]) && ctype_digit($rules[0])))) ||
            (isset($rules[0]['id']) && !(is_int($rules[0]['id']) || (is_string($rules[0]['id']) && ctype_digit($rules[0]['id']))))
        ) {
            return null;
        } elseif (count($rules) == 2 && isset($rules[0]) && isset($rules[1]) && is_bool($rules[1])) {
            $regex = $rules[1];
            $rules = $rules[0];
        } elseif (count($rules) == 1 && isset($rules[0]) && is_array($rules[0])) {
            $rules = $rules[0];
        }


        if ($this->loaded === false) {
            $allRules = Rules::process($rules);
            list($simpleRules, $advRules) = Rules::splitSimpleAndAdv($allRules, $this->service);

            if (count($simpleRules) > 0) {
                list($where, $bind) = Rules::processSimpleToWhereAndBind($simpleRules, $regex ?? false, $this->service->getDataFieldPrefix());

                if (empty($where)) {
                    $where = $this->where;
                    $bind = $this->bind;
                } else {
                    if (!empty($this->where)) {
                        $where = '`' . $this->service->getDataTable() . '`.`' . $this->service->getDataFieldPrefix() . 'id` IN (SELECT `' . $this->service->getDataFieldPrefix() . 'id` FROM `' . $this->service->getDataTable() . '` WHERE ' . $this->where . ') AND ' . $where;
                    }
                    $bind = array_merge($this->bind, $bind);
                }

                if ($entity = count($advRules) > 0 ? (new ObjectStorageProxy($where, $bind, $this->service))->getOne($advRules, $regex ?? false) : $this->service->getOne($where, $bind)) {
                    $entity = Helper\Wrapper::createWrappedProxy($this->service, $entity->id, $entity);
                    $this->objectStorage->attach($entity);
                }

                return $entity;
            }
        }

        $this->loadObjectStorage();
        return $this->objectStorage->getOne($rules, $regex ?? false);
    }

    /**
     * @param bool $detach
     * @return bool
     */
    public function deleteAll(bool $detach = true): bool
    {
        $this->cachedPreloadCount = null;

        if ($this->objectStorage->count() > 0) {
            foreach ($this->objectStorage as $object) {
                $object->delete();
            }
        }

        $return = $this->service->deleteWhere($this->where, $this->bind);

        if ($detach === true) {
            $this->detachAll(false);
        }

        return $return;
    }

    /**
     * @return bool
     */
    public function detachAll(): bool
    {
        $this->cachedPreloadCount = null;

        if ($this->objectStorage->count() > 0) {
            return $this->objectStorage->detachAll();
        }

        return true;
    }

    /**
     * @param array $rules
     * @param bool $regex
     * @param bool $detach
     * @return bool
     * @throws \Exception
     */
    public function deleteBy(array $rules = [], bool $regex = false, bool $detach = true): bool
    {
        $this->cachedPreloadCount = null;

        $return = false;

        if ($objects = $this->get($rules, $regex)) {
            $return = $objects->deleteAll();

            if ($detach === true && $this->isLoaded()) {
                $this->detachBy($rules, $regex, false);
            }
        }

        return $return;
    }

    /**
     * @param array $rules
     * @param bool $regex
     * @return bool
     * @throws \Exception
     */
    public function detachBy(array $rules = [], bool $regex = false): bool
    {
        $this->cachedPreloadCount = null;

        $return = true;
        if ($this->objectStorage->count() === 0) {
            return true; // todo: deze waarde is niet 100% juist, wat als het object niet eens voorkomt?
        } else {
            $objects = $this->get($rules, $regex);

            foreach ($objects as $object) {
                $return = $this->objectStorage->detach($object) ? $return : false;
            }
        }

        return $return;
    }

    /**
     *
     * @param array $rules
     * @return ObjectStorageInterface|Entity\EntityInterface[]
     * @throws \Exception
     */
    public function get(...$rules): ObjectStorageInterface
    {
        if (count($rules) == 1 &&
            is_array($rules[0]) &&
            isset($rules[0][0]) &&
            ctype_digit(implode('', array_keys($rules[0]))) &&
            (is_int($rules[0][0]) || (is_string($rules[0][0]) && ctype_digit($rules[0][0]))) &&
            ctype_digit(implode('', $rules[0]))
        ) {
            $rules = ['id' => $rules[0]];
        } elseif (count($rules) == 2 && isset($rules[0]) && isset($rules[1]) && is_bool($rules[1])) {
            $regex = $rules[1];
            $rules = $rules[0];
        } elseif (count($rules) == 1 && isset($rules[0]) && is_array($rules[0])) {
            $rules = $rules[0];
        }

        if ($this->loaded === false) {
            if (isset($rules['limit']) && is_int($rules['limit'])) {
                $limit = $rules['limit'];
                unset($rules['limit']);

                if (isset($rules['page']) && is_int($rules['page'])) {
                    $page = $rules['page'];
                    unset($rules['page']);
                }
            }

            if (isset($rules['sort']) && is_array($rules['sort'])) {
                $sort = $rules['sort'];
                unset($rules['sort']);
            }

            if (empty($rules) && (isset($limit) || isset($sort))) {
                if (isset($sort)) {
                    $this->sort($sort);
                }

                if (isset($limit)) {
                    $this->limit(((($page ?? 1) - 1) * $limit), $limit);
                }

                return $this->get();
            }

            $allRules = Rules::process($rules);
            list($simpleRules, $advRules) = Rules::splitSimpleAndAdv($allRules, $this->service);

            if (count($simpleRules) > 0) {
                list($where, $bind) = Rules::processSimpleToWhereAndBind($simpleRules, $regex ?? false, $this->service->getDataFieldPrefix());

                if (empty($where)) {
                    $where = $this->where;
                    $bind = $this->bind;
                } else {
                    if (!empty($this->where)) {
                        $where = '`' . $this->service->getDataTable() . '`.`' . $this->service->getDataFieldPrefix() . 'id` IN (SELECT `' . $this->service->getDataFieldPrefix() . 'id` FROM `' . $this->service->getDataTable() . '` WHERE ' . $this->where . ') AND ' . $where;
                    }
                    $bind = array_merge($this->bind, $bind);
                }

                $objectStorageProxy = new ObjectStorageProxy($where, $bind, $this->service);

                if (count($advRules) > 0) {
                    if (isset($limit)) {
                        $advRules['limit'] = $limit;
                    }

                    if (isset($page)) {
                        $advRules['page'] = $page;
                    }

                    if (isset($sort)) {
                        $advRules['sort'] = $sort;
                    }

                    $objectStorageProxy = $objectStorageProxy->get($advRules, $regex ?? false);
                } else {
                    if (isset($sort)) {
                        $objectStorageProxy->sort($sort);
                    }

                    if (isset($limit)) {
                        $objectStorageProxy->limit(((($page ?? 1) - 1) * $limit), $limit);
                    }
                }

                $this->objectStorageProxies[] = $objectStorageProxy;
                return $objectStorageProxy;
            }
        }

        $this->loadObjectStorage();
        return $this->objectStorage->get($rules, $regex ?? false);
    }

    /**
     * @param int $id
     * @param bool $detach
     * @return bool
     * @throws \Exception
     */
    public function deleteById(int $id, bool $detach = true): bool
    {
        $return = true;

        $this->cachedPreloadCount = null;

        if ($this->isLoaded()) {
            $return = ($entity = $this->getOne($id)) && $entity->delete() && ($detach === false || $this->detach($entity, false)) ? $return : false;
        } else {
            $return = $this->service->deleteById($id) ? $return : false;
        }

        return $return;
    }

    /**
     * @param EntityInterface $object
     * @param bool $detach
     * @return bool
     */
    public function delete(Entity\EntityInterface &$object, bool $detach = true): bool
    {
        return $object->delete() && ($detach == false || $this->detach($object, false));
    }

    /**
     * @param Entity\EntityInterface $object
     * @return bool
     */
    public function detach(Entity\EntityInterface &$object): bool
    {
        $this->cachedPreloadCount = null;

        return $this->objectStorage->count() > 0 ? $this->objectStorage->detach($object) : true;
    }

    /**
     * @param bool $validate
     * @param bool $includeSubObjects
     * @param array $args
     * @return bool
     * @throws \Exception
     */
    public function saveAll(bool $validate = true, bool $includeSubObjects = true, array &$args = []): bool
    {
        $return = true;

        if (count($this->objectStorageProxies) > 0) {
            foreach ($this->objectStorageProxies as $objectStorageProxy) {
                /* @var $objectStorageProxy \Alexssssss\OrmModel\ObjectStorageProxy */
                $objectStorage = ($objectStorageProxy instanceof FakeObjectInterface ? $objectStorageProxy->getRealObject() : $objectStorageProxy);

                if ($objectStorage->count() > 0) {
                    foreach ($objectStorage as &$entity) {
                        $this->objectStorage->attach($entity);
                    }
                }
            }
        }

        if ($this->objectStorage->count() > 0) {
            $return = $this->objectStorage->saveAll($validate, $includeSubObjects, $args) ? $return : false;
        }

        return $return;
    }

    /**
     * @param bool $load
     * @return ObjectStorageInterface|Entity\EntityInterface[]
     * @throws \Exception
     */
    public function getRealObject(bool $load = true): ObjectStorageInterface
    {
        if ($load) {
            $this->loadObjectStorage();
        }

        return $this->objectStorage;
    }

    public function setRealObject(ObjectStorageInterface $objects)
    {
        if ($this->objectStorage !== null &&
            $this->objectStorage->count() > 0 &&
            (
                $objects->count() != $this->objectStorage->count() ||
                !empty(array_diff($objects->distinct('getUuid()'), $this->objectStorage->distinct('getUuid()'))) ||
                !empty(array_diff($this->objectStorage->distinct('getUuid()'), $objects->distinct('getUuid()')))
            )
        ) {
            throw new \Exception("Can't override objectStorage entities with setRealObject there not the same");
        } else {
            $this->loaded = true;
            $this->objectStorage = ($objects instanceof FakeObjectInterface ? $objects->getRealObject() : $objects);
        }

        return $this;
    }

    /**
     * @return \ArrayIterator|\Traversable
     * @throws \Exception
     */
    public function getIterator() : \Traversable
    {
        $this->loadObjectStorage();
        return $this->objectStorage->getIterator();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isEmpty(): bool
    {
        return $this->count() === 0 ? true : false;
    }

    /**
     * Count functie mbt Countable, voor het weergeven van het aantal objecten
     * @return integer
     * @throws \Exception
     */
    public function count($raw = false): int
    {
        if ($raw === true) {
            return (int)$this->objectStorage->count();
        }

        if ($this->loaded === false) {
            if ($this->cachedPreloadCount === null) {
                $this->cachedPreloadCount = $this->service->count($this->where, $this->bind);
            }
            return $this->cachedPreloadCount + $this->objectStorage->get(['id' => null])->count();
        } else {
            $this->loadObjectStorage();
            return (int)$this->objectStorage->count();
        }
    }

    /**
     * @param string $field
     * @return array
     * @throws \Exception
     */
    public function distinct(string $field): array
    {
        if ($this->isLoaded() === false &&
            strpos($field, '(') === false &&
            strpos($field, '.') === false &&
            $this->service->isSimpleDbField($field)
        ) {
            return $this->service->distinct($field, $this->where, $this->bind);
        } else {
            $this->loadObjectStorage();
            return $this->objectStorage->distinct($field);
        }
    }

    public function __sleep()
    {
        // todo: objectStorageProxies?

        $this->serviceName = get_class($this->service);
        return ['where', 'bind', 'serviceName'];
    }

    public function __wakeup()
    {
        $this->service = ($this->serviceName)::getInstance();
    }

    public function __toString()
    {
        $this->loadObjectStorage();
        return $this->objectStorage->__toString();
    }

    public function toJson()
    {
        $this->loadObjectStorage();
        return $this->objectStorage->toJson();
    }

    public function toArray(): array
    {
        $this->loadObjectStorage();
        return $this->objectStorage->toArray();
    }
}
