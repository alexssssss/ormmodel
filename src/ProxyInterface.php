<?php

namespace Alexssssss\OrmModel;

interface ProxyInterface extends FakeObjectInterface
{

    /**
     * @param $fun
     * @param $params
     * @return mixed
     */
    public function __call($fun, $params);

    public function isLoaded();
}
