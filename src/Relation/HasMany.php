<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

class HasMany extends OrmModel\ObjectStorage implements HasManyInterface
{

    use HasManyTrait,
        HelperTrait;

    /**
     *
     * @var OrmModel\Entity\EntityInterface
     */
    protected $parent;

    /**
     *
     * @var string
     */
    protected $foreignKeyObjectField;

    /**
     *
     * @var string
     */
    protected $foreignKeyField;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param string $foreignKeyField
     * @param array $arrayOfObjects
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     * @throws \Exception
     */
    public function __construct(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, string $foreignKeyField, array $arrayOfObjects, OrmModel\Service\ServiceInterface &$service)
    {
        if (!is_a((is_a($parent, 'Alexssssss\\OrmModel\\WrapperInterface') ? $parent->getRealObject(false) : $parent), 'Alexssssss\\OrmModel\\AbstractProxy')) {
            $parentService = $parent->getService();
            $this->parent = $this->createParentProxy($parentService, $parent, $parent->id);
        } else {
            $this->parent = $parent;
        }

        $this->foreignKeyObjectField = $foreignKeyObjectField;
        $this->foreignKeyField = $foreignKeyField;

        parent::__construct($arrayOfObjects, $service);
    }

    public function __sleep()
    {
        $parent = parent::__sleep();
        return array_merge($parent, ['parent', 'foreignKeyObjectField', 'foreignKeyField']);
    }
}
