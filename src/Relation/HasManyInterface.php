<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

interface HasManyInterface extends OrmModel\ObjectStorageInterface
{
    public function replace(OrmModel\ObjectStorageInterface $objectStorage): bool;
}
