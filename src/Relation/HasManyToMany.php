<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

class HasManyToMany extends OrmModel\ObjectStorage implements HasManyToManyInterface
{
    use HasManyToManyTrait,
        HelperTrait;

    /**
     *
     * @var OrmModel\Entity\EntityInterface
     */
    protected $parent;

    /**
     *
     * @var string
     */
    protected $linkStoreInField;

    /**
     * @var string
     */
    protected $linkSecForeignKeyObjectField;

    /**
     * @var string
     */
    protected $linkSecForeignKeyField;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $linkStoreInField
     * @param string $linkSecForeignKeyObjectField
     * @param string $linkSecForeignKeyField
     * @param array $arrayOfObjects
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     * @throws \Exception
     */
    public function __construct(OrmModel\Entity\EntityInterface &$parent, string $linkStoreInField, string $linkSecForeignKeyObjectField, string $linkSecForeignKeyField, array $arrayOfObjects, OrmModel\Service\ServiceInterface &$service)
    {
        if (!is_a((is_a($parent, 'Alexssssss\\OrmModel\\WrapperInterface') ? $parent->getRealObject(false) : $parent), 'Alexssssss\\OrmModel\\AbstractProxy')) {
            $parentService = $parent->getService();
            $this->parent = $this->createParentProxy($parentService, $parent, $parent->id);
        } else {
            $this->parent = $parent;
        }

        $this->linkStoreInField = $linkStoreInField;
        $this->linkSecForeignKeyObjectField = $linkSecForeignKeyObjectField;
        $this->linkSecForeignKeyField = $linkSecForeignKeyField;

        parent::__construct($arrayOfObjects, $service);
    }

    public function __sleep()
    {
        $parent = parent::__sleep();
        return array_merge($parent, ['parent', 'linkStoreInField', 'linkSecForeignKeyObjectField', 'linkSecForeignKeyField']);
    }
}
