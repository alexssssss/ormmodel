<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

interface HasManyToManyInterface extends OrmModel\ObjectStorageInterface
{

    /**
     *
     * @param array $data
     * @param bool $setDefaults
     * @param array $linkData
     * @param bool $linkSetDefaults
     * @return OrmModel\Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true, array $linkData = [], bool $linkSetDefaults = true): OrmModel\Entity\EntityInterface;
    
    public function replace(OrmModel\ObjectStorageInterface $objectStorage): bool;
}
