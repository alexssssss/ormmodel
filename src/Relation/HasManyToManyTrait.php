<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

/**
 * Trait HasManyToManyTrait
 * @package Alexssssss\OrmModel\Relation
 */
trait HasManyToManyTrait
{

    /**
     * @param array $data
     * @param bool $setDefaults
     * @param array $linkData
     * @param bool $linkSetDefaults
     * @return OrmModel\Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true, array $linkData = [], bool $linkSetDefaults = true): OrmModel\Entity\EntityInterface
    {
        @trigger_error('Create function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $entity = parent::create($data, $setDefaults);

        $linkData[$this->linkSecForeignKeyObjectField] = $entity;

        $entityToSaveAfterSave = $this->parent->{$this->linkStoreInField}->create($linkData, $linkSetDefaults);

        $this->parent->addEntityToSaveAfterSave($entityToSaveAfterSave);
        $entity->addEntityToSaveAfterSave($entityToSaveAfterSave);

        return $entity;
    }

    /**
     * @param OrmModel\Entity\EntityInterface $object
     * @return bool
     */
    public function attach(OrmModel\Entity\EntityInterface &$object): bool
    {
        @trigger_error('Attach function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $linkData = [$this->linkSecForeignKeyObjectField => $object];

        $linkEntity = $this->parent->{$this->linkStoreInField}->create($linkData);

        if ($this->parent->id === null) {
            $this->parent->addEntityToSaveAfterSave($linkEntity);
            $object->addEntityToSaveAfterSave($linkEntity);
        } else {
            if ($object->id === null) {
                $object->save();
            }
            $linkEntity->save(false);
        }

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::attach($object));
    }

    /**
     * @param int|integer[] $ids
     * @return bool Returns false on error
     */
    public function attachById($ids): bool
    {
        @trigger_error('AttachById function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as $id) {
            $linkData = [$this->linkSecForeignKeyField => $id];

            $linkEntity = $this->parent->{$this->linkStoreInField}->create($linkData);

            if ($this->parent->id === null) {
                $this->parent->addEntityToSaveAfterSave($linkEntity);
                $linkEntity->{$this->linkSecForeignKeyObjectField}->addEntityToSaveAfterSave($linkEntity);
            } else {
                $linkEntity->save(false);
            }
        }

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::attachById($ids));
    }

    /**
     * @param OrmModel\Entity\EntityInterface $object
     * @return bool
     */
    public function detach(OrmModel\Entity\EntityInterface &$object): bool
    {
        @trigger_error('Detach function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $linkData = [$this->linkSecForeignKeyField => $object->id];

        $this->parent->{$this->linkStoreInField}->deleteBy($linkData);

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::detach($object));
    }

    /**
     * @param array $rules
     * @param bool $regex
     * @return bool
     */
    public function detachBy(array $rules = [], bool $regex = false): bool
    {
        @trigger_error('DetachBy function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        if ($objects = parent::get($rules, $regex)) {
            $ids = [];
            foreach ($objects as $object) {
                $ids[] = $object->id;
            }

            $linkData = [$this->linkSecForeignKeyField => $ids];

            $this->parent->{$this->linkStoreInField}->deleteBy($linkData);

            return parent::detachBy($rules, $regex);
        }

        return true;
    }

    /**
     * @param integer|integer[] $ids
     * @return bool
     */
    public function detachById($ids): bool
    {
        @trigger_error('DetachById function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $linkData = [$this->linkSecForeignKeyField => $ids];

        $this->parent->{$this->linkStoreInField}->deleteBy($linkData);

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::detachById($ids));
    }

    /**
     * @return bool
     */
    public function detachAll(): bool
    {
        @trigger_error('DetachAll function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $this->parent->{$this->linkStoreInField}->deleteAll();

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::detachAll());
    }

    /**
     * @param bool $detach
     * @return bool
     */
    public function deleteAll(bool $detach = true): bool
    {
        @trigger_error('DeleteAll function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $this->parent->{$this->linkStoreInField}->deleteAll($detach);

        return parent::deleteAll($detach);
    }

    /**
     * @param array $rules
     * @param bool $regex
     * @param bool $detach
     * @return mixed
     */
    public function deleteBy(array $rules = [], bool $regex = false, bool $detach = true): bool
    {
        @trigger_error('DeleteBy function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        if ($objects = parent::get($rules, $regex)) {
            $ids = [];
            foreach ($objects as $object) {
                $ids[] = $object->id;
            }

            $linkData = [$this->linkSecForeignKeyField => $ids];

            $this->parent->{$this->linkStoreInField}->deleteBy($linkData);

            return parent::deleteBy($rules, $regex, $detach);
        }

        return true;
    }

    /**
     * @param int $id
     * @param bool $detach
     * @return mixed
     */
    public function deleteById(int $id, bool $detach = true): bool
    {
        @trigger_error('DeleteById function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $linkData = [$this->linkSecForeignKeyField => $id];

        $this->parent->{$this->linkStoreInField}->deleteBy($linkData);

        return parent::deleteById($id, $detach);
    }

    /**
     * @param EntityInterface $object
     * @param bool $detach
     * @return bool
     */
    public function delete(OrmModel\Entity\EntityInterface &$object, bool $detach = true): bool
    {
        @trigger_error('Delete function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        return ($object->id !== null && $this->deleteById($object->id)) || parent::delete($object, $detach);
    }

    public function replace(OrmModel\ObjectStorageInterface $objectStorage): bool
    {
        @trigger_error('Replace function is depricated on many-to-many objects', \E_USER_DEPRECATED);
        $this->deleteBy([$this->linkSecForeignKeyField, '!=', $objectStorage->distinct('id')]);
        $existingIds = $this->distinct($this->linkSecForeignKeyField);

        // todo: sub optimaal maar werkt wel. De Object storage set snapt addEntityToSaveAfterSave niet.
        foreach ($objectStorage as $object) {
            if ($object->id === null || !in_array($object->id, $existingIds)) {
                $linkData = [$this->linkSecForeignKeyObjectField => $object];

                $linkEntity = $this->parent->{$this->linkStoreInField}->create($linkData);

                if ($this->parent->id === null) {
                    $this->parent->addEntityToSaveAfterSave($linkEntity);
                    $object->addEntityToSaveAfterSave($linkEntity);
                } else {
                    if ($object->id === null) {
                        $object->save();
                    }
                    $linkEntity->save(false);
                }

                if (!$this instanceof OrmModel\ObjectStorageProxy || $this->isLoaded() == true || $object->id == null) {
                    parent::attach($object);
                }
            }
        }

        return true;
    }
}
