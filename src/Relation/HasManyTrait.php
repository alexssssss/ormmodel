<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

/**
 * Trait HasManyTrait
 * @package Alexssssss\OrmModel\Relation
 */
trait HasManyTrait
{

    /**
     *
     * @param array $data
     * @param bool $setDefaults
     * @return OrmModel\Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true): OrmModel\Entity\EntityInterface
    {
        $data[$this->foreignKeyObjectField] = $this->parent;
        return parent::create($data, $setDefaults);
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $object
     * @return bool
     */
    public function attach(OrmModel\Entity\EntityInterface &$object): bool
    {
        $object->{$this->foreignKeyObjectField} = $this->parent;

        if ($this->parent->id !== null) {
            $object->save();
        } else {
            $this->parent->addEntityToSaveAfterSave($object);
        }

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::attach($object));
    }

    /**
     * @param int|integer[] $ids
     * @return bool Returns false on error
     */
    public function attachById($ids): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $this->service->setById($ids, [$this->foreignKeyField => $this->parent->id]);

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::attachById($ids));
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $object
     * @return OrmModel\Entity\EntityInterface
     */
    public function detach(OrmModel\Entity\EntityInterface &$object, bool $save = true): bool
    {
        $object->{$this->foreignKeyField} = null;

        if ($save) {
            if ($object->id !== null && $this->parent->id !== null) {
                $object->save();
            } elseif ($object->id !== null) {
                $this->parent->addEntityToSaveAfterSave($object);
            }
        }

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::detach($object));
    }

    /**
     *
     * @param array $rules
     * @param bool $regex
     * @return OrmModel\ObjectStorageInterface ObjectStorage with the detached objects
     */
    public function detachBy(array $rules = [], bool $regex = false, bool $save = true): bool
    {
        if ($save) {
            parent::get($rules, $regex)->set([$this->foreignKeyField => null]);
        }

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::detachBy($rules, $regex));
    }

    /**
     * @param integer|integer $ids
     * @return bool
     */
    public function detachById($ids): bool
    {
        $this->service->setById($ids, [$this->foreignKeyField => null]);

        return (($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) || parent::detachById($ids));
    }

    /**
     * @return bool
     */
    public function detachAll(bool $save = true): bool
    {
        if ($this instanceof OrmModel\ObjectStorageProxy && $this->isLoaded() == false) {
            return $save ? $this->service->set($this->where, $this->bind, [$this->foreignKeyField => null]) : true;
        } else {
            // todo: dit lijkt niet goed te gaan, later saveAll op de parent en de FK lijkt nog ingesteld
            if ($save) {
                $this->set([$this->foreignKeyField => null]);
            }

            return parent::detachAll();
        }
    }

    /**
     * @param OrmModel\ObjectStorageInterface $objectStorage
     * @return bool
     */
    public function replace(OrmModel\ObjectStorageInterface $objectStorage): bool
    {
        $this->detachBy(['id', '!=', $objectStorage->distinct('id')]);

        $existingIds = $this->distinct('id');

        // todo: sub optimaal maar werkt wel. De Object storage set snapt addEntityToSaveAfterSave niet.
        foreach ($objectStorage as $object) {
            if ($object->id === null || !in_array($object->id, $existingIds)) {
                $object->{$this->foreignKeyObjectField} = $this->parent;

                if ($this->parent->id !== null) {
                    $object->save();
                } else {
                    $this->parent->addEntityToSaveAfterSave($object);
                }

                if (!$this instanceof OrmModel\ObjectStorageProxy || $this->isLoaded() == true || $object->id == null) {
                    parent::attach($object);
                }
            }
        }

        return true;
    }

    /**
     * @param bool $detach
     * @return bool
     */
    public function deleteAll(bool $detach = true): bool
    {
        if (!$this instanceof OrmModel\ObjectStorageProxy || $this->isLoaded() == true) {
            $objects = $this->get(['id' => null]);

            foreach ($objects as $object) {
                $this->parent->addEntityToDeleteBeforeSave($object);
            }
        }

        return parent::deleteAll($detach);
    }

    /**
     * @param array $rules
     * @param bool $regex
     * @param bool $detach
     * @return bool
     */
    public function deleteBy(array $rules = [], bool $regex = false, bool $detach = true): bool
    {
        if (!$this instanceof OrmModel\ObjectStorageProxy || $this->isLoaded() == true) {
            // todo: controlleren of dit hier onder niet alle entities eerst inlaad
            $objects = $this->get(['id' => null])->get($rules, $regex);

            foreach ($objects as $object) {
                $this->parent->addEntityToDeleteBeforeSave($object);
            }
        }

        return parent::deleteBy($rules, $regex, $detach);
    }

    /**
     * @param OrmModel\Entity\EntityInterface $object
     * @param bool $detach
     * @return bool
     */
    public function delete(OrmModel\Entity\EntityInterface &$object, bool $detach = true): bool
    {
        if ($object->id == null) {
            $this->parent->addEntityToDeleteBeforeSave($object);
        }

        return parent::delete($object, $detach);
    }
}
