<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;
use Alexssssss\OrmModel\Entity;

class HasOne extends OrmModel\Entity\Proxy\AbstractEntity implements Entity\EntityInterface, HasOneInterface
{

    use HasOneTrait,
        HelperTrait;

    /**
     *
     * @var OrmModel\Entity\EntityInterface
     */
    protected $parent;

    /**
     *
     * @var string
     */
    protected $foreignKeyObjectField;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     */
    public function __construct(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, OrmModel\Service\ServiceInterface &$service)
    {
        if (!is_a((is_a($parent, 'Alexssssss\\OrmModel\\WrapperInterface') ? $parent->getRealObject(false) : $parent), 'Alexssssss\\OrmModel\\AbstractProxy')) {
            $parentService = $parent->getService();
            $this->parent = $this->createParentProxy($parentService, $parent, $parent->id);
        } else {
            $this->parent = $parent;
        }

        $this->service = $service;
        $this->foreignKeyObjectField = $foreignKeyObjectField;
    }

    /**
     * @param Entity\EntityInterface|null $entity
     * @return mixed|void
     */
    protected function setEntity(Entity\EntityInterface $entity = null)
    {
        if ($entity !== null) {
            $this->entity = $entity;
        }
    }

    public function __sleep()
    {
        $parent = parent::__sleep();
        return array_merge($parent, ['parent', 'foreignKeyObjectField']);
    }
}
