<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

interface HasOneInterface
{

    /**
     *
     * @param array $data
     * @param bool $setDefaults
     * @return OrmModel\Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true): OrmModel\Entity\EntityInterface;

    /**
     *
     * @param array $args
     * @return bool
     */
    public function delete(array &$args = []): bool;

    /**
     *
     * @param array $args
     * @return bool
     */
    public function detach(array &$args = []): bool;

    public function replace(OrmModel\Entity\EntityInterface $entity): bool;

    public function isset(): bool;
}
