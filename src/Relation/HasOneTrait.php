<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

trait HasOneTrait
{

    /**
     *
     * @param array $data
     * @param bool $setDefaults
     * @return OrmModel\Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true): OrmModel\Entity\EntityInterface
    {
        $this->delete();

        $return = parent::create($data, $setDefaults);

        if (!empty($this->parent) && !empty($this->foreignKeyObjectField)) {
            $return->{$this->foreignKeyObjectField} = $this->parent;
        }

        return $return;
    }

    /**
     * @param array $args
     * @return bool
     */
    public function delete(array &$args = []): bool
    {
        if ($this->entity !== null) {
            $this->entity->delete($args);
        } elseif ($this instanceof OrmModel\Relation\Proxy\HasOne) {
            $this->service->deleteWhere($this->where, $this->bind);
        }

        if (!isset($args['detach']) || $args['detach'] === true) {
            $this->unload($args);
        }

        return true;
    }

    /**
     * @param array $args
     * @return bool
     */
    public function detach(array &$args = []): bool
    {
        if ($this->entity !== null) {
            $this->entity->{$this->foreignKeyObjectField} = null;

            if ($this->entity->id !== null) {
                $this->entity->save($args);
            }
        } elseif ($this instanceof OrmModel\Relation\Proxy\HasOne) {
            $this->service->set($this->where, $this->bind, [$this->foreignKeyObjectField => null]);
        }

        $this->unload($args);

        return true;
    }

    public function replace(OrmModel\Entity\EntityInterface $entity): bool
    {
        if ($this->entity !== null && $this->entity->id === $entity->id) {
            return true;
        }

        $this->entity = $entity;
        $entity->{$this->foreignKeyObjectField} = $this->parent;

        if ($this->parent->id !== null) {
            $entity->save();
        } else {
            $this->parent->addEntityToSaveAfterSave($entity);
        }

        return true;
    }
}
