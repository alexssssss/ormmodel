<?php

namespace Alexssssss\OrmModel\Relation;

use Alexssssss\OrmModel;

trait HelperTrait
{

    /**
     * @codeCoverageIgnore
     * @param OrmModel\Service\ServiceInterface $parentService
     * @param OrmModel\Entity\EntityInterface $parent
     * @param int|null $id
     * @return OrmModel\WrapperInterface
     */
    protected function createParentProxy(OrmModel\Service\ServiceInterface &$parentService, OrmModel\Entity\EntityInterface &$parent, int $id = null)
    {
        return OrmModel\Helper\Wrapper::createWrappedProxy($parentService, $id, $parent);
    }
}
