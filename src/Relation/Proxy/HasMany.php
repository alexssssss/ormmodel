<?php

namespace Alexssssss\OrmModel\Relation\Proxy;

use Alexssssss\OrmModel;
use Alexssssss\OrmModel\Relation;

class HasMany extends OrmModel\ObjectStorageProxy implements Relation\HasManyInterface
{

    use Relation\HasManyTrait,
        Relation\HelperTrait;

    /**
     *
     * @var OrmModel\Entity\EntityInterface
     */
    protected $parent;

    /**
     *
     * @var string
     */
    protected $foreignKeyObjectField;
    /**
     *
     * @var string
     */
    protected $foreignKeyField;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param string $foreignKeyField
     * @param string $where
     * @param array $bind
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     *
     */
    public function __construct(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, string $foreignKeyField, string $where, array $bind, OrmModel\Service\ServiceInterface &$service)
    {
        if (!is_a((is_a($parent, 'Alexssssss\\OrmModel\\WrapperInterface') ? $parent->getRealObject(false) : $parent), 'Alexssssss\\OrmModel\\AbstractProxy')) {
            $parentService = $parent->getService();
            $this->parent = $this->createParentProxy($parentService, $parent, $parent->id);
        } else {
            $this->parent = $parent;
        }

        $this->foreignKeyObjectField = $foreignKeyObjectField;
        $this->foreignKeyField = $foreignKeyField;

        parent::__construct($where, $bind, $service);
    }

    public function __sleep()
    {
        $parent = parent::__sleep();
        return array_merge($parent, ['parent', 'foreignKeyObjectField', 'foreignKeyField']);
    }
}
