<?php

namespace Alexssssss\OrmModel\Relation\Proxy;

use Alexssssss\OrmModel;
use Alexssssss\OrmModel\Relation;

class HasManyToMany extends OrmModel\ObjectStorageProxy implements Relation\HasManyToManyInterface
{

    use Relation\HasManyToManyTrait,
        Relation\HelperTrait;

    /**
     *
     * @var OrmModel\Entity\EntityInterface
     */
    protected $parent;

    /**
     *
     * @var string
     */
    protected $linkStoreInField;

    /**
     * @var string
     */
    protected $linkSecForeignKeyObjectField;

    /**
     * @var string
     */
    protected $linkSecForeignKeyField;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $linkStoreInField
     * @param string $linkSecForeignKeyObjectField
     * @param string $linkSecForeignKeyField
     * @param string $where
     * @param array $bind
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     */
    public function __construct(OrmModel\Entity\EntityInterface &$parent, string $linkStoreInField, string $linkSecForeignKeyObjectField, string $linkSecForeignKeyField, string $where, array $bind, OrmModel\Service\ServiceInterface &$service)
    {
        if (!is_a((is_a($parent, 'Alexssssss\\OrmModel\\WrapperInterface') ? $parent->getRealObject(false) : $parent), 'Alexssssss\\OrmModel\\AbstractProxy')) {
            $parentService = $parent->getService();
            $this->parent = $this->createParentProxy($parentService, $parent, $parent->id);
        } else {
            $this->parent = $parent;
        }

        $this->linkStoreInField = $linkStoreInField;
        $this->linkSecForeignKeyObjectField = $linkSecForeignKeyObjectField;
        $this->linkSecForeignKeyField = $linkSecForeignKeyField;

        parent::__construct($where, $bind, $service);
    }

    public function __sleep()
    {
        $parent = parent::__sleep();
        return array_merge($parent, ['parent', 'linkStoreInField', 'linkSecForeignKeyObjectField', 'linkSecForeignKeyField']);
    }
}
