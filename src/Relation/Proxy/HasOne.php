<?php

namespace Alexssssss\OrmModel\Relation\Proxy;

use Alexssssss\OrmModel;
use Alexssssss\OrmModel\Relation;

class HasOne extends OrmModel\Entity\Proxy\AbstractEntity implements Relation\HasOneInterface
{

    use Relation\HasOneTrait,
        Relation\HelperTrait;

    /**
     *
     * @var string
     */
    protected $where;

    /**
     *
     * @var array
     */
    protected $bind = [];

    /**
     *
     * @var OrmModel\Entity\EntityInterface
     */
    protected $parent;

    /**
     *
     * @var string
     */
    protected $foreignKeyObjectField;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $parent
     * @param string $foreignKeyObjectField
     * @param string $where
     * @param array $bind
     * @param \Alexssssss\OrmModel\Service\ServiceInterface $service
     */
    public function __construct(OrmModel\Entity\EntityInterface &$parent, string $foreignKeyObjectField, string $where, array $bind, OrmModel\Service\ServiceInterface &$service)
    {
        if (!is_a((is_a($parent, 'Alexssssss\\OrmModel\\WrapperInterface') ? $parent->getRealObject(false) : $parent), 'Alexssssss\\OrmModel\\AbstractProxy')) {
            $parentService = $parent->getService();
            $this->parent = $this->createParentProxy($parentService, $parent, $parent->id);
        } else {
            $this->parent = $parent;
        }

        $this->where = $where;
        $this->bind = $bind;
        $this->service = $service;
        $this->foreignKeyObjectField = $foreignKeyObjectField;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @return mixed
     * @throws OrmModel\Exception\UndefinedField
     */
    public function __set(string $field, $value)
    {
        $this->load();

        if ($this->isLoaded()) {
            return parent::__set($field, $value);
        }
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface|null $entity
     */
    protected function setEntity(OrmModel\Entity\EntityInterface $entity = null)
    {
        $this->entity = $entity ?? $this->service->getOne($this->where, $this->bind);
    }

    public function __sleep()
    {
        $parent = parent::__sleep();
        return array_merge($parent, ['where', 'bind', 'parent', 'foreignKeyObjectField']);
    }
}
