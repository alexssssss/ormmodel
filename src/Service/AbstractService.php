<?php

namespace Alexssssss\OrmModel\Service;

use DateTime;
use Alexssssss\OrmModel;

/**
 * Class AbstractService
 *
 * @package Alexssssss\OrmModel\Service
 */
abstract class AbstractService implements ServiceInterface
{

    protected static $instances = [];

    /**
     *
     * @var OrmModel\DataMapper\DataMapperInterface
     */
    protected $dataMapper;

    /**
     *
     * @var OrmModel\DataAccess\DataAccessInterface
     */
    protected $dataAccess;
    protected $allowInsert = true;
    protected $allowUpdate = true;
    protected $allowDelete = true;


    protected $customSave;
    protected $customSaveAll;
    protected $customUpdate;
    protected $customInsert;
    protected $customDelete;

    /**
     * Array of field names mass fillable (with wildcard support)
     *
     * @var array
     */
    protected $massFillable = ['*'];

    /**
     * Array of field names mass fillable by setter (with wildcard support)
     *
     * @var array
     */
    protected $massFillableBySetter = [];

    /**
     * Array of field names not mass fillable (with wildcard support)
     *
     * @var array
     */
    protected $massGuarded = [];


    /**
     *
     * @var \Symfony\Contracts\Translation\TranslatorInterface
     */
    protected $translator;

    /**
     * Cache of all columns of this table with defaults
     *
     * @var array
     */
    protected $tableCols = [];

    /**
     *
     * @var OrmModel\Validator
     */
    protected $validator;

    /**
     * Voor het instellen van "hasOne" relaties
     *
     * Voorbeeld: A user has one profile.
     *
     * @var array
     */
    protected $hasOne = [];

    /**
     * Voor het instellen van "hasManyToMany" relaties
     *
     * @var array
     */
    protected $hasManyToMany = [];

    /**
     * Voor het instellen van "hasMany" relaties
     *
     * Voorbeeld: A user can have multiple recipes.
     *
     * @var array
     */
    protected $hasMany = [];

    /**
     * Voor het instellen van "belongsTo" relaties
     *
     * Voorbeeld: Many recipes belong to a user.
     *
     * @var array
     */
    protected $belongsTo = [];

    protected $name;

    /**
     *
     * @param OrmModel\DataAccess\DataAccessInterface $dataAccess
     * @param OrmModel\DataMapper\DataMapperInterface $dataMapper
     * @param \Symfony\Contracts\Translation\TranslatorInterface $translator
     */
    public function __construct(OrmModel\DataAccess\DataAccessInterface $dataAccess, OrmModel\DataMapper\DataMapperInterface $dataMapper, \Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
        $this->dataAccess = $dataAccess;
        $this->dataMapper = $dataMapper;
        $this->translator = $translator;

        $this->name = get_called_class();

        $this->dataMapper->setDataAccess($this->dataAccess);
        $this->dataMapper->setService($this);

        // define not mass changeable fields
        $this->massGuarded = array_merge($this->massGuarded, ['service', 'previous', 'uuid', 'wrapped', 'entitiesToDeleteBeforeSave', 'entitiesToSaveAfterSave']);

        // check if class has custom save, savaAll, update, insert or delete methods
        //$reflect = new \ReflectionClass($this);
        //foreach ($reflect->getMethods() as $value) {
        //    if ($value->getFileName() == $reflect->getFileName()) {
        //        if ($value->name == 'save') {
        //            $this->customSave = $this->customSave ?? true;
        //        } elseif ($value->name == 'saveAll') {
        //            $this->customSaveAll = $this->customSaveAll ?? true;
        //        } elseif ($value->name == 'update') {
        //            $this->customUpdate = $this->customUpdate ?? true;
        //        } elseif ($value->name == 'insert') {
        //            $this->customInsert = $this->customInsert ?? true;
        //        } elseif ($value->name == 'delete') {
        //            $this->customDelete = $this->customDelete ?? true;
        //        }
        //    }
        //}

        // make class static callable
        self::$instances[$this->name] = $this;
    }

    /**
     *
     * @param array $rules
     *
     * @return OrmModel\Entity\EntityInterface|null
     */
    public function getOne(...$rules): ?OrmModel\Entity\EntityInterface
    {
        if (count($rules) == 1 && isset($rules[0]) && (is_int($rules[0]) || (is_string($rules[0]) && ctype_digit($rules[0])))) {
            if ($this->dataMapper->isCached($rules[0])) {
                $return = $this->dataMapper->getFromCache($rules[0], false);
            } else {
                $row = $this->getArrayById($rules[0]);
                $return = empty($row) ? null : $this->dataMapper->rowToEntity($row);
            }
        } elseif ((count($rules) == 1 && isset($rules[0]) && !is_array($rules[0]) && !(is_int($rules[0]) || (is_string($rules[0]) && ctype_digit($rules[0])))) ||
            (isset($rules[0]['id']) && !(is_int($rules[0]['id']) || (is_string($rules[0]['id']) && ctype_digit($rules[0]['id']))))
        ) {
            $return = null;
        } else {
            if (count($rules) != 0 && !is_array($rules[0])
            ) {
                array_unshift($rules, []);
            }

            $row = $this->dataAccess->getBy($rules[0] ?? [], $rules[1] ?? null, $rules[2] ?? [], true);
            $return = empty($row) ? null : $this->dataMapper->rowToEntity($row);
        }

        return $return;
    }

    /**
     *
     * @param integer $id
     *
     * @return array
     */
    public function getArrayById(int $id)
    {
        $row = $this->dataAccess->getById($id);
        return empty($row) ? null : $row;
    }

    /**
     *
     * @param array $rules
     *
     * @return OrmModel\ObjectStorageInterface|OrmModel\Entity\EntityInterface[]
     */
    public function get(...$rules): OrmModel\ObjectStorageInterface
    {
        if (count($rules) == 1 &&
            is_array($rules[0]) &&
            isset($rules[0][0]) &&
            ctype_digit(implode('', array_keys($rules[0]))) &&
            (is_int($rules[0][0]) || (is_string($rules[0][0]) && ctype_digit($rules[0][0]))) &&
            ctype_digit(implode('', $rules[0]))
        ) {
            $rules[0] = ['id' => $rules[0]];
        } elseif (count($rules) != 0 && !is_array($rules[0])) {
            array_unshift($rules, []);
        }

        $rows = $this->dataAccess->getBy($rules[0] ?? [], $rules[1] ?? null, $rules[2] ?? []);

        return $this->dataMapper->rowsToEntities($rows);
    }

    /**
     *
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function getArray(string $where = null, array $bind = []): array
    {
        $return = $this->dataAccess->get($where, $bind);

        if (is_object($return)) {
            $return = iterator_to_array($return);
        }

        return $return;
    }

    /**
     * @param ...$rules
     *
     * @return int
     */
    public function count(...$rules): int
    {
        if (count($rules) == 1 &&
            is_array($rules[0]) &&
            isset($rules[0][0]) &&
            ctype_digit(implode('', array_keys($rules[0]))) &&
            (is_int($rules[0][0]) || (is_string($rules[0][0]) && ctype_digit($rules[0][0]))) &&
            ctype_digit(implode('', $rules[0]))
        ) {
            $rules[0] = ['id' => $rules[0]];
        } elseif (count($rules) != 0 && !is_array($rules[0])) {
            array_unshift($rules, []);
        }

        return $this->dataAccess->countBy($rules[0] ?? [], $rules[1] ?? null, $rules[2] ?? []);
    }


    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     * @throws \Exception
     */
    public function save(OrmModel\Entity\EntityInterface &$entity, bool $validate = true, array &$args = []): bool
    {
        if (!isset($args['alreadySavedOrDeleted']) || !is_array($args['alreadySavedOrDeleted'])) {
            $args['alreadySavedOrDeleted'] = [];
        }

        if ($this->checkEntityType($entity instanceof OrmModel\FakeObjectInterface ? $entity->getRealObject() : $entity) === true) {
            if ($entitiesToDeleteBeforeSave = $entity->getEntitiesToDeleteBeforeSave()) {
                foreach ($entitiesToDeleteBeforeSave as $entityToDeleteBeforeSave) {
                    $entityToDeleteBeforeSave->delete($args);
                }
            }

            if (($entity instanceof OrmModel\AbstractProxy && $entity->isLoaded() === false) || in_array($entity->getUuid(), $args['alreadySavedOrDeleted'])) {
                $return = true;
            } else {
                if ($entity->hasUnsavedChanges() === false) {
                    $return = true;
                } elseif ($validate == false || $entity->validate()) {
                    try {
                        if ($entity->id !== null) {
                            $return = $this->update($entity, $args);
                        } else {
                            $return = $this->insert($entity, $args);
                        }
                    } catch (\Exception $exception) {
                        if (strpos($exception->getMessage(), 'Duplicate entry') !== false) {
                            throw new OrmModel\Exception\Duplicate($exception->getMessage(), $this->translator);
                        } else {
                            throw $exception;
                        }
                    }

                    if (!isset($args['setPrevious']) || $args['setPrevious'] === true) {
                        $entity->setPrevious();
                    } else {
                        $this->setLastStoredVersion();
                    }
                }

                $args['alreadySavedOrDeleted'][] = $entity->getUuid();

                if ($entitiesToSaveAfterSave = $entity->getEntitiesToSaveAfterSave()) {
                    foreach ($entitiesToSaveAfterSave as $entityToSaveAfterSave) {
                        $entityToSaveAfterSave->save($validate, $args);
                    }
                }
            }
        } else {
            throw new \Exception("Service '" . $this->name . "' function 'save' entity type check failed");
        }

        return $return;
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     */
    public function saveAll(OrmModel\Entity\EntityInterface &$entity, bool $validate = true, array &$args = []): bool
    {
        if (!isset($args['alreadySavedOrDeleted']) || !is_array($args['alreadySavedOrDeleted'])) {
            $args['alreadySavedOrDeleted'] = [];
        }

        if (!in_array($entity->getUuid(), $args['alreadySavedOrDeleted'])) {
            if ($validate === true) {
                $entity->validateAll();
                $validate = false;
            }

            $belongsToRelations = $entity->getService()->getBelongsTo();

            foreach ($belongsToRelations as $rel) {
                if (isset($entity->{$rel['storeInField']}) &&
                    $entity->{$rel['storeInField']} instanceof OrmModel\Entity\EntityInterface &&
                    $entity->{$rel['storeInField']}->getId() === null
                ) {
                    $entity->{$rel['storeInField']}->save($validate, $args);
                    $args['alreadySavedOrDeleted'][] = $entity->{$rel['storeInField']}->getUuid();
                }
            }

            $entity->save($validate, $args);

            foreach ($entity->toArray() as $key => $object) {
                if ($object instanceof OrmModel\WrapperInterface) {
                    $object = $object->getRealObject(false);
                }

                if (!is_object($object) || $object instanceof DateTime || ($object instanceof OrmModel\ObjectStorageProxy && $object->isLoaded() === false)) {
                    continue;
                } elseif ($object instanceof OrmModel\Entity\EntityInterface) {
                    if ($object->isset() && !in_array($object->getUuid(), $args['alreadySavedOrDeleted'])) {
                        $object->saveAll($validate, $args);
                    }
                } elseif ($object instanceof OrmModel\ObjectStorageInterface) {
                    $object->saveAll($validate, true, $args);
                }
            }
        }

        return true;
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @param array $args
     *
     * @return bool
     * @throws OrmModel\Exception\DeleteForeignKeyConstraint
     * @throws \Exception
     */
    public function delete(OrmModel\Entity\EntityInterface &$entity, array &$args = []): bool
    {
        if ($this->allowDelete !== true) {
            throw new \Exception("Service '" . $this->name . "' function 'delete' can't be used model is read only");
        }

        if (!isset($args['alreadySavedOrDeleted']) || !is_array($args['alreadySavedOrDeleted'])) {
            $args['alreadySavedOrDeleted'] = [];
        }

        if (in_array($entity->getUuid(), $args['alreadySavedOrDeleted'])) {
            $return = true;
        } else {
            try {
                $return = $this->dataMapper->delete($entity);
            } catch (\Exception $exception) {
                if (strpos($exception->getMessage(), 'Cannot delete or update a parent row: a foreign key constraint fails') !== false) {
                    throw new OrmModel\Exception\DeleteForeignKeyConstraint($exception->getMessage(), $this->translator);
                } else {
                    throw $exception;
                }
            }

            $args['alreadySavedOrDeleted'][] = $entity->getUuid();

            $entity->clearId(true);
            $entity->setPrevious();
        }

        return $return;
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @param array $args
     *
     * @return bool
     * @throws \Exception
     */
    protected function insert(OrmModel\Entity\EntityInterface &$entity, array &$args = []): bool
    {
        if ($this->allowInsert !== true) {
            throw new \Exception("Service '" . $this->name . "' function 'insert' can't be used model is read only");
        }

        return $this->dataMapper->insert($entity);
    }

    /**
     *
     * @param OrmModel\Entity\EntityInterface $entity
     * @param array $args
     *
     * @return bool
     * @throws \Exception
     */
    protected function update(OrmModel\Entity\EntityInterface &$entity, array &$args = []): bool
    {
        if (property_exists($entity, 'updated_at')) {
            $entity->updated_at = new DateTime();
        }

        if (property_exists($entity, 'updatedAt')) {
            $entity->updatedAt = new DateTime();
        }

        if ($this->allowUpdate !== true) {
            throw new \Exception("Service '" . $this->name . "' function 'update' can't be used model is read only");
        }

        return $this->dataMapper->update($entity);
    }

    /**
     * check to see if a entity is a belongs to this service
     */
    public function checkEntityType($entity): bool
    {
        return $this->dataMapper->checkEntityType($entity);
    }

    /**
     *
     * @param array $data
     * @param bool $setDefaults
     *
     * @return OrmModel\Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true): OrmModel\Entity\EntityInterface
    {
        if ($setDefaults === true) {
            if (empty($this->tableCols)) {
                $this->tableCols = $this->dataAccess->getTableCols();
            }

            foreach ($this->tableCols as $col) {
                if ($col['Default'] !== null && !isset($data[$col['Field']])) {
                    if ($col['Default'] === 'current_timestamp(6)') {
                        $now = DateTime::createFromFormat('U.u', microtime(true));
                        $data[$col['Field']] = $now->format("Y-m-d H:i:s.u");
                    } elseif ($col['Default'] === 'CURRENT_TIMESTAMP' || $col['Default'] === 'current_timestamp()') {
                        $data[$col['Field']] = date('Y-m-d H:i:s');
                    } else {
                        $data[$col['Field']] = $col['Default'];
                    }
                }
            }
        }

        return $this->dataMapper->rowToEntity($data, false);
    }

    /**
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function getIds(string $where = null, array $bind = []): array
    {
        return $this->dataAccess->getIds($where, $bind);
    }

    /**
     *
     * @param string $fieldName
     *
     * @return string
     */
    public function fieldToHumanReadable(string $fieldName): string
    {
        $class = explode('\\', $this->name);
        $field = str_replace('\\', '.', end($class) . "." . ucfirst($fieldName));

        return $this->translator->trans($field, [], 'entities');
    }

    /**
     *
     * @param string|array $relations
     *
     * @return OrmModel\EagerLoader|$this
     */
    public function with($relations): OrmModel\EagerLoader
    {
        if (is_string($relations)) {
            $relations = func_get_args();
        }

        return $this->createEagerLoaderObject($relations);
    }

    /**
     * @codeCoverageIgnore
     *
     * @param array $relations
     *
     * @return OrmModel\EagerLoader
     * @throws \Exception
     */
    protected function createEagerLoaderObject($relations): OrmModel\EagerLoader
    {
        return new OrmModel\EagerLoader($this, $relations);
    }

    /**
     *
     * @return string
     */
    public function getDataFieldPrefix(): string
    {
        return $this->dataAccess->getFieldPrefix();
    }

    /**
     *
     * @return string
     */
    public function getDataTable(): string
    {
        return $this->dataAccess->getTable();
    }

    /**
     *
     * @return OrmModel\DataAccess\DataAccessInterface
     */
    public function getDataAccess(): OrmModel\DataAccess\DataAccessInterface
    {
        return $this->dataAccess;
    }

    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function isSimpleDbField(string $fieldName): bool
    {
        if (!in_array($fieldName, ['id'])) {
            $entityClassName = $this->dataMapper->getEntityClassName();

            $functions = get_class_methods($entityClassName);

            $accessor = "get" . ucfirst($fieldName);

            $notMappedFields = $entityClassName::getNotMapped();

            if (in_array($accessor, $functions) || in_array($fieldName, $notMappedFields)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function min($field, string $where = null, array $bind = [])
    {
        return $this->dataMapper->min($field, $where, $bind);
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function max($field, string $where = null, array $bind = [])
    {
        return $this->dataMapper->max($field, $where, $bind);
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function avg($field, string $where = null, array $bind = [])
    {
        return $this->dataMapper->avg($field, $where, $bind);
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function sum($field, string $where = null, array $bind = [])
    {
        return $this->dataMapper->sum($field, $where, $bind);
    }

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function distinct($field, string $where = null, array $bind = []): array
    {
        return $this->dataAccess->distinct($field, $where, $bind);
    }

    /**
     * @return string
     */
    public function getEntityClassName(): string
    {
        return $this->dataMapper->getEntityClassName();
    }

    /**
     * @return string
     */
    public function getWrapperClassName(): string
    {
        return $this->dataMapper->getWrapperClassName();
    }

    /**
     * @codeCoverageIgnore
     *
     * @param string $where
     * @param array $bind
     *
     * @return OrmModel\ObjectStorageProxy
     */
    public function getProxy(string $where = '', array $bind = []): OrmModel\ObjectStorageProxy
    {
        return new OrmModel\ObjectStorageProxy($where, $bind, $this);
    }

    /**
     * @param string $where
     * @param array $bind
     * @param array $values
     *
     * @return bool
     * @throws \Exception
     */
    public function set(string $where = '', array $bind = [], array $values = [], bool $validate = true, array &$args = []): bool
    {
        $fields = array_keys($values);

        $allMassFillable = true;

        foreach ($fields as $field) {
            foreach ($this->massGuarded as $patternOrField) {
                if (fnmatch($patternOrField, $field) === true) {
                    throw new \Exception("The field '" . $field . "' is guarded and can't be mass set by '" . $this->name . "'");
                }
            }

            if ($allMassFillable === true) {
                foreach ($this->massFillableBySetter as $patternOrField) {
                    if (fnmatch($patternOrField, $field) === true) {
                        $allMassFillable = false;
                        continue 2;
                    }
                }

                if (count($this->massFillable) > 1 || $this->massFillable[0] !== '*') {
                    foreach ($this->massFillable as $patternOrField) {
                        if (fnmatch($patternOrField, $field) === false) {
                            $allMassFillable = false;
                            continue 2;
                        }
                    }
                }
            }
        }

        if ($allMassFillable === true && $this->customSave !== true && $this->customUpdate !== true) {
            // todo: validation??

            $arrayOfLines = $this->dataAccess->getIds($where, $bind);

            if (count($arrayOfLines) === 0) {
                $return = true;
            } else {
                $ids = [];
                foreach ($arrayOfLines as $line) {
                    $ids[] = (int)$line['id'];
                }

                $return = $this->dataMapper->updateById($ids, $values);
            }
        } else {
            $return = $this->get($where, $bind)->set($values, $validate, $args);
        }

        return $return;
    }

    /**
     * @param integer|integer[] $ids
     * @param array $values
     *
     * @return bool
     */
    public function setById($ids, array $values = []): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        return $this->dataMapper->updateById($ids, $values);
    }

    /**
     * @param integer|integer[] $ids
     *
     * @return bool
     */
    public function deleteById($ids): bool
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        return $this->dataMapper->deleteById($ids);
    }

    /**
     * @param string $where
     * @param array $bind
     *
     * @return bool
     */
    public function deleteWhere(string $where = '', array $bind = []): bool
    {
        $arrayOfLines = $this->dataAccess->getIds($where, $bind);

        if (count($arrayOfLines) === 0) {
            $return = true;
        } else {
            $ids = [];
            foreach ($arrayOfLines as $line) {
                $ids[] = (int)$line['id'];
            }

            $return = $this->dataMapper->deleteById($ids);
        }

        return $return;
    }

    /**
     * @return $this
     */
    public static function getInstance()
    {
        $name = get_called_class();

        if (!isset(self::$instances[$name])) {
            ServiceFactory::create($name);
        }

        return self::$instances[$name];
    }

    public function getValidator()
    {
        if ($this->validator == null) {
            $this->validator = OrmModel\Validator::getInstance($this->translator);
        }

        return $this->validator;
    }

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return bool
     * @throws OrmModel\Exception\InvalidFields
     */
    public function validate(OrmModel\Entity\EntityInterface &$entity): bool
    {
        if ($entity->hasUnsavedChanges()) {
            $this->getValidator()->validate($entity);
        }

        return true; // no exception means ok
    }

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return bool
     */
    public function validateAll(OrmModel\Entity\EntityInterface &$entity): bool
    {
        $this->getValidator()->validateAll($entity);

        return true; // no exception means ok
    }

    /**
     *
     * @param string $relatedService
     * @param string $foreignKeyField
     * @param string $storeInField
     */
    protected function belongsTo(string $relatedService, string $foreignKeyField = null, string $storeInField = null)
    {
        if ($foreignKeyField === null) {
            if (strpos($relatedService, '\\Service\\') !== false) {
                $parts = explode('\\Service\\', $relatedService);
                $foreignKeyField = lcfirst(str_replace('\\', '', end($parts)) . 'Id');
            } else {
                $parts = explode('\\', $relatedService);
                $foreignKeyField = lcfirst(end($parts) . 'Id');
            }
        }

        if ($storeInField === null) {
            $parts = explode('\\', $relatedService);
            $storeInField = lcfirst(end($parts));
        }

        $this->belongsTo[$storeInField] = [
            'service' => $relatedService,
            'foreignKeyField' => $foreignKeyField,
            'storeInField' => $storeInField
        ];
    }

    /**
     * @param string $linkRelatedService
     * @param string $secRelatedService
     * @param string|null $primForeignKeyField
     * @param string|null $linkForeignKeyField
     * @param string|null $linkSecForeignKeyField
     * @param string|null $linkForeignKeyObjectField
     * @param string|null $linkSecForeignKeyObjectField
     * @param string|null $secForeignKeyField
     * @param string|null $primStoreInField
     * @param string|null $linkStoreInField
     * @param string|null $secConditions
     * @param string|null $linkConditions
     */
    protected function hasManyToMany(string $linkRelatedService, string $secRelatedService, string $primForeignKeyField = null, string $linkForeignKeyField = null, string $linkSecForeignKeyField = null, string $linkForeignKeyObjectField = null, string $linkSecForeignKeyObjectField = null, string $secForeignKeyField = null, string $primStoreInField = null, string $linkStoreInField = null, string $secConditions = null, string $linkConditions = null)
    {
        if ($primForeignKeyField === null || $linkForeignKeyField === null) {
            if (strpos($this->name, '\\Service\\') !== false) {
                $parts = explode('\\Service\\', $this->name);
                $primForeignKeyField = $primForeignKeyField ?? (lcfirst(str_replace('\\', '', end($parts)) . 'Id'));
                $linkForeignKeyField = $linkForeignKeyField ?? (lcfirst(str_replace('\\', '', end($parts)) . 'Id'));
            } else {
                $parts = explode('\\', $this->name);
                $primForeignKeyField = $primForeignKeyField ?? (lcfirst(end($parts) . 'Id'));
                $linkForeignKeyField = $linkForeignKeyField ?? (lcfirst(end($parts) . 'Id'));
            }
        }

        if ($secForeignKeyField === null || $linkSecForeignKeyField === null) {
            if (strpos($secRelatedService, '\\Service\\') !== false) {
                $parts = explode('\\Service\\', $secRelatedService);
                $secForeignKeyField = $secForeignKeyField ?? lcfirst(str_replace('\\', '', end($parts)) . 'Id');
                $linkSecForeignKeyField = $linkSecForeignKeyField ?? lcfirst(str_replace('\\', '', end($parts)) . 'Id');
            } else {
                $parts = explode('\\', $secRelatedService);
                $secForeignKeyField = $secForeignKeyField ?? lcfirst(end($parts) . 'Id');
                $linkSecForeignKeyField = $linkSecForeignKeyField ?? lcfirst(end($parts) . 'Id');
            }
        }

        if ($linkForeignKeyObjectField === null) {
            $parts = explode('\\', $this->name);
            $linkForeignKeyObjectField = lcfirst(end($parts));
        }

        if ($linkSecForeignKeyObjectField === null) {
            $parts = explode('\\', $secRelatedService);
            $linkSecForeignKeyObjectField = lcfirst(end($parts));
        }

        if ($primStoreInField === null) {
            $parts = explode('\\', $secRelatedService);
            $primStoreInField = lcfirst(end($parts));
        }

        if ($linkStoreInField === null) {
            $parts = explode('\\', $linkRelatedService);
            $linkStoreInField = lcfirst(end($parts));
        }

        $this->hasManyToMany[$primStoreInField] = [
            'secService' => $secRelatedService,
            'linkRelatedService' => $linkRelatedService,
            'primForeignKeyField' => $primForeignKeyField,
            'linkForeignKeyField' => $linkForeignKeyField,
            'linkSecForeignKeyField' => $linkSecForeignKeyField,
            'secForeignKeyField' => $secForeignKeyField,
            'linkForeignKeyObjectField' => $linkForeignKeyObjectField,
            'linkSecForeignKeyObjectField' => $linkSecForeignKeyObjectField,
            'primStoreInField' => $primStoreInField,
            'linkStoreInField' => $linkStoreInField,
            'secConditions' => $secConditions,
            'linkConditions' => $linkConditions
        ];

        $this->hasMany($linkRelatedService, $linkForeignKeyField, $linkForeignKeyObjectField, $linkStoreInField, $linkConditions);
    }

    /**
     *
     * @param string $relatedService
     * @param string $foreignKeyField
     * @param string $foreignKeyObjectField
     * @param string $storeInField
     * @param string $conditions
     */
    protected function hasMany(string $relatedService, string $foreignKeyField = null, string $foreignKeyObjectField = null, string $storeInField = null, string $conditions = null)
    {
        if ($foreignKeyField === null) {
            if (strpos($this->name, '\\Service\\') !== false) {
                $parts = explode('\\Service\\', $this->name);
                $foreignKeyField = lcfirst(str_replace('\\', '', end($parts)) . 'Id');
            } else {
                $parts = explode('\\', $this->name);
                $foreignKeyField = lcfirst(end($parts) . 'Id');
            }
        }

        if ($foreignKeyObjectField === null) {
            $parts = explode('\\', $this->name);
            $foreignKeyObjectField = lcfirst(end($parts));
        }

        if ($storeInField === null) {
            $parts = explode('\\', $relatedService);
            $storeInField = lcfirst(end($parts));
        }

        $this->hasMany[$storeInField] = [
            'service' => $relatedService,
            'foreignKeyField' => $foreignKeyField,
            'foreignKeyObjectField' => $foreignKeyObjectField,
            'storeInField' => $storeInField,
            'conditions' => $conditions
        ];
    }

    /**
     *
     * @param string $relatedService
     * @param string $foreignKeyField
     * @param string $foreignKeyObjectField
     * @param string $storeInField
     * @param string $conditions
     */
    protected function hasOne(string $relatedService, string $foreignKeyField = null, string $foreignKeyObjectField = null, string $storeInField = null, string $conditions = null)
    {
        if ($foreignKeyField === null) {
            if (strpos($this->name, '\\Service\\') !== false) {
                $parts = explode('\\Service\\', $this->name);
                $foreignKeyField = lcfirst(str_replace('\\', '', end($parts)) . 'Id');
            } else {
                $parts = explode('\\', $this->name);
                $foreignKeyField = lcfirst(end($parts) . 'Id');
            }
        }

        if ($foreignKeyObjectField === null) {
            $parts = explode('\\', $this->name);
            $foreignKeyObjectField = lcfirst(end($parts));
        }

        if ($storeInField === null) {
            $parts = explode('\\', $relatedService);
            $storeInField = lcfirst(end($parts));
        }

        $this->hasOne[$storeInField] = [
            'service' => $relatedService,
            'foreignKeyField' => $foreignKeyField,
            'foreignKeyObjectField' => $foreignKeyObjectField,
            'storeInField' => $storeInField,
            'conditions' => $conditions
        ];
    }

    /**
     * @param string $field
     *
     * @return array
     */
    public function getBelongsToRelByField(string $field): array
    {
        if (!empty($this->belongsTo)) {
            foreach ((array)$this->belongsTo as $rel) {
                if ($rel['foreignKeyField'] == $field) {
                    return $rel;
                }
            }
        }

        return [];
    }

    /**
     * @param string $field
     *
     * @return array
     */
    public function getHasManyRelByField(string $field): array
    {
        if (!empty($this->hasMany)) {
            foreach ((array)$this->hasMany as $rel) {
                if ($rel['storeInField'] == $field) {
                    return $rel;
                }
            }
        }

        return [];
    }

    /**
     * @param string $field
     *
     * @return array
     */
    public function getHasManyToManyRelByField(string $field): array
    {
        if (!empty($this->hasManyToMany)) {
            foreach ((array)$this->hasManyToMany as $rel) {
                if ($rel['primStoreInField'] == $field) {
                    return $rel;
                }
            }
        }

        return [];
    }

    /**
     * @param string $field
     *
     * @return array
     */
    public function getHasOneRelByField(string $field): array
    {
        if (!empty($this->hasOne)) {
            foreach ((array)$this->hasOne as $rel) {
                if ($rel['storeInField'] == $field) {
                    return $rel;
                }
            }
        }

        return [];
    }

    /**
     * @param string $field
     *
     * @return bool
     */
    public function isBelongsToField(string $field): bool
    {
        return !empty($this->belongsTo) && isset($this->belongsTo[$field]);
    }

    /**
     *
     * @return array
     */
    public function getBelongsTo(): array
    {
        return (array)$this->belongsTo;
    }

    /**
     *
     * @return array
     */
    public function getHasMany(): array
    {
        return (array)$this->hasMany;
    }

    /**
     *
     * @return array
     */
    public function getHasManyToMany(): array
    {
        return (array)$this->hasManyToMany;
    }

    /**
     *
     * @return array
     */
    public function getHasOne(): array
    {
        return (array)$this->hasOne;
    }

    public function refresh(OrmModel\Entity\EntityInterface &$entity): OrmModel\Entity\EntityInterface
    {
        if ($entity->id !== null) {
            $this->dataMapper->deleteFromCache($entity->id);
            $entity = $this->getOne(['id' => $entity->id]);
        } else {
            $entity = $this->create();
        }

        return $entity;
    }
}
