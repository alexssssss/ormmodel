<?php

namespace Alexssssss\OrmModel\Service;

use Alexssssss\OrmModel;

/**
 * Interface ServiceInterface
 * @package Alexssssss\OrmModel\Service
 */
interface ServiceInterface
{
    /**
     * @param integer $id
     */
    public function getArrayById(int $id);

    /**
     * @param array $rules
     *
     * @return OrmModel\ObjectStorageInterface
     */
    public function get(...$rules): OrmModel\ObjectStorageInterface;

    /**
     * @param array $rules
     *
     * @return OrmModel\Entity\EntityInterface|null
     */
    public function getOne(...$rules): ? OrmModel\Entity\EntityInterface;

    /**
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function getArray(string $where = null, array $bind = []): array;

    /**
     * @param array $rules
     *
     * @return int
     */
    public function count(...$rules): int;

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     * @param bool $validate
     * @param array $args
     * @return bool
     */
    public function save(OrmModel\Entity\EntityInterface &$entity, bool $validate = true, array &$args = []): bool;

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     * @param bool $validate
     * @param array $args
     *
     * @return bool
     */
    public function saveAll(OrmModel\Entity\EntityInterface &$entity, bool $validate = true, array &$args = []): bool;

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     * @param array $args
     *
     * @return bool
     */
    public function delete(OrmModel\Entity\EntityInterface &$entity, array &$args = []): bool;

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     *
     * @return bool
     */
    public function checkEntityType($entity): bool;

    /**
     * @param array $data
     * @param bool $setDefaults
     *
     * @return OrmModel\Entity\EntityInterface
     */
    public function create(array $data = [], bool $setDefaults = true): OrmModel\Entity\EntityInterface;

    /**
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function getIds(string $where = null, array $bind = []): array;

    /**
     * @param string $fieldName
     *
     * @return string
     */
    public function fieldToHumanReadable(string $fieldName): string;

    /**
     * @param $relations
     *
     * @return OrmModel\EagerLoader
     */
    public function with($relations): OrmModel\EagerLoader;

    /**
     * @return string
     */
    public function getDataFieldPrefix(): string;

    /**
     * @return string
     */
    public function getDataTable(): string;

    /**
     * @return OrmModel\DataAccess\DataAccessInterface
     */
    public function getDataAccess(): OrmModel\DataAccess\DataAccessInterface;

    /**
     * @param string $fieldName
     *
     * @return bool
     */
    public function isSimpleDbField(string $fieldName): bool;

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function min($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function max($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function avg($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return mixed
     */
    public function sum($field, string $where = null, array $bind = []);

    /**
     * @param $field
     * @param string|null $where
     * @param array $bind
     *
     * @return array
     */
    public function distinct($field, string $where = null, array $bind = []): array;

    /**
     * @return string
     */
    public function getEntityClassName(): string;

    /**
     * @return string
     */
    public function getWrapperClassName(): string;

    /**
     * @param string $where
     * @param array $bind
     *
     * @return OrmModel\ObjectStorageProxy
     */
    public function getProxy(string $where = '', array $bind = []): OrmModel\ObjectStorageProxy;

    /**
     * @param string $where
     * @param array $bind
     * @param array $values
     *
     * @return bool
     */
    public function set(string $where = '', array $bind = [], array $values = [], bool $validate = true, array &$args = []): bool;

    /**
     * @param integer|integer[] $ids
     * @return bool
     */
    public function setById($ids, array $values = []): bool;

    /**
     * @param integer|integer[] $ids
     * @return bool
     */
    public function deleteById($ids): bool;

    /**
     * @param string $where
     * @param array $bind
     *
     * @return bool
     */
    public function deleteWhere(string $where = '', array $bind = []): bool;

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     * @return bool
     */
    public function validate(OrmModel\Entity\EntityInterface &$entity): bool;

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     * @return bool
     */
    public function validateAll(OrmModel\Entity\EntityInterface &$entity): bool;

    /**
     * @return array
     */
    public function getBelongsTo(): array;

    /**
     * @return array
     */
    public function getHasMany(): array;

    /**
     * @return array
     */
    public function getHasManyToMany(): array;

    /**
     * @return array
     */
    public function getHasOne(): array;

    /**
     * @param string $field
     * @return array
     */
    public function getBelongsToRelByField(string $field): array;

    /**
     * @param string $field
     * @return array
     */
    public function getHasManyRelByField(string $field): array;

    /**
     * @param string $field
     * @return array
     */
    public function getHasManyToManyRelByField(string $field): array;

    /**
     * @param string $field
     * @return array
     */
    public function getHasOneRelByField(string $field): array;

    /**
     * @param OrmModel\Entity\EntityInterface $entity
     * @return OrmModel\Entity\EntityInterface
     */
    public function refresh(OrmModel\Entity\EntityInterface &$entity): OrmModel\Entity\EntityInterface;
}
