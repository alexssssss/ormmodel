<?php

namespace Alexssssss\OrmModel;

use Alexssssss\OrmModel\Entity\EntityInterface;
use Symfony\Component\Validator\Validation;

class Validator
{

    /**
     *
     * @var \Symfony\Contracts\Translation\TranslatorInterface
     */
    protected $translator;

    /**
     *
     * @var \Symfony\Component\Validator\Validation;
     */
    protected $validator;

    /**
     * @var Validator
     */
    protected static $instance = null;

    protected function __construct(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
        $this->translator = $translator;

        $this->validator = $this->createValidatorObject();

        self::$instance = $this;
    }

    /**
     * @codeCoverageIgnore
     * @throws \Exception
     */
    protected function createValidatorObject()
    {
        if (!defined('VENDOR_DIR')) {
            throw new \Exception('VENDOR_DIR needs to be defined to use Alexssssss\Validator');
        }

        $namespace = 'Symfony\\Component\\Validator\\Constraints\\';

        \Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(function ($class) use ($namespace) {
            if (strpos($class, $namespace) === 0) {
                $file = ltrim(str_replace([$namespace, '\\'], DIRECTORY_SEPARATOR, $class) . ".php", DIRECTORY_SEPARATOR);

                $directory = VENDOR_DIR . '/symfony/validator/Constraints';
                $ormDirectory = VENDOR_DIR . '/alexssssss/ormmodel/src/Validator/Constraints';

                if (is_file($directory . DIRECTORY_SEPARATOR . $file)) {
                    require $directory . DIRECTORY_SEPARATOR . $file;
                    return true;
                }

                if (is_file($ormDirectory . DIRECTORY_SEPARATOR . $file)) {
                    require $ormDirectory . DIRECTORY_SEPARATOR . $file;
                    return true;
                }
            }
        });

        return Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
    }

    /**
     *
     * @param Entity\EntityInterface $entity
     * @return bool
     * @throws Exception\InvalidFields
     */
    public function validate(Entity\EntityInterface &$entity): bool
    {
        if (($entity instanceof AbstractProxy && $entity->isLoaded() === false) || $entity->hasUnsavedChanges() === false) {
            return true;
        } else {
            $violations = $this->validator->validate($entity instanceof FakeObjectInterface ? $entity->getRealObject() : $entity);

            if ($violations->count() == 0) {
                return true;
            } else {
                throw new Exception\InvalidFields($violations, $this->translator);
            }
        }
    }

    /**
     *
     * @param Entity\EntityInterface $entity
     * @return bool
     */
    public function validateAll(Entity\EntityInterface &$entity): bool
    {
        $entity->validate();
        
        foreach ($entity->toArray() as $object) {
            if ($object instanceof WrapperInterface) {
                $object = $object->getRealObject(false);
            }

            if (!is_object($object) || $object instanceof \DateTime || ($object instanceof ObjectStorageProxy && $object->isLoaded() === false)) {
                continue;
            } elseif ($object instanceof EntityInterface) {
                if ($object->hasUnsavedChanges() == true) {
                    $object->validate();
                }
            } elseif ($object instanceof ObjectStorageInterface) {
                $this->validateObjectStorage($object);
            }
        }

        return true;
    }

    /**
     *
     * @param ObjectStorageInterface $objectStorage
     * @return bool
     */
    protected function validateObjectStorage(ObjectStorageInterface $objectStorage): bool
    {
        foreach ($objectStorage as $object) {
            if ($object instanceof WrapperInterface) {
                $object = $object->getRealObject(false);
            }

            $object->validateAll();
        }

        return true;
    }

    /**
     * @param \Symfony\Contracts\Translation\TranslatorInterface $translator
     * @return $this
     */
    public static function getInstance(\Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
        if (!isset(self::$instance)) {
            self::$instance = new Validator($translator);
        }

        return self::$instance;
    }
}
