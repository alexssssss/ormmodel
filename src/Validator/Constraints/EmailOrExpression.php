<?php

namespace Symfony\Component\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Martijn v.d. Berg <martijn@multoo.nl> <m.vd.berg1989@gmail.com>
 */
class EmailOrExpression extends Constraint
{
    const EXPRESSION_OR_MAIL_FAILED_ERROR = '8b2bga1c-2v31-d4fd-bb25-r75893481284';

    protected static $errorNames = array(
        self::EXPRESSION_OR_MAIL_FAILED_ERROR => 'EXPRESSION_OR_MAIL_FAILED_ERROR',
    );

    public $message = 'This value is not a valid email address.';
    public $checkMX = false;
    public $checkHost = false;
    public $expression = null;
    public $strict;

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return array(self::CLASS_CONSTRAINT, self::PROPERTY_CONSTRAINT);
    }
}
