<?php

namespace Symfony\Component\Validator\Constraints;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\RuntimeException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Egulias\EmailValidator\Validation\EmailValidation;
use Egulias\EmailValidator\Validation\NoRFCWarningsValidation;

/**
 * @author Martijn v.d. Berg <martijn@multoo.nl> <m.vd.berg1989@gmail.com>
 */
class EmailOrExpressionValidator extends ConstraintValidator
{
    /**
     * @var bool
     */
    private $isStrict;

    /**
     * @var PropertyAccessorInterface
     */
    private $propertyAccessor;

    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;

    public function __construct($strict = false, PropertyAccessorInterface $propertyAccessor = null, ExpressionLanguage $expressionLanguage = null)
    {
        $this->isStrict = $strict;
        $this->propertyAccessor = $propertyAccessor;
        $this->expressionLanguage = $expressionLanguage;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof EmailOrExpression) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\EmailOrExpression');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;
        $variables = array();

        if (null === $constraint->strict) {
            $constraint->strict = $this->isStrict;
        }

        // Symfony 2.5+
        if ($this->context instanceof ExecutionContextInterface) {
            $variables['value'] = $constraint->expression;
            $variables['this'] = $this->context->getObject();
        } elseif (null === $this->context->getPropertyName()) {
            $variables['value'] = $constraint->expression;
            $variables['this'] = $constraint->expression;
        } else {
            $root = $this->context->getRoot();
            $variables['value'] = $constraint->expression;

            if (is_object($root)) {
                // Extract the object that the property belongs to from the object graph
                $path = new PropertyPath($this->context->getPropertyPath());
                $parentPath = $path->getParent();
                $variables['this'] = $parentPath ? $this->getPropertyAccessor()->getValue($root, $parentPath) : $root;
            } else {
                $variables['this'] = null;
            }
        }

        // Fail if both check fails
        $expressionCheckFail = false;
        $mailCheckFail = false;

        if ($constraint->expression !== null && !$this->getExpressionLanguage()->evaluate($constraint->expression, $variables)) {
            $expressionCheckFail = true;
        }

        if ($constraint->strict) {
            if (!class_exists('\Egulias\EmailValidator\EmailValidator')) {
                throw new RuntimeException('Strict email validation requires egulias/email-validator ~1.2|~2.0');
            }

            $strictValidator = new \Egulias\EmailValidator\EmailValidator();

            if (interface_exists(EmailValidation::class) && !$strictValidator->isValid($value, new NoRFCWarningsValidation())) {
                $mailCheckFail = true;
            } elseif (!interface_exists(EmailValidation::class) && !$strictValidator->isValid($value, false, true)) {
                $mailCheckFail = true;
            }
        } elseif (!preg_match('/^.+\@\S+\.\S+$/', $value)) {
            $mailCheckFail = true;
        }

        $host = substr($value, strrpos($value, '@') + 1);

        if ($constraint->checkMX && !$this->checkMX($host)) {
            $mailCheckFail = true;
        }

        if ($constraint->checkHost && !$this->checkHost($host)) {
            $mailCheckFail = true;
        }

        if (($constraint->expression == null && $mailCheckFail) || ($expressionCheckFail && $mailCheckFail)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(EmailOrExpression::EXPRESSION_OR_MAIL_FAILED_ERROR)
                ->addViolation();

            return;
        }
    }

    /**
     * Check DNS Records for MX type.
     *
     * @param string $host Host
     *
     * @return bool
     */
    private function checkMX($host)
    {
        return checkdnsrr($host, 'MX');
    }

    /**
     * Check if one of MX, A or AAAA DNS RR exists.
     *
     * @param string $host Host
     *
     * @return bool
     */
    private function checkHost($host)
    {
        return $this->checkMX($host) || (checkdnsrr($host, 'A') || checkdnsrr($host, 'AAAA'));
    }

    private function getExpressionLanguage()
    {
        if (null === $this->expressionLanguage) {
            if (!class_exists('Symfony\Component\ExpressionLanguage\ExpressionLanguage')) {
                throw new RuntimeException('Unable to use expressions as the Symfony ExpressionLanguage component is not installed.');
            }
            $this->expressionLanguage = new ExpressionLanguage();
        }

        return $this->expressionLanguage;
    }

    private function getPropertyAccessor()
    {
        if (null === $this->propertyAccessor) {
            if (!class_exists('Symfony\Component\PropertyAccess\PropertyAccess')) {
                throw new RuntimeException('Unable to use expressions as the Symfony PropertyAccess component is not installed.');
            }
            $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        }

        return $this->propertyAccessor;
    }
}
