<?php

namespace Alexssssss\OrmModel;

trait WrapperTrait
{

    /**
     *
     * @var Entity\EntityInterface
     */
    protected $wrapped;

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $wrapped
     */
    public function __construct(Entity\EntityInterface &$wrapped)
    {
        $this->wrapped = $wrapped;

        $properties = array_keys(get_class_vars(get_class($this)));
        unset($properties[array_search('wrapped', $properties)]);
        unset($properties[array_search('notMapped', $properties)]);
        unset($properties[array_search('hidden', $properties)]);

        foreach ($properties as $property) {
            unset($this->{$property});
        }
    }

    /**
     * @param bool $checkSub
     * @return Entity\EntityInterface
     */
    public function getRealObject(bool $checkSub = true)
    {
        if ($checkSub === true && $this->wrapped instanceof FakeObjectInterface) {
            return $this->wrapped->getRealObject();
        } else {
            return $this->wrapped;
        }
    }

    /**
     * @param $fun
     * @param $params
     * @return mixed
     * @throws Exception\UndefinedMethod
     */
    public function __call($fun, $params)
    {
        if (is_callable(array($this->wrapped, $fun))) {
            return call_user_func_array([$this->wrapped, $fun], $params);
        } else {
            throw new Exception\UndefinedMethod('Call to undefined method ' . get_class($this) . '::' . $fun . '() / ' . get_class($this->wrapped) . '::' . $fun . '()');
        }
    }
}
