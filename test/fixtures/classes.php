<?php

use Symfony\Component\Validator\Constraints as Assert;
use Alexssssss\OrmModel;

class ClassWithAll3TypesOfProps extends OrmModel\Entity\AbstractEntity
{

    public $publicProp = 1;
    protected $protectedProp = 2;
    private $privateProp = 3;

    public function getProtectedProp()
    {
        return $this->protectedProp;
    }

    public function getPrivateProp()
    {
        return $this->privateProp;
    }
}

class UserClassWithChildAndValidation extends OrmModel\Entity\AbstractEntity
{

    /**
     * @Assert\NotBlank
     */
    public $name = "T. Tester";

    /**
     * @Assert\NotBlank
     * @Assert\Email
     */
    public $email = "test@test.nl";
    public $dog;

    public function __construct(OrmModel\Service\ServiceInterface $service)
    {
        parent::__construct($service);
        $this->dog = new ChildDogsClassWithValidation($service);
    }
}

class ChildDogsClassWithValidation extends OrmModel\Entity\AbstractEntity
{

    /**
     * @Assert\NotBlank
     */
    public $name = "Max";

}

class HasManyTestObject extends OrmModel\Entity\AbstractEntity
{

    public $id = 1;
    public $fieldA = "A";
    public $fieldB = "B";
    public $parentId = null;
    public $parent = null;

}

class HasManyTestObjectWrapper extends HasManyTestObject implements \Alexssssss\OrmModel\WrapperInterface
{

    use \Alexssssss\OrmModel\WrapperTrait;
}


class ObjectStorageTestObject extends OrmModel\Entity\AbstractEntity
{

    public $fieldA = "A";
    public $fieldB = "A";
    public $subObj = null;
    public $field;
    public $number;
    public $date;

}

class AbstractDataMapperTestObject extends OrmModel\Entity\AbstractEntity
{

    public $name = 'Hans Berliner';
    public $country = 'Germany';

}

class AbstractServiceTestObject extends OrmModel\Entity\AbstractEntity
{

    public $id = 123;
    public $tested = true;

}

class AbstractEntityTestClass extends OrmModel\Entity\AbstractEntity
{

    protected $id = null;
    public $name = "Mr. tester";
    public $country = "Netherlands";
    protected $password = "PAS123";
    protected $secret = "SEC456";
    protected $objectId = 159;
    public $object;

    public function __construct(OrmModel\Service\AbstractService $service)
    {
        parent::__construct($service);
        $this->object = new \StdClass();
        $this->object->id = 159;
        $this->object->itsValue = 'test';
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($newPassword)
    {
        $this->password = $newPassword;
    }
}

class DataMapperWithoutCache extends OrmModel\DataMapper\AbstractDataMapper
{

    protected $entityTable = 'tableName';

}

class DataMapperWithMemCache3Hours extends OrmModel\DataMapper\AbstractDataMapper
{

    protected $entityTable = 'tableName';
    protected $cacheEnabled = true;
    protected $cacheType = 'memory';
    protected $cacheMaxAge = "-3 hours";

}

class ServiceTestClass extends OrmModel\Service\AbstractService
{

}

class AbstractEntityTestClassWrapper extends AbstractEntityTestClass implements \Alexssssss\OrmModel\WrapperInterface
{

    use \Alexssssss\OrmModel\WrapperTrait;
}
