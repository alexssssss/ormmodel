<?php

namespace Test\Alexssssss\OrmModel\AbstractData;

class AuraSqlTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var DataAccessAuraSql
     */
    protected $object;

    protected function setUp()
    {
        $this->loggerMock = $this->getMockBuilder('Psr\Log\LoggerInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->adapterMock = $this->getMockBuilder('Aura\Sql\ExtendedPdo')
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = new \Alexssssss\OrmModel\DataAccess\AuraSql($this->adapterMock);

        $reflection = new \ReflectionClass($this->object);
        $reflectionProperty = $reflection->getProperty('logger');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->object, $this->loggerMock);
    }

    protected function tearDown()
    {
    }

    public function testSetTable()
    {
        $value = 'tested';
        $this->object->setTable($value);
        $this->assertAttributeEquals($value, 'table', $this->object);
    }

    public function testSetFieldPrefix()
    {
        $value = 'tested';
        $this->object->setFieldPrefix($value);
        $this->assertAttributeEquals($value, 'fieldPrefix', $this->object);
    }
}
