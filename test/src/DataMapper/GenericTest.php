<?php

namespace Test\Alexssssss\OrmModel\DataMapper;

class GenericTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var Generic
     */
    protected $object;

    protected function setUp()
    {
        $this->entityFactoryMock = $this->getMockBuilder('Alexssssss\OrmModel\Entity\EntityFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = new \Alexssssss\OrmModel\DataMapper\Generic($this->entityFactoryMock);
    }

    protected function tearDown()
    {

    }

    public function testSetEntityClassName()
    {
        $value = 'tested';
        $this->object->setEntityClassName($value);

        $this->assertAttributeEquals($value, 'entityClassName', $this->object);
    }
}
