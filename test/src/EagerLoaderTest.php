<?php

namespace Test\Alexssssss\OrmModel;

class EagerLoaderTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var EagerLoader
     */
    protected $object;
    protected $serviceMock;

    protected function setUp()
    {
        $this->serviceMock = $this->getMockBuilder('Alexssssss\OrmModel\Service\AbstractService')
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {

    }

    public function testRelations()
    {
        $relationsInDotNot = ['elementA.elementB', 'elementA.elementC', 'elementD.elementE.elementF', 'elementD.elementG'];

        $relationsArray = [
            'elementA' => [
                'elementB' => [],
                'elementC' => []
            ]
            ,
            'elementD' => [
                'elementE' => [
                    'elementF' => []
                ],
                'elementG' => []
            ]
        ];

        $this->object = new \Alexssssss\OrmModel\EagerLoader($this->serviceMock, $relationsInDotNot);

        $this->assertAttributeEquals($relationsArray, 'relations', $this->object);
    }
}
