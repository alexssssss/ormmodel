<?php

namespace Test\Alexssssss\OrmModel\Entity\Proxy;

/**
 * Generated by PHPUnit_SkeletonGenerator 1.2.1 on 2013-12-28 at 11:21:44.
 */
class EntityTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var Entity
     */
    protected $object;
    public $serviceMock;
    protected $abstractServiceMock;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->abstractServiceMock = $this->getMockBuilder('Alexssssss\OrmModel\Service\AbstractService')
            ->disableOriginalConstructor()
            ->getMock();

        $this->serviceMock = $this->getMockBuilder('Alexssssss\OrmModel\Service\AbstractService')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {

    }

    public function test__call()
    {
        $proxyEntity = new \Alexssssss\OrmModel\Entity\Proxy\Entity($this->serviceMock, 123);

        $entity = new \ClassWithAll3TypesOfProps($this->abstractServiceMock);

        $this->serviceMock->expects($this->once())
            ->method('getOne')
            ->will(
                $this->returnValue(
                    $entity
                )
        );

        $this->assertEquals(2, $proxyEntity->getProtectedProp());
    }

    public function test__set()
    {
        $proxyEntity = new \Alexssssss\OrmModel\Entity\Proxy\Entity($this->serviceMock, 123);

        $entity = new \ClassWithAll3TypesOfProps($this->abstractServiceMock);

        $this->serviceMock->expects($this->once())
            ->method('getOne')
            ->will(
                $this->returnValue(
                    $entity
                )
        );

        $proxyEntity->publicProp = 'tested';

        $this->assertEquals('tested', $entity->publicProp);
    }

    public function test__get()
    {
        $proxyEntity = new \Alexssssss\OrmModel\Entity\Proxy\Entity($this->serviceMock, 123);

        $entity = new \ClassWithAll3TypesOfProps($this->abstractServiceMock);

        $this->serviceMock->expects($this->once())
            ->method('getOne')
            ->will(
                $this->returnValue(
                    $entity
                )
        );

        $this->assertEquals(1, $proxyEntity->publicProp);
    }

    public function test__isset()
    {
        $proxyEntity = new \Alexssssss\OrmModel\Entity\Proxy\Entity($this->serviceMock, 123);

        $entity = new \ClassWithAll3TypesOfProps($this->abstractServiceMock);

        $this->serviceMock->expects($this->once())
            ->method('getOne')
            ->will(
                $this->returnValue(
                    $entity
                )
        );

        $this->assertTrue(isset($proxyEntity->publicProp));
    }

    public function testGetRealObject()
    {
        $proxyEntity = new \Alexssssss\OrmModel\Entity\Proxy\Entity($this->serviceMock, 123);

        $entity = new \ClassWithAll3TypesOfProps($this->abstractServiceMock);

        $this->serviceMock->expects($this->once())
            ->method('getOne')
            ->will(
                $this->returnValue(
                    $entity
                )
        );

        $this->assertSame($entity, $proxyEntity->getRealObject());
    }

    /**
     * @todo   Implement testSetPreSaveRelations().
     */
    public function testSetPreSaveRelations()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    /**
     * @todo   Implement testSetPostSaveRelations().
     */
    public function testSetPostSaveRelations()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
        );
    }

    public function testToArray()
    {
        $proxyEntity = new \Alexssssss\OrmModel\Entity\Proxy\Entity($this->serviceMock, 123);

        $abstractServiceMock = $this->getMockBuilder('Alexssssss\OrmModel\Service\AbstractService')
            ->setMockClassName('ServiceMock')
            ->disableOriginalConstructor()
            ->getMock();

        $entityMock = $this->getMockBuilder('AbstractEntityTestClass')
            ->setConstructorArgs([$abstractServiceMock])
            ->getMock();

        $entityMock->expects($this->once())
            ->method('toArray')
            ->with(true, true, true)
            ->will($this->returnValue(['name' => 'tested']));

        $this->serviceMock->expects($this->once())
            ->method('getOne')
            ->will(
                $this->returnValue(
                    $entityMock
                )
        );

        $proxyEntity->name = 'tested';

        $changedFields = $proxyEntity->toArray(true, true, true);

        $this->assertSame(['name' => 'tested'], $changedFields);
    }
}
