<?php

namespace Test\Alexssssss\OrmModel;

class ObjectStorageTest extends \PHPUnit\Framework\TestCase
{

    /**
     *
     * @var Alexssssss\OrmModel\ObjectStorage
     */
    protected $object;
    protected $abstractServiceMock;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {

        $this->abstractServiceMock = $this->getMockBuilder('Alexssssss\OrmModel\Service\AbstractService')
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = $this->getMockBuilder('Alexssssss\OrmModel\ObjectStorage')
            ->setConstructorArgs([[], $this->abstractServiceMock])
            ->setMockClassName('ObjectStorageMock')
            ->setMethods(['createObjectStorageObject'])
            ->getMock();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {

    }

    public function test__construct()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1->uuid => $obj1, $obj2->uuid => $obj2, $obj3->uuid => $obj3];
        $object = new \Alexssssss\OrmModel\ObjectStorage($arrayOfObjects);

        $this->assertAttributeEquals($arrayOfObjects, 'array', $object);
    }

    /**
     * @expectedException Exception
     */
    public function testMultipleEntitiesTypesError()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ClassWithAll3TypesOfProps($this->abstractServiceMock);
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $object = new \Alexssssss\OrmModel\ObjectStorage($arrayOfObjects);
    }

    public function testAttach()
    {
        $newObject = new \ObjectStorageTestObject($this->abstractServiceMock);
        $newObject->field = true;
        $this->object->attach($newObject);
        $this->assertAttributeEquals([$newObject->uuid => $newObject], 'array', $this->object);
    }

    public function testIterator()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $i = 0;
        foreach ($this->object as $key => $object) {
            $i++;
            $this->assertEquals($i - 1, $key);
            $this->assertEquals(${'obj' . $i}, $object);
        }
        $this->assertEquals(3, $i);
    }

    public function testGet()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $subObjectStorageMock = $this->getMockBuilder('Alexssssss\OrmModel\ObjectStorage')
            ->disableOriginalConstructor()
            ->getMock();

        $this->object->expects($this->once())
            ->method('createObjectStorageObject')
            ->will($this->returnValue($subObjectStorageMock));

        $subObjectStorageMock->expects($this->at(0))
            ->method('attach')
            ->with($obj2);

        $subObjectStorageMock->expects($this->exactly(1))
            ->method('attach');

        $this->object->get(['number' => 3]);
    }

    public function testGetWithSubObject()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->subObj = &$subObj1;
        $subObj1->number = 2;

        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->subObj = &$subObj2;
        $subObj2->number = 3;

        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->subObj = &$subObj3;
        $subObj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $subObjectStorageMock = $this->getMockBuilder('Alexssssss\OrmModel\ObjectStorage')
            ->disableOriginalConstructor()
            ->getMock();

        $this->object->expects($this->once())
            ->method('createObjectStorageObject')
            ->will($this->returnValue($subObjectStorageMock));

        $subObjectStorageMock->expects($this->at(0))
            ->method('attach')
            ->with($obj2);

        $subObjectStorageMock->expects($this->exactly(1))
            ->method('attach');

        $this->object->get(['subObj.number' => 3]);
    }

    public function testNotGet()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $subObjectStorageMock = $this->getMockBuilder('Alexssssss\OrmModel\ObjectStorage')
            ->disableOriginalConstructor()
            ->getMock();

        $this->object->expects($this->once())
            ->method('createObjectStorageObject')
            ->will($this->returnValue($subObjectStorageMock));

        $subObjectStorageMock->expects($this->at(0))
            ->method('attach')
            ->with($obj1);
        $subObjectStorageMock->expects($this->at(1))
            ->method('attach')
            ->with($obj3);

        $subObjectStorageMock->expects($this->exactly(2))
            ->method('attach');

        $this->object->get(['number', '<>', 3]);
    }

    public function testNotGetWithSubObject()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->subObj = &$subObj1;
        $subObj1->number = 2;

        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->subObj = &$subObj2;
        $subObj2->number = 3;

        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->subObj = &$subObj3;
        $subObj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $subObjectStorageMock = $this->getMockBuilder('Alexssssss\OrmModel\ObjectStorage')
            ->disableOriginalConstructor()
            ->getMock();

        $this->object->expects($this->once())
            ->method('createObjectStorageObject')
            ->will($this->returnValue($subObjectStorageMock));

        $subObjectStorageMock->expects($this->at(0))
            ->method('attach')
            ->with($obj1);
        $subObjectStorageMock->expects($this->at(1))
            ->method('attach')
            ->with($obj3);

        $subObjectStorageMock->expects($this->exactly(2))
            ->method('attach');

        $this->object->get(['subObj.number', '<>', 3]);
    }

    public function testSortNumeric()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->sort(['number' => 'DESC']);

        $this->assertAttributeEquals([$obj3, $obj2, $obj1], 'array', $this->object);
    }

    public function testSortAlphabetical()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 'a';
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 'b';
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 'c';

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->sort(['number' => 'DESC']);

        $this->assertAttributeEquals([$obj3, $obj2, $obj1], 'array', $this->object);
    }

    public function testSortDate()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = '2014-01-01';
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = '2014-02-02';
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = '2014-03-03';

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->sort(['number' => 'DESC']);

        $this->assertAttributeEquals([$obj3, $obj2, $obj1], 'array', $this->object);
    }

    public function testSortWithSubObject()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->subObj = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->subObj->number = 2;

        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->subObj = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->subObj->number = 3;

        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->subObj = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->subObj->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->sort(['subObj.number' => 'DESC']);

        $this->assertAttributeEquals([$obj3, $obj2, $obj1], 'array', $this->object);
    }

    public function testCount()
    {
        $arrayOfObjects = [new \StdClass(), new \StdClass(), new \StdClass()];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals(3, $this->object->count());
    }

    public function testSum()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals(9, $this->object->sum('number'));
    }

    public function testAvg()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals(3, $this->object->avg('number'));
    }

    public function testMinWithNumber()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals(2, $this->object->min('number'));
    }

    public function testMinWithDate()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->date = '2015-01-01';
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->date = '2012-01-01';
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->date = '2017-01-01';

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals('2012-01-01', $this->object->min('date'));
    }

    public function testMaxWithNumber()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals(4, $this->object->max('number'));
    }

    public function testMaxWithDate()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->date = '2015-01-01';
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->date = '2012-01-01';
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->date = '2017-01-01';

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals('2017-01-01', $this->object->max('date'));
    }

    public function testGetFirst()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals($obj1, $this->object->getFirst());
    }

    public function testGetLast()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->assertEquals($obj3, $this->object->getLast());
    }

    public function testLimitFromStart()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->expects($this->once())
            ->method('createObjectStorageObject')
            ->with([$obj1, $obj2]);

        $this->object->limit(2);
    }

    public function testLimitFromPosition()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->number = 2;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->number = 3;
        $obj3 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj3->number = 4;

        $arrayOfObjects = [$obj1, $obj2, $obj3];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->expects($this->once())
            ->method('createObjectStorageObject')
            ->with([$obj2, $obj3]);

        $this->object->limit(1, 2);
    }

    public function testSet()
    {
        $obj1 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj1->fieldA = null;
        $obj2 = new \ObjectStorageTestObject($this->abstractServiceMock);
        $obj2->fieldA = null;

        $value = "tested";


        $arrayOfObjects = [$obj1, $obj2];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->set(['fieldA' => $value]);

        $this->assertEquals($value, $obj1->fieldA);
        $this->assertEquals($value, $obj2->fieldA);
    }

    public function testSetWithSubObject()
    {
        $object = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj = new \ObjectStorageTestObject($this->abstractServiceMock);
        $subObj->fieldA = false;
        $object->subObj = &$subObj;

        $value = "tested";

        $arrayOfObjects = [$object];
        $this->setArrayOfObject($arrayOfObjects);

        $this->object->set(['subObj.fieldA' => $value]);

        $this->assertEquals($value, $object->subObj->fieldA);
    }

    protected function setArrayOfObject(array $arrayOfObjects)
    {
        $reflection = new \ReflectionClass($this->object);
        $reflectionProperty = $reflection->getProperty('array');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($this->object, $arrayOfObjects);
    }

    public function testCreate()
    {

        $fields = ['a' => 'b'];
        $defaults = true;
        $entity = new \ObjectStorageTestObject($this->abstractServiceMock);
        $entity->id = null;

        $this->abstractServiceMock->expects($this->once())
            ->method('create')
            ->with($fields, $defaults)
            ->will($this->returnValue($entity));

        $return = $this->object->create($fields, $defaults);

        $this->assertEquals($return, $entity);
    }
}
